import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LanguageService extends GetxService {
  static LanguageService get to => Get.find();
  var locale = const Locale('en', 'US');
  var localeKey = 'en';

  Future<LanguageService> init() async {
    if (ui.window.locale.languageCode == 'zh') {
      locale = const Locale('zh', 'CN');
      localeKey = 'zh-CN';
    } else if (ui.window.locale.languageCode == 'en') {
      locale = const Locale('en', 'US');
      localeKey = 'en';
    } else if (ui.window.locale.languageCode == 'vi') {
      locale = const Locale('vi', 'VN');
      localeKey = 'vi';
    } else if (ui.window.locale.languageCode == 'ja') {
      locale = const Locale('ja', 'JP');
      localeKey = 'ja';
    }
    return this;
  }

  void changeLocale(l) {
    if (l == const Locale('zh', 'CN')) {
      localeKey = 'zh-CN';
      updateLocale(const Locale('zh', 'CN'));
    } else if (l == const Locale('en', 'US')) {
      localeKey = 'en';
      updateLocale(const Locale('en', 'US'));
    } else if (l == const Locale('vi', 'VN')) {
      localeKey = 'vi';
      updateLocale(const Locale('vi', 'VN'));
    } else if (l == const Locale('ja', 'JP')) {
      localeKey = 'ja';
      updateLocale(const Locale('ja', 'JP'));
    }
  }

  void updateLocale(_l) {
    locale = _l;
    Get.updateLocale(_l);
  }
}
