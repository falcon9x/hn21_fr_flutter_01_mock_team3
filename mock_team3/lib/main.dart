import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_team3/app/core/app_theme.dart';
import 'package:mock_team3/app/routes/app_pages.dart';
import 'package:mock_team3/app/routes/app_routes.dart';
import 'dart:developer' as developer;

import 'generated/locales.g.dart';
import 'translations/language_service.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  try {
    await GetStorage.init();
  } catch (ex) {
    developer.log(ex.toString(), name: 'GetStorageException');
  }
  initServices();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Scratch',
        theme: appThemeData,
        initialRoute: AppRoute.initialScreen,
        getPages: AppPage.routes,
        translationsKeys: AppTranslation.translations,
        locale: LanguageService.to.locale, // locale
        fallbackLocale: const Locale("en", "US"), // false back
      ),
    );
  }
}

Future<void> initServices() async {
  await Get.putAsync(() => LanguageService().init());
}
