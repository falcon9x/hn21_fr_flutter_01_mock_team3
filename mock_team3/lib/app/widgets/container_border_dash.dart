import 'package:flutter/material.dart';

class ContainerBorderDash extends StatelessWidget {
  const ContainerBorderDash({
    Key? key,
    required this.boxHeight,
    required this.boxWidth,
    required this.circularValue,
    required this.thickness,
    required this.color,
    required this.child,
  }) : super(key: key);
  final double boxHeight;
  final double boxWidth;
  final double circularValue;
  final double thickness;
  final Color color;
  final Widget child;
  @override
  Widget build(BuildContext context) {
    List<Widget> dividers = [];
    List<Widget> verticalDivider = [];
    for (int i = 0; i <= (boxWidth - 2 * circularValue) / 10; i++) {
      dividers.add(SizedBox(
        width: 2,
        child: Divider(
          color: Colors.white,
          thickness: thickness,
          height: 0,
        ),
      ));
    }
    for (int i = 0; i <= (boxHeight - 2 * circularValue) / 10; i++) {
      verticalDivider.add(SizedBox(
        height: 2,
        child: VerticalDivider(
          color: Colors.white,
          thickness: thickness,
          width: 0,
        ),
      ));
    }
    return SizedBox(
      height: boxHeight,
      width: boxWidth,
      child: Stack(
        children: [
          // Create border

          Container(
            height: boxHeight,
            width: boxWidth,
            decoration: BoxDecoration(
                border: Border.all(
                  color: color,
                  width: thickness,
                ),
                borderRadius: BorderRadius.circular(circularValue)),
            child: child,
          ),

          // Create border Dash horizontal
          Padding(
            padding: EdgeInsets.symmetric(horizontal: circularValue),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: thickness,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: dividers,
                  ),
                ),
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: dividers,
                ),
              ],
            ),
          ),

          // Create border Dash vertical
          Padding(
            padding: EdgeInsets.symmetric(vertical: circularValue),
            child: Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: verticalDivider,
                ),
                const Spacer(),
                Padding(
                  padding: EdgeInsets.only(right: thickness),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: verticalDivider,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
