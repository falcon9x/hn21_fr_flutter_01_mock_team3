import 'package:flutter/material.dart';

class CircleTabIndicator extends Decoration {
  final BoxPainter _painter;
  final Color color;
  final double radius;
  final double indicatorHeight;

  CircleTabIndicator({
    required this.color,
    required this.radius,
    required this.indicatorHeight,
  }) : _painter = _CirclePainter(color, radius, indicatorHeight);

  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) => _painter;
}

class _CirclePainter extends BoxPainter {
  final Paint _paint;
  final double radius;
  final double indicatorHeight;

  _CirclePainter(Color color, this.radius, this.indicatorHeight)
      : _paint = Paint()
    ..color = color
    ..isAntiAlias = true;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    Rect rect = Offset(offset.dx,
        ((configuration.size?.height ?? 0) - indicatorHeight)) &
    Size((configuration.size?.width ?? 0) , indicatorHeight);
    canvas.drawRRect(
        RRect.fromRectAndCorners(rect,
            topRight: Radius.circular(radius),
            topLeft: Radius.circular(radius)),
        _paint);
  }
}
