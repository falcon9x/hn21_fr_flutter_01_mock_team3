import 'package:flutter/material.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/core/app_text_theme.dart';

class AppRoundedButton extends StatelessWidget {
  final double? width;
  final String text;
  final Function()? onPress;
  final bool isCircle;
  const AppRoundedButton(
      {Key? key,
      required this.text,
      this.width,
      required this.isCircle,
      this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: width,
      decoration: BoxDecoration(
        color: AppColors.mainColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: ElevatedButton(
        onPressed: onPress,
        child: Text(
          text,
          style: appTextTheme.headline5?.copyWith(color: AppColors.colorWhite),
        ),
        style: ElevatedButton.styleFrom(
          elevation: 0,
          padding: const EdgeInsets.all(0),
          shape: isCircle
              ? const CircleBorder()
              : RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          primary: Colors.transparent,
        ),
      ),
    );
  }
}
