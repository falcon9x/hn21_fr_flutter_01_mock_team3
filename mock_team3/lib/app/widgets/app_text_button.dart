import 'package:flutter/material.dart';
import '../core/app_text_theme.dart';

class AppTextButton extends StatelessWidget {
  const AppTextButton({
    Key? key,
    required this.press,
    required this.textButton,
    required this.textButtonColor,
    required this.underlineTextColor,
  }) : super(key: key);
  final Color textButtonColor;
  final Color underlineTextColor;
  final Function()? press;
  final String textButton;

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: press,
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: underlineTextColor,
                width: 1.5,
              ),
            ),
          ),
          child: Text(
            textButton,
            style: appTextTheme.button,
          ),
        ));
  }
}
