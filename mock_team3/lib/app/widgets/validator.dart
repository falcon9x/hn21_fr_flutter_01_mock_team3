class Validator {
  static final regexEmail =
      RegExp(r'^[a-zA-Z0-9]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$');
  static final regexUsername = RegExp(r'^[a-zA-Z," "]+$');
  static final regexPassword =
      RegExp(r'(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])');

  static String? isValidEmail(String email) {
    if (!regexEmail.hasMatch(email)) {
      return 'Email\'s format is wrong';
    } else {
      return null;
    }
  }

  static String? isValidFullName(String fullName) {
    if (!regexUsername.hasMatch(fullName)) {
      return 'User\'s formal only contain letters';
    } else {
      return null;
    }
  }

  static String? isValidPassword(String password) {
    if (password.length < 6) {
      return 'Password is too short';
    }
    if (!regexPassword.hasMatch(password) || password.contains(" ")) {
      return 'Password consists of lowercase letters, numbers, and special characters';
    } else {
      return null;
    }
  }

  static String? isValidRePassword(String password, String rePassword) {
    if (!_equalsIgnoreCase(rePassword, password)) {
      return "Password doesn't match";
    } else {
      return null;
    }
  }

  static bool _equalsIgnoreCase(String string1, String string2) {
    return string1.toLowerCase() == string2.toLowerCase();
  }
}
