// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:mock_team3/app/core/app_colors.dart';

import '../core/app_text_theme.dart';

class AppTextFormField extends StatelessWidget {
  String? labelText;
  TextEditingController? textEditingController;
  bool isPassword;
  bool? isIcon;
  String? Function(String?)? validator;
  void Function(String)? onchange;
  void Function()? onEditingComplete;
  String? errorText;
  FocusNode? focusNode;
  String? initialValue;
  TextInputType? keyboardType;
  Widget? child;
  Color? focusedBorderColor;
  bool? showTextField;
  AppTextFormField({
    Key? key,
    this.focusedBorderColor,
    this.isIcon,
    this.child,
    this.errorText,
    this.validator,
    this.onchange,
    this.onEditingComplete,
    this.labelText,
    this.focusNode,
    this.textEditingController,
    required this.isPassword,
    this.initialValue,
    this.keyboardType,
    this.showTextField,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool _checkUserTextFiled;
    if (showTextField == null) {
      _checkUserTextFiled = true;
    } else {
      _checkUserTextFiled = showTextField!;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        child ??
            Text(
              labelText ?? "",
              style: appTextTheme.subtitle2,
            ),
        _checkUserTextFiled
            ? TextFormField(
                keyboardType: keyboardType ?? TextInputType.text,
                maxLines: keyboardType == null ? 1 : null,
                minLines: 1,
                initialValue: initialValue,
                focusNode: focusNode,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: validator,
                onChanged: onchange,
                onEditingComplete: onEditingComplete,
                controller: textEditingController,
                cursorColor: AppColors.colorBlack,
                obscureText: isPassword,
                decoration: InputDecoration(
                  errorMaxLines: 3,
                  focusColor: AppColors.colorGreyWhite,
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColors.colorGreyWhite),
                  ),
                  errorText: errorText,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: focusedBorderColor ?? AppColors.colorGreyWhite,
                    ),
                  ),
                ),
                style: appTextTheme.bodyText1,
              )
            : const SizedBox(),
      ],
    );
  }
}
