import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/view_models/edit_recipe_view_model.dart';

import '../../data/firebase/firebase_repository.dart';
import '../../data/firebase/firebase_service/firebase_service.dart';

class EditRecipeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EditRecipeViewModel(FirebaseRepositoryImpl(
        FireBaseService(
            firebaseStorage: FirebaseStorage.instance,
            firebaseAuth: FirebaseAuth.instance,
            fireStore: FirebaseFirestore.instance))));
  }
}
