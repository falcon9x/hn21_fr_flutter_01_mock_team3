import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/cooking_mode/view_model/cooking_mode_view_model.dart';
import 'package:mock_team3/data/direction/direction_repository.dart';
import 'package:mock_team3/data/direction/direction_service.dart';
import 'package:mock_team3/data/direction_detail/direction_detail_repository.dart';
import 'package:mock_team3/data/direction_detail/direction_detail_service.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';
import 'package:mock_team3/data/info/info_repository.dart';
import 'package:mock_team3/data/info/info_service.dart';
import 'package:mock_team3/data/recipe/model/recipe_repository.dart';
import 'package:mock_team3/data/recipe/model/recipe_service.dart';

class CookingModeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CookingModeViewModel(
        RecipeRepositoryImpl(RecipeService()),
        InfoRepositoryImpl(InfoService()),
        DirectionRepositoryImpl(
            DirectionService(),
            FireBaseService(
                firebaseStorage: FirebaseStorage.instance,
                fireStore: FirebaseFirestore.instance,
                firebaseAuth: FirebaseAuth.instance)),
        DirectionDetailRepositoryImpl(DirectionDetailService())));
  }
}
