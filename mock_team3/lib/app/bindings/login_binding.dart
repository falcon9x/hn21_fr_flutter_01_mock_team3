import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_team3/app/features/login/view_models/login_view_model.dart';
import 'package:mock_team3/data/account/account_repository.dart';
import 'package:mock_team3/data/account/service/account_service.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';

import '../utils/share_prefs_repository.dart';

class LoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => LoginViewModel(
          AccountRepositoryImpl(
            AccountService(),
          ),
          SharePrefsRepositoryImpl(GetStorage()),
          FirebaseRepositoryImpl(FireBaseService(
              firebaseStorage: FirebaseStorage.instance,
              fireStore: FirebaseFirestore.instance,
              firebaseAuth: FirebaseAuth.instance))),
    );
  }
}
