import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_team3/app/features/on_boarding/view_models/on_boarding_view_model.dart';

import '../utils/share_prefs_repository.dart';

class OnBoardingBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
        () => OnBoardingViewModel(SharePrefsRepositoryImpl(GetStorage())));
  }
}
