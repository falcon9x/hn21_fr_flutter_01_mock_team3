import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_team3/app/features/user_profile/view_model/user_profile_view_model.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/cook_book/service/cook_book_service.dart';
import 'package:mock_team3/data/recipe/model/recipe_service.dart';
import 'package:mock_team3/data/user/user_repository.dart';
import 'package:mock_team3/data/user/user_service.dart';

class UserProfileBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserProfileViewModel(
        UserRepositoryImpl(UserService(),CookBookService(),RecipeService()), SharePrefsRepositoryImpl(GetStorage())));
  }
}
