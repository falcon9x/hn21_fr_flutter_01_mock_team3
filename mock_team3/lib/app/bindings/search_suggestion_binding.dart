import 'package:get/get.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view_model/search_suggestion_view_model.dart';
import 'package:mock_team3/data/search_suggestion/search_suggestion_repository.dart';
import 'package:mock_team3/data/search_suggestion/service/search_suggestion_service.dart';

class SearchSuggestionBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<SearchSuggestionViewModel>(() => SearchSuggestionViewModel(SearchSuggestionImpl(SearchSuggestionService())));
  }

}