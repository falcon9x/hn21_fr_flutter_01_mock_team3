import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/sign_up/view_models/sign_up_view_model.dart';
import 'package:mock_team3/data/account/account_repository.dart';
import 'package:mock_team3/data/account/service/account_service.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';

class SignUpBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => SignUpViewModel(
        AccountRepositoryImpl(
          AccountService(),
        ),
        FirebaseRepositoryImpl(
          FireBaseService(
              firebaseStorage: FirebaseStorage.instance,
              fireStore: FirebaseFirestore.instance,
              firebaseAuth: FirebaseAuth.instance),
        ),
      ),
    );
  }
}
