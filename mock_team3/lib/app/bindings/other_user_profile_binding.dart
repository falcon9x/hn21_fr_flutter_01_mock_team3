import 'package:get/get.dart';
import 'package:mock_team3/app/features/other_user_profile/view_model/other_user_profile_view_model.dart';
import 'package:mock_team3/data/cook_book/service/cook_book_service.dart';
import 'package:mock_team3/data/recipe/model/recipe_service.dart';
import 'package:mock_team3/data/user/user_repository.dart';
import 'package:mock_team3/data/user/user_service.dart';

class OtherUserProfileBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
        () => OtherUserProfileViewModel(UserRepositoryImpl(UserService(),CookBookService(),RecipeService())));
  }
}
