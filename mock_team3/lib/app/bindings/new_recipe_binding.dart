import 'package:get/get.dart';
import 'package:mock_team3/app/features/new_recipe/view_models/new_recipe_view_model.dart';

class NewRecipeBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<NewRecipeViewModel>(() => NewRecipeViewModel());
  }

}