import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_team3/app/features/recipe_feed/recipe_feed_view_model/recipe_feed_view_model.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';
import 'package:mock_team3/data/recipe/recipe_repository.dart';
import 'package:mock_team3/data/recipe/service/recipe_service.dart';

class RecipeFeedBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RecipeFeedViewModel>(() => RecipeFeedViewModel(
        RecipeRepositoryImpl(RecipeService()),
        FirebaseRepositoryImpl(FireBaseService(
            firebaseStorage: FirebaseStorage.instance,
            firebaseAuth: FirebaseAuth.instance,
            fireStore: FirebaseFirestore.instance)),
        SharePrefsRepositoryImpl(GetStorage())));
  }
}
