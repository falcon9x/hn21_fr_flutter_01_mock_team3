import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_team3/app/features/browse_my_recipe/view_models/browse_my_recipe_view_model.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';

class BrowseMyRecipeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => BrowseMyRecipeViewModel(FirebaseRepositoryImpl(
        FireBaseService(
            firebaseStorage: FirebaseStorage.instance,
            fireStore: FirebaseFirestore.instance,
            firebaseAuth: FirebaseAuth.instance)),SharePrefsRepositoryImpl(GetStorage())));
  }
}
