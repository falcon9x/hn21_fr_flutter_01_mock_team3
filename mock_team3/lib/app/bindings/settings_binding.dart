import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mock_team3/app/features/settings/view_model/settings_view_model.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/user/user_repository.dart';

import '../../data/cook_book/service/cook_book_service.dart';
import '../../data/recipe/model/recipe_service.dart';
import '../../data/user/user_service.dart';

class SettingsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SettingViewModel(SharePrefsRepositoryImpl(GetStorage()),
        UserRepositoryImpl(UserService(), CookBookService(), RecipeService())));
  }
}
