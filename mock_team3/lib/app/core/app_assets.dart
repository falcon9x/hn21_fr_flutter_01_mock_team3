class IconBlack {
  static String iconAdd = "assets/icons/black/add.svg";
  static String iconArrowDown = "assets/icons/black/arrow_down.svg";
  static String iconLeft = "assets/icons/black/arrow_left.svg";
  static String iconRight = "assets/icons/black/arrow_right.svg";
  static String iconArrowUp = "assets/icons/black/arrow_up.svg";
  static String iconClose = "assets/icons/black/close.svg";
  static String iconDelete = "assets/icons/black/delete.svg";
  static String iconEdit = "assets/icons/black/edit.svg";
  static String iconFilter = "assets/icons/black/filter.svg";
  static String iconLike = "assets/icons/black/like.svg";
  static String iconLogout = "assets/icons/black/log_out.svg";
  static String iconMessage = "assets/icons/black/message.svg";
  static String iconMoreMenu = "assets/icons/black/more_menu.svg";
  static String iconNav1 = "assets/icons/black/nav1.svg";
  static String iconNav2 = "assets/icons/black/nav2.svg";
  static String iconNav3 = "assets/icons/black/nav3.svg";
  static String iconNotification = "assets/icons/black/notifications.svg";
  static String iconPlay = "assets/icons/black/play.svg";
  static String iconSearch = "assets/icons/black/search.svg";
  static String iconSetting = "assets/icons/black/settings.svg";
  static String iconTime = "assets/icons/black/timer.svg";
  static String iconUpload = "assets/icons/black/upload.svg";
}

class IconGreen {
  static String iconAdd = 'assets/icons/green/add.svg';
  static String iconDone = 'assets/icons/green/done.svg';
  static String iconEdit = 'assets/icons/green/edit_color.svg';
  static String iconFilter = 'assets/icons/green/filterColor.svg';
  static String iconNavi1 = 'assets/icons/green/nav1.svg';
  static String iconNavi2 = 'assets/icons/green/nav2.svg';
  static String iconNavi3 = 'assets/icons/green/nav3.svg';
  static String iconPlay = 'assets/icons/green/play.svg';
}

class IconNumbering {
  static String iconNumbering = 'assets/icons/numbering/black.svg';
  static String iconNumberingBlackOutlined =
      'assets/icons/numbering/black_outlined.svg';
  static String iconNumberingFilled = 'assets/icons/numbering/filled.svg';
  static String iconNumberingOutlined = 'assets/icons/numbering/outlined.svg';
}

class IconRed {
  static String iconRedDelete = 'assets/icons/red/delete.svg';
  static String iconDot = 'assets/icons/red/dot.svg';
  static String iconsIconRed = 'assets/icons/red/like.svg';
}

class IconWhile {
  static String iconWhiteArrowLeft = 'assets/icons/white/arrow_left.svg';
  static String iconRedDot = ' assets/icons/red/dot.svg';
  static String iconWhiteDelete = 'assets/icons/white/delete.svg';
  static String iconWhiteEdit = 'assets/icons/white/edit.svg';
  static String iconFullScreen = 'assets/icons/white/full_screen.svg';
  static String iconWhiteFullScreenExit =
      'assets/icons/white/full_screen_exit.svg';
  static String iconWhitePIP = 'assets/icons/white/video_pip.svg';
  static String iconWhitePlay = 'assets/icons/white/play_1.svg';
}

class AppIcon {
  static String iconLogo = 'assets/icons/logo.svg';
  static String iconLogoDart = 'assets/icons/logoDark.svg';
}

class BrowseMyRecipeAssets {
  static String image = 'assets/images/browse_my_recipe.png';
}

class ViewRecipeAssets {
  static String image = "assets/images/view_recipe.png";
  static String subImage = "assets/images/sub_view_recipe.png";
  static String imageProfile = "assets/images/avatar.png";
}

class LoginAssets {
  static String assetsImage = "assets/images/sign-up.png";
}

class OnBoardingAssets {
  static String assetsLogo = "assets/icons/logo.svg";
  static String assetsImage = "assets/images/on_boarding.png";
}

class EditThirdSectionAssets {
  static String assetsImage = "assets/images/edit_direction.png";
}
