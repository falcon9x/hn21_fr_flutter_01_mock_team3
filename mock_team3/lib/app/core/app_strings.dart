class StringRecipeFeed {
  static const String describe =
      "Apparently we had reached a great height in the atmophere, for the sky was fgmflgmflghmflgm";
  static const String btnSave = "Save";
  static const String titleDialog = "Save to";
  static const String txtAddNewCookBook = "Add New Cookbook";
}

class UserProfileString {
  static const String titleAppBar = "My Kitchen";
  static const String textEndAppBar = "Settings";
  static const String titleTabBar0 = "Recipes";
  static const String titleTabBar1 = "Saved";
  static const String titleTabBar2 = "Following";
}

class OtherUserProfileString {
  static const String titleAppBar = "Back";
  static const String textButton = "Follow";
}

class EditUserString {
  static const String titleAppBar = "Back to Profile";
  static const String titleHeader = "Edit Profile";
  static const String titleUnderAvatar = "Edit Profile Picture";
  static const String labelFormField1 = "Full Name";
  static const String labelFormField2 = "Bio";
  static const String labelFormField3 = "Email";
  static const String labelFormField4 = "Phone";
  static const String textButton = "Save Profile";
}

class SignUpString {
  static const String titleFirst = "Start";
  static const String titleSecond = "from Scratch";
  static const String subtitle = "Create account to continue.";
  static const String fullNameTextField = "Full Name";
  static const String emailTextField = "Email";
  static const String passwordTextField = "Password";
  static const String textButton = "Create Account";
  static const String footer = "Already have an account?";
  static const String textButtonFooter = "Login Here";
}

class BrowseMyRecipeString {
  static const String textBackButton = "Back to My Profile";
  static const String title = "My Recipes";
  static const String textButton = "Add New";
  static const String hastag = "Western (5)";
  static const String titleImage = "Cooked Coconut Mussels";
  static const String timeFood = "± 5 mins";
  static const String amountIngredients = "4 ingredients";
  static const String textButtonCook = "Cook";
}

class ViewRecipeString {
  static const String textBackButton = "Back to My Profile";
  static const String textButtonCook = "Cook Now";
  static const String titleImage = "Engine-Cooked Honey Orange Pancake";
  static const String firstNameTabbar = "Ingredients";
  static const String secondNameTabbar = "How to Cook";
  static const String thirdNameTabbar = "Add";
}

class SettingsString {
  static const String titleLeadingAppBar = "Back";
  static const String titleActionAppBar = "Log Out";
  static const String titleHeader = "Settings";
  static const String titleGroup1 = "Push Notifications";
  static const String titleGroup1Title1 = "Notify me for followers";
  static const String titleGroup1Title2 = "When someone send me a message";
  static const String titleGroup1Title3 = "When someone do live cooking";
  static const String titleGroup2 = "Privacy Settings";
  static const String titleGroup2Title1 = "Followers can see my saved recipes";
  static const String titleGroup2SubTitle1 =
      "If disabled, you won't be able to see recipes saved by other profiles. Leave this enabled to share your collected recipes to others. ";
  static const String titleGroup2SubTitle2 = "why this matter?";
  static const String titleGroup2Title2 = "Followers can see profiles I follow";
  static const String titleFooter = "Change Password";
}

class EditRecipeString {
  static const String ingredients = "Lemonade, coconut, peppers, egg + 5 more";
  static const List<String> cookSteps = [
    "Heat a Belgian waffle iron.",
    "Mix the flour, sugar, and baking powder together in a mixing bowl. Stir in 1 cup eggnog, butter, and the egg until well blended. Add more eggnog if needed to make a pourable batter.",
    "Lightly grease or spray the waffle iron with non-stick cooking spray. Pour some batter onto the preheated waffle iron, …"
  ];
  static const List<List<String>> contentAdditionals = [
    ["12 Mins"],
    nutritionFacts,
    ["Sweet, Coconut, Quick, Easy, Homemade"]
  ];
  static const List<String> nutritionFacts = [
    "222 calories",
    "6.2 g fat",
    "7.2 g carbohydrates",
    "28.6 g protein",
    "68 mg cholesterol",
    "268 mg sodium"
  ];
}

class StringSearchSuggestion {
  static String titleAppBar = "Search recipe,people ,or tag";
  static String txtTrendingRecipes = "Trending Recipes";
  static String txtWhatCanIMake = "What can I make with ingredients";
  static String txtSearchFilter = "Search Filter";
  static String txtIngredients = "Ingredients";
  static String txtServingTime = "Serving Time";
  static String txtSearchFor = "Search for";
  static String btnApplyFilter = "Apply Filter";
}

class StringSearch {
  static String txtRecipe = "Recipe";
  static String txtFollowers = "Followers";
  static String txtProfiles = "Profiles";
  static String txtTags = "Tags";
}

class StringNewRecipe {
  static String txtTitleAppBar = "Back to My Recipe";
  static String txtNewRecipe = "New Recipe";
  static String txtTitleRecipeName = "Recipe Name";
  static String txtHintNameRecipe = "Write Down Recipe Name";
  static String txtGallery = "Gallery";
  static String txtHintGallery = "Upload Images or Open Camera";
  static String txtIngredients = "Ingredients";
  static String txtHintIngredients = "Add Ingredients";
  static String txtHowToCook = "How to cook";
  static String txtHintHowToCook = "Add Directions";
  static String txtAdditionalInfo = "Additional Info";
  static String txtHintAdditionalInfo = "Add Info";
  static String txtSaveTo = "Save to";
  static String btnSaveRecipe = "Save Recipe";
  static String btnPostToFeed = "Post to Feed";
}

class CookingModeString {
  static String titleAppBar = "Back to Recipe";
  static String titleHeader = "Cooking Mode";
  static String titleBody1 = "Steps";
  static String titleBody2 = "View Ingredients";
  static String fakeText =
      "Heat a Belgian waffle iron. Mix the flour, sugar, and baking powder together in a mixing bowl. Stir in 1 cup eggnog, butter, and the egg until well blended. Add more eggnog if needed to make a pourable batter.";
}

class EditThirdSectionString {
  static String titleVideo = "The Making of Waffle.mp4";
}
