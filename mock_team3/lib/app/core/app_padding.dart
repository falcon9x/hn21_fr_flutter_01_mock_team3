import 'package:flutter/cupertino.dart';

class AppPadding {
  static const defaultPadding = EdgeInsets.symmetric(horizontal: 25);
  static const appBarPaddingOnlyRight = EdgeInsets.only(right: 15);
  static const appBarLeadingPadding =
      EdgeInsets.only(left: 14, top: 14, bottom: 14);
  static const appBarTitlePadding = EdgeInsets.only(left: 2.5);
  static const defaultPaddingAll = EdgeInsets.all(25);
  static const defaultPaddingItemListView = EdgeInsets.only(left: 25,right: 25,bottom: 25);
}

class UserProfilePadding {
  static const tabBarPadding = EdgeInsets.only(top: 25);
}

class SignUpPadding {
  static const signUPPadding = EdgeInsets.only(top: 30);
  static const titleAndContentPadding = EdgeInsets.only(top: 20);
}

class EditRecipePadding {
  static const editRecipePadding = EdgeInsets.only(top: 20);
  static const cardPadding = EdgeInsets.all(15);
}
