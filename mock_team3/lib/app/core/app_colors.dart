import 'package:flutter/material.dart';

class AppColors {
  static Color mainColor = const Color(0xFF30BE76);
  static Color mainColorOpacity = const Color(0xFF30BE76).withOpacity(0.2);
  static Color systemColorPink = const Color(0xffF84971);
  static Color systemColorOrange = const Color(0xffF8B449);
  static Color colorBlack = const Color(0xff030F09);
  static Color colorGrey = const Color(0xff767676);
  static Color colorGreyWhite = const Color(0xffCCCCCC);
  static Color colorWhiteGrey = const Color(0xffE5E5E5);
  static Color colorWhiteLight = const Color(0xffF7F8FA);
  static Color colorWhite = const Color(0xffFFFFFF);
  static Color textGrey = const Color(0xFF606060);
  static Color textSubtle = const Color(0xFFA8A8A8);
  static Color colorDivider = const Color(0xFFE6E6E6);
  static Color colorBlackOpacity = const Color(0xff030F09).withOpacity(0.4);
  static Color colorSubTitleVideo = const Color(0x80282928).withOpacity(0.8);
}
