import 'package:flutter/material.dart';

class AppBorderRadius {
  static BorderRadius appborderRadius = BorderRadius.circular(8);
  static BorderRadius smallBorderRadius = BorderRadius.circular(4);
}
