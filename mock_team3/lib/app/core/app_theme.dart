import 'package:flutter/material.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'app_text_theme.dart';

final ThemeData appThemeData = ThemeData(
  highlightColor: AppColors.mainColorOpacity,
  splashColor: AppColors.mainColorOpacity,
  primaryColor: AppColors.colorWhite,
  brightness: Brightness.light,
  fontFamily: 'Nunito-Regular',
  textTheme: appTextTheme,
);
