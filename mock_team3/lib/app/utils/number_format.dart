class NumberFormat {
  static String numberToString(int number) {
    if (number > 999999) {
      return "${number ~/ 1000000}m";
    }
    if (number > 999) {
      return "${number ~/ 1000}k";
    }
    return "$number";
  }

  static String numberToTime(int number) {
    int hour = 0, min = 0;
    hour = number ~/ 60;
    min = number % 60;
    return "${(hour < 9 ? "0$hour" : hour)}:${min < 9 ? "0$min" : min}";
  }
}
