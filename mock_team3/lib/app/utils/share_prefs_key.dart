class SharePrefsKey {
  static String keepLogIn = "keepLogIn";
  static String userId = "userId";
  static String isNotifyMeForFollowers = "isNotifyMeForFollowers";
  static String isWhenSomeoneSendMeAMessage = "isWhenSomeoneSendMeAMessage";
  static String isWhenSomeoneDoLiveCooking = "isWhenSomeoneDoLiveCooking";
  static String isFollowersCanSeeMySavedRecipes = "isFollowersCanSeeMySavedRecipes";
  static String isFollowersCanSeeProfilesIFollow = "isFollowersCanSeeProfilesIFollow";
}
