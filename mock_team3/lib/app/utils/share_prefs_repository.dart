import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

abstract class SharePrefsRepository {
  void saveBool(String key, bool value);

  bool getBool(String key);

  void setString(String key, String value);

  String getString(String key);
}

class SharePrefsRepositoryImpl extends GetxService
    implements SharePrefsRepository {
  late final GetStorage _storage;

  SharePrefsRepositoryImpl(this._storage);

  @override
  bool getBool(String key) {
    return _storage.read(key) ?? false;
  }

  @override
  void saveBool(String key, bool value) {
    _storage.write(key, value);
  }

  @override
  String getString(String key) {
    return _storage.read(key) ?? "";
  }

  @override
  void setString(String key, String value) {
    _storage.write(key, value);
  }
}
