class AppRoute {
  static const String initialScreen = "/on_boarding";
  static const String onBoardingScreen = "/on_boarding";
  static const String userProfileScreen = "/user_profile";
  static const String otherUserProfileScreen = "/other_user_profile";
  static const String loginScreen = "/login";
  static const String recipeFeed = "/recipe_feed";
  static const String editUserProfileScreen = "/edit_user";
  static const String signUpScreen = "/sign_up";
  static const String cookingModeScreen ="/cooking_mode";
  static const String searchSuggestion = "/search_suggestion";
  static const String search = "/search";
  static const String browseMyRecipeScreen = "/browse_my_recipe";
  static const String viewRecipeScreen = "/view_recipe";
  static const String settingsScreen = "/settings";
  static const String editRecipeScreen = "/edit_recipe";
  static const String newRecipeScreen = "/new_recipe";
}
