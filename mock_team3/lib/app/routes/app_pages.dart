import 'package:get/get.dart';
import 'package:mock_team3/app/bindings/browse_my_recipe_binding.dart';
import 'package:mock_team3/app/bindings/cooking_mode_binding.dart';
import 'package:mock_team3/app/bindings/edit_recipe_binding.dart';
import 'package:mock_team3/app/bindings/edit_user_binding.dart';
import 'package:mock_team3/app/bindings/login_binding.dart';
import 'package:mock_team3/app/bindings/new_recipe_binding.dart';
import 'package:mock_team3/app/bindings/other_user_profile_binding.dart';
import 'package:mock_team3/app/bindings/sign_up_binding.dart';
import 'package:mock_team3/app/features/cooking_mode/views/cooking_mode_screen.dart';

import 'package:mock_team3/app/bindings/search_suggestion_binding.dart';
import 'package:mock_team3/app/features/browse_my_recipe/views/browse_my_recipe_screen.dart';
import 'package:mock_team3/app/features/edit_profile/views/edit_user_screen.dart';
import 'package:mock_team3/app/features/edit_recipe/views/edit_recipe_screen.dart';
import 'package:mock_team3/app/features/new_recipe/view/new_recipe_screen.dart';
import 'package:mock_team3/app/features/search/search_view/search_screen.dart';
import 'package:mock_team3/app/features/view_recipe/views/view_recipe_screen.dart';
import 'package:mock_team3/app/bindings/settings_binding.dart';
import '../features/settings/views/settings_screen.dart';
import '../features/login/views/login_screen.dart';

import '../bindings/on_boarding_binding.dart';
import '../bindings/recipe_feed_binding.dart';
import '../bindings/user_profile_binding.dart';
import '../features/on_boarding/views/on_boarding_screen.dart';
import '../features/recipe_feed/recipe_feed_view/recipe_feed_screen.dart';
import '../features/search_suggestion/search_suggestion_view/search_suggestion_screen.dart';
import '../features/sign_up/views/sign_up_screen.dart';
import 'app_routes.dart';

class AppPage {
  static const initial = AppRoute.initialScreen;
  static final routes = [
    GetPage(
      name: AppRoute.onBoardingScreen,
      page: () => const OnBoardingScreen(),
      binding: OnBoardingBinding(),
    ),
    GetPage(
      name: AppRoute.loginScreen,
      page: () => const LoginScreen(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: AppRoute.signUpScreen,
      page: () => const SignUPScreen(),
      binding: SignUpBinding(),
    ),
    GetPage(
        name: AppRoute.recipeFeed,
        page: () => const RecipeFeedScreen(),
        bindings: [
          RecipeFeedBinding(),
          SearchSuggestionBinding(),
          UserProfileBinding(),
          OtherUserProfileBinding(),
        ]),
    GetPage(
        name: AppRoute.editUserProfileScreen,
        page: () => const EditUserScreen(),
        binding: EditUserBinding()),
    GetPage(
        name: AppRoute.search,
        page: () => const SearchScreen(),
        binding: SearchSuggestionBinding()),
    GetPage(
        name: AppRoute.searchSuggestion,
        page: () => const SearchSuggestionScreen(),
        binding: SearchSuggestionBinding()),
    GetPage(
        name: AppRoute.editUserProfileScreen,
        page: () => const EditUserScreen(),
        binding: EditUserBinding()),
    GetPage(
        name: AppRoute.cookingModeScreen,
        page: () => const CookingModeScreen(),
        binding: CookingModeBinding()),
    GetPage(
        name: AppRoute.editUserProfileScreen,
        page: () => const EditUserScreen(),
        binding: EditUserBinding()),
    GetPage(
      name: AppRoute.viewRecipeScreen,
      page: () => const ViewRecipeScreen(),
      binding: EditRecipeBinding(),
    ),
    GetPage(
        name: AppRoute.settingsScreen,
        page: () => const SettingsScreen(),
        binding: SettingsBinding()),
    GetPage(
      name: AppRoute.editRecipeScreen,
      page: () => const EditRecipScreen(),
      binding: EditRecipeBinding(),
    ),
    GetPage(
      name: AppRoute.browseMyRecipeScreen,
      page: () => const BrowseMyRecipeScreen(),
      binding: BrowseMyRecipeBinding(),
    ),
    GetPage(
        name: AppRoute.newRecipeScreen,
        page: () => const NewRecipeScreen(),
        binding: NewRecipeBinding())
  ];
}
