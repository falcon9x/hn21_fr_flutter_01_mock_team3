import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';
import 'package:mock_team3/app/features/other_user_profile/view_model/other_user_profile_view_model.dart';

import '../../../../core/app_padding.dart';
import '../../../../core/app_text_theme.dart';

class OtherRecipesTab extends GetView<OtherUserProfileViewModel> {
  const OtherRecipesTab({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double gridViewCrossAxisSpacing = 15;
    double gridViewMaxCrossAxisExtent = 200;
    double gridViewChildAspectRatio = 155 / 132;
    double gridViewMainAxisSpacing = 15;
    double imageBorderRadius = 8;
    double imageAspectRatio = 155 / 100;
    return Obx(() => GridView.builder(
          padding: UserProfilePadding.tabBarPadding,
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: gridViewMaxCrossAxisExtent,
              childAspectRatio: gridViewChildAspectRatio,
              crossAxisSpacing: gridViewCrossAxisSpacing,
              mainAxisSpacing: gridViewMainAxisSpacing),
          itemBuilder: (context, index) {
            return InkWell(
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(imageBorderRadius),
                ),
                elevation: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildRecipeImage(imageBorderRadius, imageAspectRatio,
                        controller.listCookbookUIModel[index].imageUrl),
                    _buildRecipeName(controller.listCookbookUIModel[index].name)
                  ],
                ),
              ),
            );
          },
          itemCount: controller.listCookbookUIModel.length,
        ));
  }

  Expanded _buildRecipeName(String name) {
    return Expanded(
      flex: 2,
      child: Center(
        child: Text(
          name,
          style: appTextTheme.bodyText2,
        ),
      ),
    );
  }

  ClipRRect _buildRecipeImage(
      double imageBorderRadius, double imageAspectRatio, String imageUrl) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(imageBorderRadius),
        topRight: Radius.circular(imageBorderRadius),
      ),
      child: AspectRatio(
        aspectRatio: imageAspectRatio,
        child:
            CacheNetworkImageCustom(urlImage: imageUrl, boxFit: BoxFit.cover),
      ),
    );
  }
}
