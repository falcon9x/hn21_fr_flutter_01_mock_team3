import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/recipe_feed/recipe_feed_view_model/recipe_feed_view_model.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';

class CustomOtherProfileAppBar {
  final controller = Get.find<RecipeFeedViewModel>();
  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      actions: [
        Padding(
          padding: AppPadding.appBarPaddingOnlyRight,
          child: IconButton(
              onPressed: () => _goToMoreOptions(),
              icon: SvgPicture.asset(IconBlack.iconMoreMenu)),
        )
      ],
      leadingWidth: 0,
      leading: const Text(""),
      title: InkWell(
        onTap: () => _onBack(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(IconBlack.iconLeft),
            Text(
              OtherUserProfileString.titleAppBar,
              style: appTextTheme.caption,
            )
          ],
        ),
      ),
    );
  }

  void _goToMoreOptions() {
    //TODO
  }
  void _onBack() {
    controller.appBarSelected.value = 0;
    controller.iconSelected.value = 0;
    Get.back();
  }
}
