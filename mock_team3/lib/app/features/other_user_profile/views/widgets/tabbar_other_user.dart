import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../view_model/other_user_profile_view_model.dart';
import 'other_follow_tab.dart';
import 'other_recipe_tab.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../utils/number_format.dart';
import '../../../../widgets/custom_tab_bar_indicator.dart';

class TabBarOtherUser extends GetView<OtherUserProfileViewModel> {
  const TabBarOtherUser({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Column(
        children: [
          _buildTabBar(),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: AppColors.colorDivider, width: 0.5),
                ),
              ),
              child: const TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: [
                  OtherRecipesTab(),
                  OtherFollowTab(),
                ],
              ),
            ),
          ),
        ],
      ),
      length: 2,
    );
  }

  TabBar _buildTabBar() {
    return TabBar(
      onTap: (value) {
        controller.updateIndexTabBar(value);
      },
      indicator: CircleTabIndicator(
        color: AppColors.mainColor,
        radius: 100,
        indicatorHeight: 4,
      ),
      tabs: [
        Tab(
          height: kToolbarHeight,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(
                () => Text(
                  NumberFormat.numberToString(controller.otherUser.value.cookbooks?.length ?? 0),
                  style: controller.indexTabBar.value == 0
                      ? appTextTheme.headline4
                      : appTextTheme.headline4!
                          .copyWith(color: AppColors.colorBlackOpacity),
                ),
              ),
              Obx(
                () => Text(
                  UserProfileString.titleTabBar0,
                  style: controller.indexTabBar.value == 0
                      ? appTextTheme.bodyText1
                      : appTextTheme.bodyText1!
                          .copyWith(color: AppColors.colorBlackOpacity),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
        Tab(
          height: kToolbarHeight,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(
                () => Text(
                  NumberFormat.numberToString(controller.otherUser.value.saved?.length ?? 0),
                  style: controller.indexTabBar.value == 1
                      ? appTextTheme.headline4
                      : appTextTheme.headline4!
                          .copyWith(color: AppColors.colorBlackOpacity),
                ),
              ),
              Obx(
                () => Text(
                  UserProfileString.titleTabBar2,
                  style: controller.indexTabBar.value == 1
                      ? appTextTheme.bodyText1
                      : appTextTheme.bodyText1!
                          .copyWith(color: AppColors.colorBlackOpacity),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
