import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/features/other_user_profile/view_model/other_user_profile_view_model.dart';
import 'package:mock_team3/app/features/other_user_profile/views/widgets/other_user_info.dart';
import 'package:mock_team3/app/features/other_user_profile/views/widgets/tabbar_other_user.dart';
import 'package:mock_team3/app/widgets/app_rounded_button.dart';
import 'package:mock_team3/generated/locales.g.dart';

import '../../../core/app_padding.dart';

class OtherUserProfileScreen extends GetView<OtherUserProfileViewModel> {
  const OtherUserProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: AppPadding.defaultPadding,
      child: Column(
        children: [
          _buildUserInfo(),
          const Spacer(),
          _buildButtonFollow(),
          _buildDivider(),
          _buildTabBar()
        ],
      ),
    );
  }

  Flexible _buildTabBar() {
    return const Flexible(
      flex: 30,
      child: TabBarOtherUser(),
    );
  }

  Flexible _buildDivider() {
    return Flexible(
      flex: 2,
      child: Center(
        child: Divider(
          thickness: 2,
          color: AppColors.colorDivider,
          height: 1,
        ),
      ),
    );
  }

  Flexible _buildButtonFollow() {
    return Flexible(
      flex: 4,
      child: AppRoundedButton(
        text: LocaleKeys.otherUserProfileString_textButton.tr,
        isCircle: false,
        width: double.infinity,
      ),
    );
  }

  Expanded _buildUserInfo() {
    return const Expanded(
      flex: 5,
      child: OtherUserInfo(
        hasMoreOption: false,
      ),
    );
  }

}
