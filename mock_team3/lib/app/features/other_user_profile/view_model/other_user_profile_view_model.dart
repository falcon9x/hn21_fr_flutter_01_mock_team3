import 'dart:developer';

import 'package:get/get.dart';
import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/data/user/model/user_model.dart';
import 'package:mock_team3/data/user/user_repository.dart';

import '../../../../data/base/data_result.dart';
import '../../../../data/cook_book/model/cook_book_model.dart';
import '../../../../data/cook_book/model/dto/cook_book_ui_model.dart';
import '../../../../data/recipe/model/recipe_model.dart';
import '../../../../data/user/dto/user_follow_ui_model.dart';

class OtherUserProfileViewModel extends BaseViewModel
     {
  final UserRepository _userRepository;
  final indexTabBar = 0.obs;
  final otherUser = UserModel.withInitial().obs;
  final otherUserAvatar = "".obs;
  RxList<CookBookModel> listCookbook = RxList();
  RxList<UserFollowUIModel> listUserFollowUIModel = RxList();
  RxList<CookBookUIModel> listCookbookUIModel = RxList();

  OtherUserProfileViewModel(this._userRepository);

  void getUserModel(String userId) async {
    DataResult result = await _userRepository.getUserProfile(userId);
    if (result.isSuccess) {
      otherUser.value = (result.data as UserModel);
      otherUserAvatar.value =
          await _getImageDownloadUrl(otherUser.value.avatar ?? "");
      await _getListCookbook(otherUser.value.cookbooks);
      await _getListRecipe(listCookbook);
    } else {
      if (result is APIFailure) {
        if ((result as APIFailure).errorResponse == "data null") {

        } else {
        }
      }
    }
  }

  Future<String> _getImageDownloadUrl(String imagePath) async {
    DataResult result = await _userRepository.getImageFromStorage(imagePath);
    if (result.isSuccess) {
      return result.data;
    } else {
      //TODO
      return "";
    }
  }

  Future<void> _getListCookbook(List<String>? cookbooks) async {
    DataResult result =
        await _userRepository.getListCookbook(cookbooks ?? List.empty());
    if (result.isSuccess) {
      listCookbook.value = (result.data as List<CookBookModel>);
      log("LAMKZZ ${listCookbook.length}");
    } else {
      if (result is APIFailure) {}
    }
  }

  Future<void> _getListRecipe(RxList<CookBookModel> listCookbook) async {
    RecipeModel recipeModel;
    for (int indexCookbook = 0;
        indexCookbook < listCookbook.length;
        indexCookbook++) {
      DataResult dataResult = await _userRepository
          .getRecipeById(listCookbook[indexCookbook].recipes![0]);
      if (dataResult.isSuccess) {
        recipeModel = dataResult.data;
        listCookbookUIModel.add(CookBookUIModel(
            id: otherUser.value.cookbooks![indexCookbook],
            name: listCookbook[indexCookbook].name,
            imageUrl: await _getImageDownloadUrl(
                recipeModel.galleries![recipeModel.indexCover])));
      } else {}
    }
  }

  void updateIndexTabBar(int indexTabBarNew) {
    indexTabBar.value = indexTabBarNew;
    if (indexTabBar.value == 1) {
      if (listUserFollowUIModel.isEmpty) {
        _getListUserFollow();
      }
    }
  }

  void _getListUserFollow() async {
    DataResult result =
        await _userRepository.getListUserFollow(otherUser.value.following!);
    if (result.isSuccess) {
      List<UserModel> listUserModel = result.data;
      for (int indexList = 0; indexList < listUserModel.length; indexList++) {
        listUserFollowUIModel.add(UserFollowUIModel(
            uid: otherUser.value.following![indexList],
            name: listUserModel[indexList].account.fullname,
            avatar: await _getImageDownloadUrl(
                listUserModel[indexList].avatar ?? "")));
      }
    } else {}
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getUserModel(Get.arguments);
  }
}
