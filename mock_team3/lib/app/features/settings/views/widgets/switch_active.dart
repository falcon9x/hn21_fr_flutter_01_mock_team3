import 'package:flutter/material.dart';

import '../../../../core/app_colors.dart';

class SwitcherActive extends StatelessWidget {
  const SwitcherActive({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      key: const ValueKey("active"),
      children: [
        Container(
          width: 36,
          height: 20,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(1000),
            ),
            color: AppColors.mainColor,
          ),
        ),
        const Positioned(
          top: 0,
          left: 0,
          bottom: 0,
          child: Icon(
            Icons.circle,
            color: Colors.white,
            size: 20,
          ),
        )
      ],
    );
  }
}

