import 'package:flutter/material.dart';

import 'switch_active.dart';
import 'switch_inactive.dart';

class CustomSwitcher extends StatelessWidget {
  const CustomSwitcher({
    Key? key,
    required this.onTap,
    required this.valueSwitch,
  }) : super(key: key);

  final VoidCallback onTap;
  final bool valueSwitch;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: AnimatedSwitcher(

        duration: const Duration(milliseconds: 300),
        child: valueSwitch ? const SwitcherActive() : const SwitcherInActive(),
      )
    );
  }
}
