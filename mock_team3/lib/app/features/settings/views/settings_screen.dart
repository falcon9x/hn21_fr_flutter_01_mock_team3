import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/settings/view_model/settings_view_model.dart';
import 'package:mock_team3/generated/locales.g.dart';

import '../../../core/app_assets.dart';
import '../../../core/app_colors.dart';
import '../../../core/app_padding.dart';
import '../../../core/app_text_theme.dart';
import 'widgets/custom_switch.dart';

class SettingsScreen extends GetView<SettingViewModel> {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BorderRadius borderRadiusSubTitle = BorderRadius.circular(8);
    EdgeInsetsGeometry paddingSubTitle = const EdgeInsets.all(10);
    return Scaffold(
      appBar: _buildAppBar(),
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(minHeight: Get.height),
          child: Padding(
            padding: AppPadding.defaultPadding,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildHeader(),
                _buildTitleGroup1(),
                _buildGroup1Title1(),
                _buildGroup1Title2(),
                _buildGroup1Title3(),
                _buildDivider(),
                _buildTitleGroup2(),
                _buildGroup2Title1(),
                _buildGroup2SubTitle1(borderRadiusSubTitle, paddingSubTitle),
                _buildGroup2Title2(),
                _buildDivider(),
                _buildFooter()
              ],
            ),
          ),
        ),
      ),
    );
  }

  InkWell _buildFooter() {
    return InkWell(
      onTap: () => _changePassword(),
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          LocaleKeys.settings_titleFooter.tr,
          style: appTextTheme.bodyText1,
        ),
        trailing: SvgPicture.asset(IconBlack.iconRight),
      ),
    );
  }

  ListTile _buildGroup2Title2() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        LocaleKeys.settings_titleFirstGroupSecondTitle.tr,
        style: appTextTheme.bodyText1,
      ),
      trailing: Obx(
        () => CustomSwitcher(
          onTap: () => controller.updateIsFollowersCanSeeProfilesIFollow(),
          valueSwitch: controller.isFollowersCanSeeProfilesIFollow.value,
        ),
      ),
    );
  }

  Ink _buildGroup2SubTitle1(
      BorderRadius borderRadiusSubTitle, EdgeInsetsGeometry paddingSubTitle) {
    return Ink(
      decoration: BoxDecoration(
          borderRadius: borderRadiusSubTitle, color: AppColors.colorDivider),
      child: Padding(
        padding: paddingSubTitle,
        child: RichText(
          text: TextSpan(
            text: LocaleKeys.settings_titleSecondGroupFirstSubTitle.tr,
            style: appTextTheme.caption,
            children: [
              TextSpan(
                  text: LocaleKeys.settings_titleSecondGroupSecondSubTitle.tr,
                  style: appTextTheme.caption!.copyWith(
                      color: AppColors.mainColor, fontWeight: FontWeight.w700),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () => _onShowReason())
            ],
          ),
        ),
      ),
    );
  }

  ListTile _buildGroup2Title1() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        LocaleKeys.settings_titleSecondGroupFirstTitle.tr,
        style: appTextTheme.bodyText1,
      ),
      trailing: Obx(
        () => CustomSwitcher(
          onTap: () => controller.updateIsFollowersCanSeeMySavedRecipes(),
          valueSwitch: controller.isFollowersCanSeeMySavedRecipes.value,
        ),
      ),
    );
  }

  ListTile _buildTitleGroup2() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        LocaleKeys.settings_titleSecondGroup.tr,
        style: appTextTheme.bodyText2!.copyWith(color: AppColors.textGrey),
      ),
    );
  }

  Padding _buildDivider() {
    return Padding(
      padding: AppPadding.defaultPadding,
      child: InkWell(
        borderRadius: BorderRadius.circular(0.5),
        child: Divider(
          thickness: 1,
          color: AppColors.colorDivider,
        ),
      ),
    );
  }

  ListTile _buildGroup1Title3() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        LocaleKeys.settings_titleFirstGroupThirdTitle.tr,
        style: appTextTheme.bodyText1,
      ),
      trailing: Obx(
        () => CustomSwitcher(
            onTap: () => controller.updateIsWhenSomeoneDoLiveCooking(),
            valueSwitch: controller.isWhenSomeoneDoLiveCooking.value),
      ),
    );
  }

  ListTile _buildGroup1Title2() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        LocaleKeys.settings_titleFirstGroupSecondTitle.tr,
        style: appTextTheme.bodyText1,
      ),
      trailing: Obx(
        () => CustomSwitcher(
            onTap: () => controller.updateIsWhenSomeoneSendMeAMessage(),
            valueSwitch: controller.isWhenSomeoneSendMeAMessage.value),
      ),
    );
  }

  ListTile _buildGroup1Title1() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        LocaleKeys.settings_titleFirstGroupFirstTitle.tr,
        style: appTextTheme.bodyText1,
      ),
      trailing: Obx(
        () => CustomSwitcher(
            onTap: () => controller.updateIsNotifyMeForFollowers(),
            valueSwitch: controller.isNotifyMeForFollowers.value),
      ),
    );
  }

  ListTile _buildTitleGroup1() {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        LocaleKeys.settings_titleFirstGroup.tr,
        style: appTextTheme.bodyText2!.copyWith(color: AppColors.textGrey),
      ),
    );
  }

  Text _buildHeader() {
    return Text(
      LocaleKeys.settings_titleHeader.tr,
      style: appTextTheme.headline3,
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      elevation: 0,
      leading: const Text(""),
      backgroundColor: Colors.transparent,
      leadingWidth: 0,
      title: InkWell(
        onTap: () => _backToLastScreen(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(IconBlack.iconLeft),
            Text(
              LocaleKeys.settings_titleLeadingAppBar.tr,
              style: appTextTheme.caption,
            )
          ],
        ),
      ),
      actions: [
        Padding(
          padding: AppPadding.appBarPaddingOnlyRight,
          child: InkWell(
            onTap: () => controller.logOut(),
            child: Row(
              children: [
                SvgPicture.asset(IconBlack.iconLogout),
                Text(
                  LocaleKeys.settings_titleActionAppBar.tr,
                  style: appTextTheme.headline5!
                      .copyWith(color: AppColors.mainColor),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  // void _logOut() {}

  void _onShowReason() {}

  void _changePassword() {}

  void _backToLastScreen() {
    Get.back();
  }
}

class SwitchListTile extends StatelessWidget {
  const SwitchListTile({
    Key? key,
    required this.controller,
    required this.title,
  }) : super(key: key);

  final SettingViewModel controller;
  final String title;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: appTextTheme.bodyText1,
      ),
      trailing: Obx(
        () => CustomSwitcher(
          onTap: () => controller.updateIsFollowersCanSeeProfilesIFollow(),
          valueSwitch: controller.isFollowersCanSeeProfilesIFollow.value,
        ),
      ),
    );
  }
}
