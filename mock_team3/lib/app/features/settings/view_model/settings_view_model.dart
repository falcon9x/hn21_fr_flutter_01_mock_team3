import 'dart:developer';

import 'package:get/get.dart';
import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/app/routes/app_routes.dart';
import 'package:mock_team3/app/utils/share_prefs_key.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/settings/model/setting_model.dart';
import 'package:mock_team3/data/user/user_repository.dart';

class SettingViewModel extends BaseViewModel {
  final isNotifyMeForFollowers = false.obs;
  final isWhenSomeoneSendMeAMessage = true.obs;
  final isWhenSomeoneDoLiveCooking = true.obs;
  final isFollowersCanSeeMySavedRecipes = true.obs;
  final isFollowersCanSeeProfilesIFollow = true.obs;
  String idUserCurrent = "";
  final UserRepository _userRepository;
  final SharePrefsRepository _sharePrefsRepository;

  SettingViewModel(this._sharePrefsRepository, this._userRepository);

  void updateIsNotifyMeForFollowers() {
    isNotifyMeForFollowers.value = !isNotifyMeForFollowers.value;
    _sharePrefsRepository.saveBool(
        SharePrefsKey.isNotifyMeForFollowers, isNotifyMeForFollowers.value);
  }

  void updateIsWhenSomeoneSendMeAMessage() {
    isWhenSomeoneSendMeAMessage.value = !isWhenSomeoneSendMeAMessage.value;
    _sharePrefsRepository.saveBool(
        SharePrefsKey.isWhenSomeoneSendMeAMessage,
        isWhenSomeoneSendMeAMessage.value);
  }

  void updateIsWhenSomeoneDoLiveCooking() {
    isWhenSomeoneDoLiveCooking.value = !isWhenSomeoneDoLiveCooking.value;
    _sharePrefsRepository.saveBool(
        SharePrefsKey.isWhenSomeoneDoLiveCooking,
        isWhenSomeoneDoLiveCooking.value);
  }

  void updateIsFollowersCanSeeMySavedRecipes() {
    isFollowersCanSeeMySavedRecipes.value =
    !isFollowersCanSeeMySavedRecipes.value;
    _sharePrefsRepository.saveBool(
        SharePrefsKey.isFollowersCanSeeMySavedRecipes,
        isFollowersCanSeeMySavedRecipes.value);
  }

  void updateIsFollowersCanSeeProfilesIFollow() {
    isFollowersCanSeeProfilesIFollow.value =
    !isFollowersCanSeeProfilesIFollow.value;
    _sharePrefsRepository.saveBool(
        SharePrefsKey.isFollowersCanSeeProfilesIFollow,
        isFollowersCanSeeProfilesIFollow.value);
  }

  @override
  void onInit() {
    super.onInit();
    idUserCurrent = _sharePrefsRepository.getString(SharePrefsKey.userId);
    isNotifyMeForFollowers.value =
        _sharePrefsRepository.getBool(SharePrefsKey.isNotifyMeForFollowers);
    isWhenSomeoneSendMeAMessage.value =
        _sharePrefsRepository.getBool(
            SharePrefsKey.isWhenSomeoneSendMeAMessage);
    isWhenSomeoneDoLiveCooking.value =
        _sharePrefsRepository.getBool(SharePrefsKey.isWhenSomeoneDoLiveCooking);
    isFollowersCanSeeMySavedRecipes.value =
        _sharePrefsRepository.getBool(
            SharePrefsKey.isFollowersCanSeeMySavedRecipes);
    isFollowersCanSeeProfilesIFollow.value =
        _sharePrefsRepository.getBool(
            SharePrefsKey.isFollowersCanSeeProfilesIFollow);
  }

  void logOut() {
    _sharePrefsRepository.saveBool(SharePrefsKey.keepLogIn, false);
    Get.offAllNamed(AppRoute.loginScreen);
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
    _updateUserSettings();
  }
  Future<void> _updateUserSettings() async{
    DataResult result = await _userRepository.updateUserSettings(idUserCurrent, SettingModel(
        isNotifyMeForFollowers: isNotifyMeForFollowers.value,
        isWhenSomeOneSendMeAMessage: isWhenSomeoneSendMeAMessage.value,
        isSomeOneLiveCook: isWhenSomeoneDoLiveCooking.value,
        isFollowersCanSeeMyRecipes: isFollowersCanSeeMySavedRecipes.value,
        isFollowersCanSeeFollowings: isFollowersCanSeeProfilesIFollow.value));
    if(result.isSuccess){
      log("LAMKZZZ SUCCESS");
    }else{
      log("LAMKZZZ ${(result.error as APIFailure).errorResponse}");
    }
  }
}
