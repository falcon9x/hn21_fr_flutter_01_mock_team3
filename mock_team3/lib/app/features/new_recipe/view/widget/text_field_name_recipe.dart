import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';

class TextFieldNameRecipe extends StatelessWidget {
  const TextFieldNameRecipe({Key? key, required this.widthScreenFigma})
      : super(key: key);
  final double widthScreenFigma;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width * (200 / widthScreenFigma),
      child: TextFormField(
        style: appTextTheme.bodyText1,
        cursorColor: AppColors.colorGreyWhite,
        decoration: InputDecoration(
          hintText: StringNewRecipe.txtHintNameRecipe,
          hintStyle:
              appTextTheme.bodyText1?.copyWith(overflow: TextOverflow.clip),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.colorGreyWhite)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppColors.colorGreyWhite)),
        ),
      ),
    );
  }
}
