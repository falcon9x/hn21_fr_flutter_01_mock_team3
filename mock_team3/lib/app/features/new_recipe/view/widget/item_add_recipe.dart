import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_text_theme.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../widgets/container_border_dash.dart';

class ItemAddRecipe extends StatelessWidget {
  const ItemAddRecipe({
    Key? key,
    required double paddingHorizontal,
    required this.heightScreen,
    required double heightScreenFigma,
    required this.widthScreen,
    required this.title,
    required this.hint,
  })  : _paddingHorizontal = paddingHorizontal,
        _heightScreenFigma = heightScreenFigma,
        super(key: key);

  final double _paddingHorizontal;
  final double heightScreen;
  final double _heightScreenFigma;
  final double widthScreen;
  final String title;
  final String hint;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          left: _paddingHorizontal,
          right: _paddingHorizontal,
          bottom: Get.height * (20 / _heightScreenFigma)),
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          children: [
            Row(
              children: [
                Text(title,
                    overflow: TextOverflow.ellipsis,
                    style: appTextTheme.headline5?.copyWith()),
                const Spacer(),
                SvgPicture.asset(IconBlack.iconEdit)
              ],
            ),
            SizedBox(
              height: heightScreen * (20 / _heightScreenFigma),
            ),
            ContainerBorderDash(
                boxHeight: 50,
                boxWidth: widthScreen,
                circularValue: 5,
                thickness: 2,
                color: AppColors.colorGrey.withOpacity(0.5),
                child: Row(
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: SvgPicture.asset(
                        IconBlack.iconAdd,
                        color: AppColors.colorGrey,
                        fit: BoxFit.scaleDown,
                      ),
                    ),
                    SizedBox(
                      width: widthScreen * (220 / 375),
                      child: Text(
                        hint,
                        overflow: TextOverflow.ellipsis,
                        style: appTextTheme.bodyText1?.copyWith(
                            color: AppColors.colorGrey.withOpacity(0.5)),
                      ),
                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
