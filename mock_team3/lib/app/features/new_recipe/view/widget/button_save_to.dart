import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mock_team3/app/core/app_text_theme.dart';
import 'package:get/get.dart';

import '../../../../core/app_assets.dart';
import 'button_save_recipe.dart';

class ButtonSaveTo extends StatelessWidget {
  const ButtonSaveTo({Key? key, required this.paddingHorizontal})
      : super(key: key);
  final double paddingHorizontal;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          left: paddingHorizontal, right: paddingHorizontal, top: 10),
      child: Row(
        children: [
          InkWell(
            onTap: () {},
            child: Container(
              width: Get.width * (180 / 375),
              height: 50,
              padding: EdgeInsets.symmetric(horizontal: Get.width * (15 / 375)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                children: [
                  Text(
                    "Western (5)",
                    style: appTextTheme.bodyText1,
                  ),
                  const Spacer(),
                  SvgPicture.asset(IconBlack.iconArrowDown)
                ],
              ),
            ),
          ),
          const Spacer(),
          const ButtonSaveRecipe()
        ],
      ),
    );
  }
}
