import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';

class ButtonSaveRecipe extends StatelessWidget {
  const ButtonSaveRecipe({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {},
      child: Text(
        StringNewRecipe.btnSaveRecipe,
        style: appTextTheme.headline5?.copyWith(color: AppColors.mainColor),
      ),
      style: OutlinedButton.styleFrom(
        minimumSize: Size(Get.width * (120 / 375), 50),
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        side: BorderSide(width: 3, color: AppColors.mainColor),
      ),
    );
  }
}
