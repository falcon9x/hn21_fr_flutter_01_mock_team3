import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_assets.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/core/app_strings.dart';
import 'package:mock_team3/app/core/app_text_theme.dart';
import 'package:mock_team3/app/features/new_recipe/view/widget/button_save_to.dart';
import 'package:mock_team3/app/features/new_recipe/view_models/new_recipe_view_model.dart';
import 'package:mock_team3/app/widgets/container_border_dash.dart';

import '../../../core/app_text_theme.dart';
import 'widget/item_add_recipe.dart';
import 'widget/text_field_name_recipe.dart';

class NewRecipeScreen extends GetWidget<NewRecipeViewModel> {
  const NewRecipeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _heightScreenFigma = 1013;
    double _widthScreenFigma = 375;
    double widthScreen = Get.width;
    double heightScreen = Get.height;
    double _paddingHorizontal = Get.width * (25 / _widthScreenFigma);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: heightScreen * (57 / _heightScreenFigma),
            ),
            _buildAppBarBack(_paddingHorizontal),
            _buildTextFieldNameRecipe(
                heightScreen, _heightScreenFigma, _paddingHorizontal),
            _buildAddRecipeName(_paddingHorizontal, heightScreen,
                _heightScreenFigma, widthScreen, _widthScreenFigma),
            SizedBox(
              height: heightScreen * (20 / _heightScreenFigma),
            ),
            ItemAddRecipe(
              paddingHorizontal: _paddingHorizontal,
              heightScreen: heightScreen,
              heightScreenFigma: _heightScreenFigma,
              widthScreen: widthScreen,
              title: StringNewRecipe.txtGallery,
              hint: StringNewRecipe.txtHintGallery,
            ),
            ItemAddRecipe(
              paddingHorizontal: _paddingHorizontal,
              heightScreen: heightScreen,
              heightScreenFigma: _heightScreenFigma,
              widthScreen: widthScreen,
              title: StringNewRecipe.txtIngredients,
              hint: StringNewRecipe.txtHintIngredients,
            ),
            ItemAddRecipe(
              paddingHorizontal: _paddingHorizontal,
              heightScreen: heightScreen,
              heightScreenFigma: _heightScreenFigma,
              widthScreen: widthScreen,
              title: StringNewRecipe.txtHowToCook,
              hint: StringNewRecipe.txtHintHowToCook,
            ),
            ItemAddRecipe(
              paddingHorizontal: _paddingHorizontal,
              heightScreen: heightScreen,
              heightScreenFigma: _heightScreenFigma,
              widthScreen: widthScreen,
              title: StringNewRecipe.txtAdditionalInfo,
              hint: StringNewRecipe.txtHintAdditionalInfo,
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: heightScreen * (30 / _heightScreenFigma),
                  left: _paddingHorizontal),
              child: Text(
                StringNewRecipe.txtSaveTo,
                style: appTextTheme.bodyText2
                    ?.copyWith(color: AppColors.colorGrey),
              ),
            ),
            ButtonSaveTo(paddingHorizontal: _paddingHorizontal),
            _buildButtonPost(_paddingHorizontal, heightScreen,
                _heightScreenFigma, widthScreen, context)
          ],
        ),
      ),
    );
  }

  Padding _buildTextFieldNameRecipe(
    double heightScreen,
    double _heightScreenFigma,
    double _paddingHorizontal,
  ) {
    return Padding(
      padding: EdgeInsets.only(
          top: heightScreen * (20 / _heightScreenFigma),
          left: _paddingHorizontal),
      child: Text(
        StringNewRecipe.txtNewRecipe,
        style: appTextTheme.headline3,
      ),
    );
  }

  Padding _buildButtonPost(double _paddingHorizontal, double heightScreen,
      double _heightScreenFigma, double widthScreen, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          right: _paddingHorizontal,
          left: _paddingHorizontal,
          bottom: heightScreen * (30 / _heightScreenFigma),
          top: heightScreen * (30 / _heightScreenFigma)),
      child: SizedBox(
        width: widthScreen,
        height: 50,
        child: ElevatedButton(
          onPressed: () {},
          child: Text(
            StringNewRecipe.btnPostToFeed,
            style:
                appTextTheme.headline5?.copyWith(color: AppColors.colorWhite),
          ),
          style: ElevatedButton.styleFrom(primary: AppColors.colorGreyWhite),
        ),
      ),
    );
  }

  Padding _buildAddRecipeName(
    double _paddingHorizontal,
    double heightScreen,
    double _heightScreenFigma,
    double widthScreen,
    double _widthScreenFigma,
  ) {
    return Padding(
      padding: EdgeInsets.only(
          left: _paddingHorizontal,
          right: _paddingHorizontal,
          top: heightScreen * (35 / _heightScreenFigma)),
      child: Row(
        children: [
          ContainerBorderDash(
              boxHeight: 60,
              boxWidth: 60,
              circularValue: 5,
              thickness: 2,
              color: AppColors.colorGrey,
              child: SvgPicture.asset(
                IconBlack.iconAdd,
                fit: BoxFit.scaleDown,
              )),
          SizedBox(
            width: widthScreen * (15 / _widthScreenFigma),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                StringNewRecipe.txtTitleRecipeName,
                style: appTextTheme.subtitle1,
              ),
              TextFieldNameRecipe(
                widthScreenFigma: _widthScreenFigma,
              ),
            ],
          )
        ],
      ),
    );
  }

  Padding _buildAppBarBack(double _paddingHorizontal) {
    return Padding(
      padding: EdgeInsets.only(left: _paddingHorizontal),
      child: InkWell(
        onTap: () {
          _backToProfile();
        },
        child: Row(
          children: [
            SvgPicture.asset(IconBlack.iconLeft),
            Text(
              StringNewRecipe.txtTitleAppBar,
              style: appTextTheme.caption,
            )
          ],
        ),
      ),
    );
  }

  void _backToProfile() {
    Get.back();
  }
}
