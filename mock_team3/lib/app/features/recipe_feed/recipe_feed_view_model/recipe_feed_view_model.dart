import 'dart:developer';

import 'package:get/get.dart';
import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/app/utils/share_prefs_key.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/cook_book/model/cook_book_model.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';
import 'package:mock_team3/data/recipe/recipe_repository.dart';
import 'package:mock_team3/data/recipe_feed/model/cook_book.dart';
import 'package:mock_team3/data/recipe_feed/model/recipe_feed_model.dart';
import 'package:mock_team3/data/user/model/user_model.dart';

import '../../../../data/base/data_result.dart';
import '../../../../data/recipe/model/recipe_model.dart';
import '../../../../data/recipe_feed/model/icon_bottom_bar.dart';

class RecipeFeedViewModel extends BaseViewModel {
  final RecipeRepository _recipeRepository;
  final FirebaseRepository _firebaseRepository;
  final SharePrefsRepository _sharePrefsRepository;

  RecipeFeedViewModel(
    this._recipeRepository,
    this._firebaseRepository,
    this._sharePrefsRepository,
  );

  var listIconBottomBar = [].obs;
  var iconSelected = 1.obs;
  var changeStatusListView = false.obs;
  var listAppBar = [].obs;
  var listBody = [].obs;
  var selected = (-1).obs;
  var appBarSelected = 1.obs;
  final _listRecipeFeed = <RecipeFeedModel>[].obs;
  final _listLinkImageRecipe = <String>[].obs;
  final _listLinkImageAvatar = [].obs;
  List<String> _listCookBookName = [];
  var listCookBookFinal = <CookBook>[].obs;
  var listNameCookBookFinal = [];

  get listCookBookName => _listCookBookName;

  set listCookBookName(value) {
    _listCookBookName = value;
  }

  @override
  void onInit() {
    super.onInit();
    _getListIconBottom();
    _getListCookBook();
    _getUsers();
    _getListCookBook();
  }

  void _getListIconBottom() {
    for (int i = 0; i < 3; i++) {
      IconBottomBar icon = IconBottomBar("", -1);
      icon.linkAsset = "assets/icons/black/nav${i + 1}.svg";
      icon.index = i;
      listIconBottomBar.add(icon);
    }
  }

  List<String> userId = [];

  void _getListCookBook() async {
    DataResult dataResult = await _firebaseRepository.getQuerySnapshot("user");
    if (dataResult.isSuccess) {
      for (var doc in dataResult.data.docs) {
        userId.add(doc.id);
      }
    }
    var response = await _recipeRepository
        .getUserByID(_sharePrefsRepository.getString(SharePrefsKey.userId));
    if (response.isSuccess) {
      if (response.data != null) {
        await _getListIDCookBookSuccess(response.data!);
        await _getListCookBookFinal(_listCookBookName);
      } else {
        //TODO
      }
    } else if (response.isFailure) {
      var error = response.error;
      if (error is APIFailure) {
        _setListCookBookError(error, error.errorResponse);
      }
    }
  }

  Future<List<String>?> _getListIDCookBookSuccess(UserModel userModel) async {
    List<String>? listIDCookBook = [];
    for (int idCookBookIndex = 0;
        idCookBookIndex < userModel.cookbooks!.length;
        idCookBookIndex++) {
      log(userModel.cookbooks!.length.toString());
      listIDCookBook.add(userModel.cookbooks![idCookBookIndex]);
    }
    log(listIDCookBook.toString());
    var listNameCookBook = [];
    for (int cookBookNameIndex = 0;
        cookBookNameIndex < listIDCookBook.length;
        cookBookNameIndex++) {
      var responseCookBookByID = await _recipeRepository
          .getCookBookNameByID(listIDCookBook[cookBookNameIndex]);
      if (responseCookBookByID.isSuccess) {
        if (responseCookBookByID.data != null) {
          listNameCookBook.add(responseCookBookByID.data!.name);

          listNameCookBookFinal = listNameCookBook;
        } else {
          //TODO
        }
      } else if (responseCookBookByID.isFailure) {
        var error = responseCookBookByID.error;
        if (error is APIFailure) {
          _setListCookBookError(error, error.errorResponse);
        }
      }
    }

    _listCookBookName = listIDCookBook;
    return _listCookBookName;
  }

  void _getUsers() async {
    var response = await _recipeRepository.getUser();
    if (response.isSuccess) {
      if (response.data != null) {
        await _getListUserSuccess(response.data!);
      } else {
        //TODO
      }
    } else if (response.isFailure) {
      var error = response.error;
      if (error is APIFailure) {
        _setListUserError(error, error.errorResponse);
      }
    }
  }

  Future _getListUserSuccess(List<UserModel> data) async {
    UserModel userModel;
    for (int userIndex = 0; userIndex < data.length; userIndex++) {
      userModel = data[userIndex];
      if (userModel.avatar != null) {
      } else {}
      if (userModel.cookbooks != null) {
        for (int cookBookIndex = 0;
            cookBookIndex < userModel.cookbooks!.length;
            cookBookIndex++) {
          CookBookModel cookBookModel = CookBookModel(name: "");
          cookBookModel =
              await _getCookBookByID(userModel.cookbooks![cookBookIndex]);
          if (cookBookModel.recipes != null) {
            for (int recipeIndex = 0;
                recipeIndex < cookBookModel.recipes!.length;
                recipeIndex++) {
              RecipeFeedModel recipeFeedModel =
                  RecipeFeedModel("", "", "", "", 0, 0);
              RecipeModel recipeModel =
                  RecipeModel(name: "", likes: 0, comments: 0, indexCover: 0);
              recipeModel =
                  await getRecipeByID(cookBookModel.recipes![recipeIndex]);
              String? linkAvatar =
                  await _getLinkImageFromStore(userModel.avatar!);
              _listLinkImageAvatar.add(linkAvatar!);

              String? linkImageRecipe =
                  await _getLinkImageFromStore(recipeModel.galleries![0]);
              _listLinkImageRecipe.add(linkImageRecipe!);
              recipeFeedModel.nameUser = userModel.account.fullname;
              recipeFeedModel.like = recipeModel.likes;
              recipeFeedModel.comment = recipeModel.comments;
              recipeFeedModel.nameRecipe = recipeModel.name;
              _listRecipeFeed.add(recipeFeedModel);
            }
          }
        }
      }
    }
  }

  Future<CookBookModel> _getCookBookByID(String cookBookID) async {
    var responseCookBookModel =
        await _recipeRepository.getCookBookNameByID(cookBookID);
    CookBookModel cookBookModel;
    if (responseCookBookModel.isSuccess) {
      if (responseCookBookModel.data != null) {
        cookBookModel = responseCookBookModel.data!;
        return cookBookModel;
      } else {
        return CookBookModel.withInitial();
      }
    }
    return CookBookModel.withInitial();
  }

  void _setListUserError(APIFailure error, String errorResponse) {}

  Future<RecipeModel> getRecipeByID(String recipeID) async {
    var responseRecipe = await _recipeRepository.getRecipeByID(recipeID);
    RecipeModel recipeModel;
    if (responseRecipe.isSuccess) {
      if (responseRecipe.data != null) {
        recipeModel = responseRecipe.data!;
        return recipeModel;
      } else {
        return RecipeModel.withInitial();
      }
    }
    return RecipeModel.withInitial();
  }

  Future<String?> _getLinkImageFromStore(String fileNameAvatar) async {
    var linkAvatar =
        await _recipeRepository.getImageFromStorage(fileNameAvatar);
    if (linkAvatar.isSuccess) {
      if (linkAvatar.data!.isNotEmpty) {
        return linkAvatar.data;
      } else if (linkAvatar.data!.isEmpty) {}
    }
    return null;
  }

  get listLinkImageAvatar => _listLinkImageAvatar;

  get listRecipeFeed => _listRecipeFeed;

  get listLinkImageRecipe => _listLinkImageRecipe;

  void _setListCookBookError(APIFailure error, String errorResponse) {}

  Future _getListCookBookFinal(List<String> idCookBooks) async {
    listCookBookFinal.clear();
    for (int i = 0; i < idCookBooks.length; i++) {
      CookBook cookBook = CookBook("", 0);
      cookBook.name = listNameCookBookFinal[i];
      cookBook.index = i;
      listCookBookFinal.add(cookBook);
    }
  }

  void changeBottomTab(int index) {
    appBarSelected.value = index;
    if (index == 1 && iconSelected.value == 1) {
      changeStatusListView.value = !changeStatusListView.value;
    }
    iconSelected.value = index;
  }
}
