import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mock_team3/app/core/app_text_theme.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_strings.dart';
import 'dialog_save_to.dart';

class ButtonSave extends StatelessWidget {
  const ButtonSave({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton.icon(
        onPressed: () {
          _openDialog(context);
        },
        style: OutlinedButton.styleFrom(
          side: BorderSide(
            width: 1,
            color: AppColors.mainColor,
          ),
          padding: const EdgeInsets.all(0),
          minimumSize: const Size(76, 26),
        ),
        icon: SvgPicture.asset(
          IconGreen.iconAdd,
          width: 15,
          height: 15,
        ),
        label: Text(
          StringRecipeFeed.btnSave,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: appTextTheme.headline5
              ?.copyWith(fontSize: 14, color: AppColors.mainColor),
        ));
  }

  Future _openDialog(BuildContext context) =>
      showDialog(context: context, builder: (context) => const DialogSaveTo());
}
