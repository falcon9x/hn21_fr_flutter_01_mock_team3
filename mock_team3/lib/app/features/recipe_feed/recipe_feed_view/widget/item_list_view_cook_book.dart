import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/data/recipe_feed/model/cook_book.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../recipe_feed_view_model/recipe_feed_view_model.dart';

class ItemListViewCookBook extends StatelessWidget {
  const ItemListViewCookBook(
      {Key? key,
      required this.cookBook,
      required this.selected,
      required this.controller})
      : super(key: key);
  final CookBook cookBook;
  final int selected;
  final RecipeFeedViewModel controller;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: Get.height * (35 / 810),
        child: Obx(() => ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: controller.selected.value == cookBook.index
                      ? AppColors.mainColorOpacity
                      : AppColors.colorWhite,
                  shape: RoundedRectangleBorder(
                      //to set border radius to button
                      borderRadius: BorderRadius.circular(5)),
                  elevation: 0),
              child: Row(
                children: [
                  Text(
                    cookBook.name,
                    style: appTextTheme.bodyText2,
                  ),
                ],
              ),
              onPressed: () {
                controller.selected.value = selected;
              },
            )));
  }
}
