import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_strings.dart';
import 'package:mock_team3/data/recipe_feed/model/cook_book.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../recipe_feed_view_model/recipe_feed_view_model.dart';
import 'item_list_view_cook_book.dart';

class DialogSaveTo extends StatelessWidget {
  const DialogSaveTo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Get.find<RecipeFeedViewModel>();
    return AlertDialog(
        titlePadding: const EdgeInsets.all(0),
        title: SizedBox(
          width: Get.width * (286 / 375),
          height: Get.height * (254 / 810),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                _buildTitleDialog(),
                _spaceBetweenTitleAndList(),
                _buildListCookBook(controller),
                _buildButtonAddCookBook()
              ],
            ),
          ),
        ));
  }

  SizedBox _spaceBetweenTitleAndList() {
    return SizedBox(
      height: Get.height * (10 / 810),
    );
  }

  Flexible _buildButtonAddCookBook() {
    return Flexible(
      child: InkWell(
        onTap: () {
          Get.back();
        },
        child: Padding(
          padding: EdgeInsets.only(
              top: Get.height * (30 / 810), bottom: Get.height * (20 / 810)),
          child: Row(
            children: [_buildIconAdd(), _buildTextAddNewCookBook()],
          ),
        ),
      ),
    );
  }

  Text _buildTextAddNewCookBook() {
    return Text(
      StringRecipeFeed.txtAddNewCookBook,
      style: appTextTheme.headline5?.copyWith(color: AppColors.mainColor),
    );
  }

  SvgPicture _buildIconAdd() {
    return SvgPicture.asset(
      IconGreen.iconAdd,
      width: 15,
      height: 15,
      color: AppColors.mainColor,
      allowDrawingOutsideViewBox: true,
    );
  }

  SizedBox _buildListCookBook(RecipeFeedViewModel controller) {
    return SizedBox(
        height: Get.height * (97 / 810),
        child: Obx(
          () => ListView.builder(
            itemCount: controller.listCookBookFinal.length,
            itemBuilder: (context, index) {
              CookBook cookBook = controller.listCookBookFinal[index];

              return ItemListViewCookBook(
                cookBook: cookBook,
                selected: index,
                controller: controller,
              );
            },
          ),
        ));
  }

  Row _buildTitleDialog() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(StringRecipeFeed.titleDialog, style: appTextTheme.headline4),
        _buildIconCloseDialog()
      ],
    );
  }

  IconButton _buildIconCloseDialog() {
    return IconButton(
        onPressed: () {
          Get.back();
        },
        icon: SvgPicture.asset(
          IconBlack.iconClose,
          color: const Color(0xff363837),
        ));
  }
}
