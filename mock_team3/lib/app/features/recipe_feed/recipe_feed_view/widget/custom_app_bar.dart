import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../core/app_assets.dart';
import '../../recipe_feed_view_model/recipe_feed_view_model.dart';

class CustomAppBar {
  final controller = Get.find<RecipeFeedViewModel>();
  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      leadingWidth: 0,
      leading: const Text(''),
      title: SvgPicture.asset(AppIcon.iconLogo),
      actions: [
        _buildIconChangePageView(),
        _buildIconMessage(),
      ],
    );
  }

  IconButton _buildIconChangePageView() {
    return IconButton(
        onPressed: () {

        },
        icon: SvgPicture.asset(IconBlack.iconNotification));
  }

  IconButton _buildIconMessage() {
    return IconButton(
        onPressed: () {}, icon: SvgPicture.asset(IconBlack.iconMessage));
  }
}
