import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/error_widget_custom.dart';
import 'package:mock_team3/data/recipe_feed/model/recipe_feed_model.dart';

import '../../recipe_feed_view_model/recipe_feed_view_model.dart';
import 'item_list_view.dart';

class RecipeFeedBody extends GetWidget<RecipeFeedViewModel> {
  const RecipeFeedBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double defaultPaddingListView = 15;
    return Obx(() => controller.listRecipeFeed.length == 0
        ? const Center(child: CircularProgressIndicator())
        : SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 30),
              child: SizedBox(
                child: controller.changeStatusListView.value
                    ? _buildListView(defaultPaddingListView)
                    : _buildPageView(defaultPaddingListView),
              ),
            ),
          ));
  }

  SizedBox _buildPageView(double defaultPaddingListView) {
    return SizedBox(
      height: Get.height * (556 / 810),
      child: PageView.builder(
        controller: PageController(viewportFraction: 0.8),
        itemCount: controller.listRecipeFeed.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          RecipeFeedModel recipeFeedModel = controller.listRecipeFeed[index];
          return ItemListView(
            defaultPaddingListView: defaultPaddingListView,
            controller: controller,
            recipe: recipeFeedModel,
            index: index,
          );
        },
      ),
    );
  }

  SizedBox _buildListView(double defaultPaddingListView) {
    return SizedBox(
        height: Get.height - Get.height * 0.15 - kBottomNavigationBarHeight,
        child: Obx(
          () => controller.listRecipeFeed.length == 0
              ? const ErrorWidgetCustom(textError: "Please Check Internet")
              : ListView.builder(
                  itemCount: controller.listRecipeFeed.length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    RecipeFeedModel recipeFeedModel =
                        controller.listRecipeFeed[index];
                    return ItemListView(
                      defaultPaddingListView: defaultPaddingListView,
                      controller: controller,
                      recipe: recipeFeedModel,
                      index: index,
                    );
                  }),
        ));
  }
}
