import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';
import 'package:mock_team3/data/recipe_feed/model/recipe_feed_model.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../utils/number_format.dart';
import '../../recipe_feed_view_model/recipe_feed_view_model.dart';
import 'button_save.dart';

class ItemListView extends StatelessWidget {
  const ItemListView(
      {Key? key,
      required this.defaultPaddingListView,
      required this.recipe,
      required this.controller,
      required this.index})
      : super(key: key);
  final double defaultPaddingListView;
  final RecipeFeedModel recipe;
  final int index;
  final RecipeFeedViewModel controller;

  @override
  Widget build(BuildContext context) {
    double _paddingHorizontal = Get.width * (15 / 375);
    return Container(
      color: Colors.white,
      width: Get.width - 20,
      height: controller.changeStatusListView.value
          ? Get.height * (360 / 810)
          : Get.height * (600 / 810),
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
        child: Column(
          children: [
            _buildImageAndNameUser(),
            _buildDetailPageView(_paddingHorizontal),
          ],
        ),
      ),
    );
  }

  Expanded _buildDetailPageView(double _paddingHorizontal) {
    return Expanded(
      child: SizedBox(
        height: Get.height * (160 / 810),
        child: Column(
          children: [
            _buildTitleRecipe(_paddingHorizontal),
            _buildDescribeRecipe(_paddingHorizontal),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: _paddingHorizontal),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buildLikeAndComment(),
                  const Flexible(child: ButtonSave())
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Row _buildLikeAndComment() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        _buildTextLike(),
        SvgPicture.asset(IconRed.iconDot),
        _buildTextComment(),
      ],
    );
  }

  Widget _buildTextComment() {
    return Text(
      "${NumberFormat.numberToString(recipe.comment)} comments",
      style: appTextTheme.bodyText2?.copyWith(color: AppColors.colorGrey),
    );
  }

  Text _buildTextLike() {
    return Text(
      "${NumberFormat.numberToString(recipe.like)} likes",
      style: appTextTheme.bodyText2?.copyWith(color: AppColors.colorGrey),
    );
  }

  Padding _buildDescribeRecipe(double _paddingHorizontal) {
    return Padding(
      padding:
          EdgeInsets.only(left: _paddingHorizontal, right: _paddingHorizontal),
      child: SizedBox(
        height: Get.height * (44 / 810),
        child: Text(
          StringRecipeFeed.describe,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: appTextTheme.subtitle2,
        ),
      ),
    );
  }

  Padding _buildTitleRecipe(double _paddingHorizontal) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: SizedBox(
        height: Get.height * (32 / 810),
        child: Row(
          children: [
            _buildTextTitleRecipe(),
            const Spacer(),
            _buildIconTitleRecipe()
          ],
        ),
      ),
    );
  }

  IconButton _buildIconTitleRecipe() {
    return IconButton(
      onPressed: () {},
      padding: const EdgeInsets.all(0),
      icon: SvgPicture.asset(
        IconBlack.iconLike,
        color: const Color(0xff363837),
      ),
    );
  }

  SizedBox _buildTextTitleRecipe() {
    return SizedBox(
      width: Get.width * 0.5,
      child: Text(
        recipe.nameRecipe,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: appTextTheme.headline6,
      ),
    );
  }

  Stack _buildImageAndNameUser() {
    return Stack(
      children: [
        _buildImageRecipe(),
        Container(
          width: Get.width - 50,
          color: Colors.white.withOpacity(0.95),
          child: Row(
            children: [
              _buildUserAvatar(),
              _buildUserNameAndTime(),
            ],
          ),
        ),
      ],
    );
  }

  Column _buildUserNameAndTime() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [_buildTextUserName(), _buildPostTime()],
    );
  }

  Text _buildPostTime() {
    return Text(
      "2h ago",
      style: appTextTheme.caption?.copyWith(color: AppColors.colorGrey),
    );
  }

  Text _buildTextUserName() {
    return Text(
      recipe.nameUser,
      style: appTextTheme.caption?.copyWith(color: AppColors.colorBlack),
    );
  }

  Padding _buildUserAvatar() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: CircleAvatar(
        radius: 15,
        child: Obx(() => Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: NetworkImage(controller.listLinkImageAvatar[index]),
                    fit: BoxFit.cover)))),
      ),
    );
  }

  SizedBox _buildImageRecipe() {
    return SizedBox(
        width: Get.width - 50,
        height: controller.changeStatusListView.value
            ? Get.height * (200 / 810)
            : Get.height * (400 / 810),
        child: Obx(() => CacheNetworkImageCustom(
            urlImage: controller.listLinkImageRecipe[index],
            boxFit: BoxFit.cover)));
  }
}
