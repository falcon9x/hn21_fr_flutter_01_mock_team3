import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/features/other_user_profile/views/other_user_profile_screen.dart';
import 'package:mock_team3/app/features/other_user_profile/views/widgets/custom_other_profile_app_bar.dart';
import 'package:mock_team3/app/features/recipe_feed/recipe_feed_view/widget/recipe_feed_body.dart';
import 'package:mock_team3/app/features/recipe_feed/recipe_feed_view_model/recipe_feed_view_model.dart';
import 'package:mock_team3/data/recipe_feed/model/icon_bottom_bar.dart';

import '../../search_suggestion/search_suggestion_view/search_suggestion_screen.dart';
import '../../user_profile/views/user_profile_screen.dart';
import '../../user_profile/views/widgets/custom_user_app_bar.dart';
import 'widget/custom_app_bar.dart';

class RecipeFeedScreen extends GetWidget<RecipeFeedViewModel> {
  const RecipeFeedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<AppBar?> appBars = [
      null,
      CustomAppBar().buildAppBar(),
      CustomUserAppBar().buildAppBar(),
      CustomOtherProfileAppBar().buildAppBar(),
    ];
    List<Widget?> body = const [
      SearchSuggestionScreen(),
      RecipeFeedBody(),
      UserProfileScreen(),
      OtherUserProfileScreen(),
    ];
    return Scaffold(
      appBar: PreferredSize(
        child: Obx(
          () => appBars[controller.appBarSelected.value] ?? const SizedBox(),
        ),
        preferredSize: const Size.fromHeight(kToolbarHeight),
      ),
      body: Obx(
        () => body[controller.appBarSelected.value] ?? const SizedBox(),
      ),
      bottomNavigationBar: Obx(
        () => _buildBottomNavigationBar(),
      ),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      showUnselectedLabels: false,
      showSelectedLabels: false,
      currentIndex: controller.iconSelected.value,
      onTap: (index) {
        controller.changeBottomTab(index);
      },
      items: [
        _buildBottomNavigationBarItem(
            controller.listIconBottomBar[0], controller.iconSelected.value),
        _buildBottomNavigationBarItem(
            controller.listIconBottomBar[1], controller.iconSelected.value),
        _buildBottomNavigationBarItem(
            controller.listIconBottomBar[2], controller.iconSelected.value),
      ],
    );
  }

  BottomNavigationBarItem _buildBottomNavigationBarItem(
      IconBottomBar icon, int index) {
    return BottomNavigationBarItem(
        icon: SvgPicture.asset(icon.linkAsset,
            color: icon.index == index
                ? AppColors.mainColor
                : AppColors.colorBlack),
        label: "");
  }
}
