import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../../../core/app_assets.dart';
import '../../../core/app_colors.dart';
import '../../../core/app_strings.dart';
import '../../../core/app_text_theme.dart';
import '../view_model/cooking_mode_view_model.dart';
import 'utils/content_status.dart';
import '../../../routes/app_routes.dart';
import '../../../utils/number_format.dart';

import '../../../core/app_padding.dart';
import 'widgets/item_step.dart';
import 'widgets/video_player.dart';

class CookingModeScreen extends GetView<CookingModeViewModel> {
  const CookingModeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (BuildContext context, Orientation orientation) {
        if (orientation == Orientation.portrait) {
          return Scaffold(
            appBar: _buildAppBar(context),
            body: _buildBodyPortrait(),
          );
        } else {
          return const Scaffold(
            body: VideoPlayerRecipe(),
          );
        }
      },
    );
  }

  Column _buildBodyPortrait() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 3,
          child: Padding(
            padding: AppPadding.defaultPadding,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    CookingModeString.titleHeader,
                    style: appTextTheme.headline3,
                  ),
                ),
                SvgPicture.asset(IconBlack.iconTime),
                Obx(() => Text(
                      NumberFormat.numberToTime(controller.time.value),
                      style: appTextTheme.headline5!
                          .copyWith(color: AppColors.mainColor),
                    ))
              ],
            ),
          ),
        ),
        const Spacer(),
        Expanded(
          flex: 3,
          child: Padding(
            padding: AppPadding.defaultPadding,
            child: Obx(() => Text(
                  controller.recipeModel.value.name,
                  style: appTextTheme.headline4,
                )),
          ),
        ),
        const Expanded(
          flex: 20,
          child: VideoPlayerRecipe(),
        ),
        const Spacer(),
        Expanded(
          flex: 2,
          child: Padding(
            padding: AppPadding.defaultPadding,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    CookingModeString.titleBody1,
                    style: appTextTheme.headline5,
                  ),
                ),
                InkWell(
                  child: Text(
                    CookingModeString.titleBody2,
                    style: appTextTheme.headline5!
                        .copyWith(color: AppColors.mainColor),
                  ),
                ),
              ],
            ),
          ),
        ),
        const Spacer(),
        Expanded(
          flex: 20,
          child: Material(
            elevation: 0,
            child: Obx(() => ListView.builder(
                  itemBuilder: (context, index) {
                    return Obx(() => ItemStep(
                          content: controller.listSub[index + 1],
                          stepRank: index + 1,
                          isSelected: getContentStatus(
                            controller.currentIndex.value,
                            index + 1,
                          ),
                        ));
                  },
                  itemCount: controller.listTimer.length - 1,
                )),
          ),
        ),
        const Spacer()
      ],
    );
  }

  ContentStatus getContentStatus(int currentIndex, int index) {
    if (currentIndex > index) {
      return ContentStatus.before;
    } else if (currentIndex == index) {
      return ContentStatus.current;
    } else {
      return ContentStatus.after;
    }
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      leadingWidth: 0,
      leading: const SizedBox(),
      title: InkWell(
        onTap: () => _backToRecipe(),
        child: Row(
          children: [
            Padding(
              padding: AppPadding.appBarTitlePadding,
              child: SvgPicture.asset(IconBlack.iconLeft),
            ),
            Text(
              CookingModeString.titleAppBar,
              style: Theme.of(context).textTheme.caption,
            )
          ],
        ),
      ),
    );
  }

  void _backToRecipe() {
    Get.offAllNamed(AppRoute.recipeFeed);
  }
}
