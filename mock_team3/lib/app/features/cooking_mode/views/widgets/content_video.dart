import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../view_model/cooking_mode_view_model.dart';
import '../../../../core/app_colors.dart';

class ContentVideo {
  static late List<Widget> _contentVideos = [];
  static final _cookingModeController = Get.find<CookingModeViewModel>();

  static List<Widget> generateContentVideo(double screenWidth) {
    if (_cookingModeController.videoLength.value != 0) {
      if (_contentVideos.isNotEmpty) {
        _contentVideos = [];
      }
      double totalLength = screenWidth - 20;
      int lengthPerSecond =
          totalLength ~/ _cookingModeController.videoLength.value;
      for (int i = 1; i < _cookingModeController.listTimer.length; i++) {
        _contentVideos.add(
          Positioned(
              left: (_cookingModeController.listTimer[i] * lengthPerSecond),
              bottom: -2,
              child: Obx(() =>Container(
                padding: const EdgeInsets.all(6),
                decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  shape: BoxShape.circle,
                  border: _cookingModeController.currentIndex >= i
                      ? Border.all(color: AppColors.mainColor)
                      : Border.all(color: AppColors.colorWhite),
                ),
                child: Text("$i"),
              )),
            ),

        );
      }
    }
    return _contentVideos;
  }
}
