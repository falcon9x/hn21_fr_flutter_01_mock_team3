import 'package:flutter/material.dart';
import '../utils/content_status.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_text_theme.dart';

class ItemStep extends StatelessWidget {
  const ItemStep({
    Key? key,
    required this.stepRank,
    required this.content,
    required this.isSelected,
  }) : super(key: key);
  final int stepRank;
  final String content;
  final ContentStatus isSelected;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: AppPadding.defaultPaddingItemListView,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Ink(
            width: 20,
            height: 20,
            decoration: BoxDecoration(
              color: isSelected == ContentStatus.before
                  ? AppColors.mainColorOpacity
                  : AppColors.colorWhite,
              shape: BoxShape.circle,
              border: Border.all(
                color: isSelected == ContentStatus.before
                    ? AppColors.mainColorOpacity
                    : AppColors.mainColor,
              ),
            ),
            child: Center(
              child: Text(
                "$stepRank",
                style: appTextTheme.caption!.copyWith(
                    color: isSelected == ContentStatus.before
                        ? AppColors.colorWhite
                        : AppColors.mainColor),
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 17.0),
              child: Text(
                content,
                maxLines: 10,
                softWrap: true,
                style: appTextTheme.subtitle2!.copyWith(
                    color: isSelected == ContentStatus.current
                        ? AppColors.colorBlack
                        : AppColors.textSubtle),
              ),
            ),
          )
        ],
      ),
    );
  }
}
