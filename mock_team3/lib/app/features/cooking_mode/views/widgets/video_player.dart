import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import 'content_video.dart';
import '../../../../utils/screen_controller.dart';
import 'package:video_player/video_player.dart';

import '../../../../core/app_assets.dart';
import '../../view_model/cooking_mode_view_model.dart';

class VideoPlayerRecipe extends GetView<CookingModeViewModel> {
  const VideoPlayerRecipe({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        GestureDetector(
          onTap: () => controller.updateIsVideoPlaying(),
          child: Obx(() => controller.videoUrl.value == ""
              ? const Center(child: CircularProgressIndicator())
              : FutureBuilder(
                  future: controller.initializeVideoPlayerFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return AspectRatio(
                        aspectRatio:
                            controller.videoPlayerController.value.aspectRatio,
                        child: VideoPlayer(
                          controller.videoPlayerController,
                        ),
                      );
                    } else if (snapshot.connectionState ==
                        ConnectionState.waiting) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return const Center(
                        child: Text("Error"),
                      );
                    }
                  },
                )),
        ),
        Positioned(
          top: 20,
          right: 20,
          child: Obx(
            () => InkWell(
              onTap: () => _goToFullScreen(),
              child: controller.isLandscapeScreen.value
                  ? SvgPicture.asset(IconWhile.iconWhiteFullScreenExit)
                  : SvgPicture.asset(IconWhile.iconFullScreen),
            ),
          ),
        ),
        Obx(
          () => Visibility(
            visible: !controller.isVideoPlaying.value,
            child: Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: Icon(
                  Icons.circle,
                  size: 90,
                  color: AppColors.colorWhite.withOpacity(0.6),
                ),
              ),
            ),
          ),
        ),
        Obx(
          () => Visibility(
            visible: !controller.isVideoPlaying.value,
            child: Positioned.fill(
              child: Align(
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: () => controller.updateIsVideoPlaying(),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: SvgPicture.asset(
                      IconWhile.iconWhitePlay,
                      width: 39,
                      height: 46,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 10,
          right: 10,
          bottom: 10,
          child: SizedBox(
              child: Obx(
            () => controller.videoUrl.value == ""
                ? const SizedBox()
                : VideoProgressIndicator(
                    controller.videoPlayerController,
                    allowScrubbing: true,
                    colors: VideoProgressColors(
                        backgroundColor: AppColors.colorWhite,
                        playedColor: AppColors.mainColor),
                  ),
          )),
        ),
        Obx(
          () => Stack(
            children: ContentVideo.generateContentVideo(
              Get.width,
            ),
          ),
        ),
        Positioned.fill(
          bottom: 30,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Obx(
              () => controller.isLandscapeScreen.value
                  ? Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: AppColors.colorSubTitleVideo),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: SizedBox(
                          width: Get.width * 3 / 4,
                          child: Text(
                            controller.listSub[controller.currentIndex.value],
                            overflow: TextOverflow.clip,
                            maxLines: 2,
                            style: appTextTheme.subtitle1!
                                .copyWith(color: AppColors.colorWhite),
                          ),
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
          ),
        )
      ],
    );
  }

  void _goToFullScreen() async {
    controller.updateIsLandscapeScreen();
    if (controller.isLandscapeScreen.value) {
      await ScreenController.makeScreenLandscapeAndHideStatusBar();
    } else {
      await ScreenController.makeScreenPortraitAndShowStatusBar();
    }
  }
}
