import 'dart:async';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/direction/direction_repository.dart';
import 'package:mock_team3/data/direction_detail/direction_detail_repository.dart';
import 'package:mock_team3/data/direction_detail/model/direction_detail_model.dart';
import 'package:mock_team3/data/info/info_repository.dart';
import 'package:mock_team3/data/recipe/dto/recipe_ui_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_repository.dart';
import 'package:video_player/video_player.dart';
import '../../../../data/direction/model/direction_model.dart';

class CookingModeViewModel extends BaseViewModel {
  late VideoPlayerController videoPlayerController;
  late Future<void> initializeVideoPlayerFuture;
  final isLandscapeScreen = false.obs;
  final isVideoPlaying = false.obs;
  final videoLength = 0.obs;
  final currentIndex = 0.obs;
  final currentSeconds = 0.obs;
  late Timer _timer;
  RxList listSub = [""].obs;
  RxList listTimer = [0.0].obs;
  final time = 0.obs;
  final videoUrl = "".obs;
  final recipeModel = RecipeModel.withInitial().obs;
  final recipeUIModel = RecipeUIModel.withInitial().obs;
  List<String> directionIds = [];
  final RecipeRepository _recipeRepository;
  final InfoRepository _infoRepository;
  final DirectionRepository _directionRepository;
  final DirectionDetailRepository _directionDetailRepository;

  CookingModeViewModel(this._recipeRepository, this._infoRepository,
      this._directionRepository, this._directionDetailRepository);

  void getRecipeById(String recipeId) async {
    DataResult result = await _recipeRepository.getRecipeById(recipeId);
    if (result.isSuccess) {
      recipeModel.value = result.data;
      await _getTime();
      await _getVideoUrl();
      await _getDirectionDetail();
    } else {
      if (result.error is APIFailure) {}
    }
  }

  void updateIsLandscapeScreen() {
    isLandscapeScreen.value = !isLandscapeScreen.value;
  }

  void updateIsVideoPlaying() {
    isVideoPlaying.value = !isVideoPlaying.value;
    if (isVideoPlaying.value) {
      videoPlayerController.play();
      _timer = Timer.periodic(
        const Duration(seconds: 1),
        (timer) {
          if (currentIndex.value < listTimer.length - 1 &&
              listTimer[currentIndex.value + 1] == currentSeconds.value) {
            currentIndex.value = currentIndex.value + 1;
          } else if (currentSeconds.value == videoLength.value) {
            timer.cancel();
          }
          currentSeconds.value = currentSeconds.value + 1;
        },
      );
    } else {
      videoPlayerController.pause();
      _timer.cancel();
    }
  }

  void _initVideoPlayer() {
    try {
      videoPlayerController = VideoPlayerController.network(
        videoUrl.value,
      );
    } catch (ex) {
      log(ex.toString(), name: 'cooking_mode_view_model');
    }
    initializeVideoPlayerFuture = videoPlayerController.initialize()
      ..then((value) => {setVideoLength(videoPlayerController)});
    videoPlayerController.setLooping(false);
  }

  @override
  void onClose() {
    videoPlayerController.dispose();
    super.onClose();
  }

  void setVideoLength(VideoPlayerController videoPlayerController) {
    videoLength.value = videoPlayerController.value.duration.inSeconds;
  }

  Future<void> _getTime() async {
    DataResult result =
        await _infoRepository.getInfo(recipeModel.value.additionalInfo ?? "");
    if (result.isSuccess) {
      time.value = result.data.time;
    } else {}
  }

  Future<void> _getVideoUrl() async {
    DataResult result = await _directionRepository
        .getDirection(recipeModel.value.direction ?? "");
    if (result.isSuccess) {
      Direction direction = result.data;
      result = await _directionRepository.getVideoFromStorage(direction.video);
      if (result.isSuccess) {
        videoUrl.value = result.data;
        directionIds = direction.directionDetails;
        _initVideoPlayer();
      }
    } else {
      log((result.error as APIFailure).errorResponse);
    }
  }

  Future<void> _getDirectionDetail() async {
    List<String> listDes = [];
    List<double> listTime = [];
    for (int indexDirection = 0;
        indexDirection < directionIds.length;
        indexDirection++) {
      DataResult result = await _directionDetailRepository
          .getDirectionDetail(directionIds[indexDirection]);
      if (result.isSuccess) {
        DirectionDetailModel detailModel = result.data;
        listDes.add(detailModel.name);
        listTime.add(detailModel.timer.toDouble());
      }
    }
    listSub.addAll(listDes);
    listTimer.addAll(listTime);
  }

  @override
  void onInit() {
    super.onInit();
    getRecipeById(Get.arguments);
  }
}
