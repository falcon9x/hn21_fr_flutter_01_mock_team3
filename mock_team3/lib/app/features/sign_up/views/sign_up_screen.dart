import 'package:flutter/material.dart';
import 'package:mock_team3/app/core/app_padding.dart';
import 'package:mock_team3/app/features/sign_up/views/widgets/sign_up_footer.dart';
import 'package:mock_team3/app/features/sign_up/views/widgets/sign_up_form.dart';
import 'package:mock_team3/app/features/sign_up/views/widgets/sign_up_title.dart';

class SignUPScreen extends StatelessWidget {
  const SignUPScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SignUPTitle(),
            Padding(
              padding: AppPadding.defaultPadding,
              child: Column(
                children: [
                  SignUPForm(),
                  const SignUPFooter(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
