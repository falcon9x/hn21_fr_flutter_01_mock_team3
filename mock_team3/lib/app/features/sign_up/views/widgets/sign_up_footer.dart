import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../routes/app_routes.dart';
import '../../../../../generated/locales.g.dart';
import '../../../../core/app_text_theme.dart';

class SignUPFooter extends StatelessWidget {
  const SignUPFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Column(
        children: [
          Text(
            LocaleKeys.signUpString_footer.tr,
            style: appTextTheme.subtitle2,
          ),
          TextButton(
            onPressed: () {
              _goToLogin();
            },
            child: Text(
              LocaleKeys.signUpString_textButtonFooter.tr,
              style: appTextTheme.button,
            ),
          ),
        ],
      ),
    );
  }

  void _goToLogin() {
    Get.offNamed(AppRoute.loginScreen);
  }
}
