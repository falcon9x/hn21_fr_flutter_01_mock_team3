import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/sign_up/view_models/sign_up_view_model.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../widgets/app_rounded_button.dart';
import '../../../../widgets/app_text_form_field.dart';

class SignUPForm extends GetView<SignUpViewModel> {
  SignUPForm({Key? key}) : super(key: key);
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final _formKeySignUp = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    return Form(
      key: _formKeySignUp,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildSubtitle(),
          _buildFullNameTextField(),
          _buildEmailTextField(),
          _buildPasswordField(),
          _buildSignUpButton(_screenWidth)
        ],
      ),
    );
  }

  Widget _buildSignUpButton(double _screenWidth) {
    return Padding(
        padding: SignUpPadding.signUPPadding,
        child: AppRoundedButton(
          width: _screenWidth,
          isCircle: false,
          text: LocaleKeys.signUpString_textButton.tr,
          onPress: () async {
            await controller.processCreationAccount(
                fullName: _fullNameController.text.trim(),
                email: _emailController.text.trim(),
                password: _passwordController.text,
                formKeySignUp: _formKeySignUp);
          },
        ));
  }

  Widget _buildPasswordField() {
    return Padding(
      padding: SignUpPadding.signUPPadding,
      child: AppTextFormField(
        validator: (value) =>
            value != null ? controller.checkPassword(value) : null,
        textEditingController: _passwordController,
        isPassword: true,
        labelText: LocaleKeys.signUpString_passwordTextField.tr,
      ),
    );
  }

  Widget _buildEmailTextField() {
    return Padding(
      padding: SignUpPadding.signUPPadding,
      child: AppTextFormField(
        validator: (value) =>
            value != null ? controller.checkEmail(value) : null,
        textEditingController: _emailController,
        isPassword: false,
        labelText: LocaleKeys.signUpString_emailTextField.tr,
      ),
    );
  }

  Widget _buildFullNameTextField() {
    return Padding(
      padding: SignUpPadding.signUPPadding,
      child: AppTextFormField(
        validator: (value) =>
            value != null ? controller.checkFullName(value) : null,
        textEditingController: _fullNameController,
        isPassword: false,
        labelText: LocaleKeys.signUpString_fullNameTextField.tr,
      ),
    );
  }

  Widget _buildSubtitle() {
    return Padding(
      padding: SignUpPadding.titleAndContentPadding,
      child: Text(
        LocaleKeys.signUpString_subtitle.tr,
        style: appTextTheme.subtitle1,
      ),
    );
  }
}
