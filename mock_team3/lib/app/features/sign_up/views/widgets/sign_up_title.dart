import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_text_theme.dart';

class SignUPTitle extends StatelessWidget {
  const SignUPTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _imageHeight = _screenWidth * 285 / 375;
    EdgeInsets _logoPadding = EdgeInsets.only(
      top: 60 * _imageHeight / 285,
    );
    EdgeInsets _titlePadding = EdgeInsets.only(
      top: 45 * _imageHeight / 285,
    );
    return SizedBox(
      height: _imageHeight,
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(_screenWidth / 4)),
            child: Image.asset(LoginAssets.assetsImage),
          ),
          Padding(
            padding: AppPadding.defaultPadding,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: _logoPadding,
                  child: SvgPicture.asset(AppIcon.iconLogo),
                ),
                Padding(
                  padding: _titlePadding,
                  child: Text(
                    LocaleKeys.signUpString_titleFirst.tr,
                    style: appTextTheme.headline3,
                  ),
                ),
                Text(
                  LocaleKeys.signUpString_titleSecond.tr,
                  style: appTextTheme.headline3,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
