import 'dart:async';
import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/widgets/validator.dart';
import 'package:mock_team3/data/account/service/account_result.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';
import '../../../../data/account/account_repository.dart';

import '../../../base/base_viewmodel.dart';
import '../../../routes/app_routes.dart';

class SignUpViewModel extends BaseViewModel {
  final AccountRepository _accountRepository;
  final FirebaseRepository _firebaseRepository;
  SignUpViewModel(
    this._accountRepository,
    this._firebaseRepository,
  );
  bool _checkProcessSignUp = false;
  String _previousEmail = "";

  Future<String?>? createAccount({
    required String fullName,
    required String email,
    required String password,
    required GlobalKey<FormState> formKeySignUp,
  }) async {
    AuthResultStatus? _status;
    int timeVerifyEmail = 120;
    if (formKeySignUp.currentState != null) {
      if (formKeySignUp.currentState!.validate()) {
        if (!_checkProcessSignUp && _previousEmail != email) {
          _previousEmail = email;
          _checkProcessSignUp = true;
          try {
            _status = await _accountRepository.createUserAuth(email, password);
            if (_status == AuthResultStatus.successful) {
              if (!await _firebaseRepository.checkEmailVeriry()) {
                await _firebaseRepository.sendEmailVerify();
                Get.snackbar("Please verify email!!!",
                    "Confirmation time is only $timeVerifyEmail seconds",
                    snackPosition: SnackPosition.BOTTOM,
                    duration: const Duration(seconds: 5));
                while (timeVerifyEmail > 0 &&
                    !await _firebaseRepository.checkEmailVeriry()) {
                  await _firebaseRepository.reloadUserCurrent();
                  Timer.periodic(const Duration(seconds: 1), (timer) async {
                    await _firebaseRepository.reloadUserCurrent();
                    if (await _firebaseRepository.checkEmailVeriry() ||
                        timeVerifyEmail <= 0) {
                      timer.cancel();
                      return;
                    } else {
                      timeVerifyEmail--;
                    }
                  });
                }
              }
              if (await _firebaseRepository.checkEmailVeriry()) {
                _checkProcessSignUp = false;
                _previousEmail = "";
                _accountRepository.createAccount(fullName, password, email);
                log(fullName + password + email);
                return "Success";
              } else {
                _previousEmail = "";
                _checkProcessSignUp = false;
                try {
                  await _firebaseRepository.deleteUserAuth();
                } on FirebaseAuthException catch (e) {
                  if (e.code == 'requires-recent-login') {
                    log('The user must reauthenticate before this operation can be executed.');
                  }
                }
                return "Email is fake or OverTimer check Email";
              }
            } else {
              _checkProcessSignUp = false;
              _previousEmail = "";
              return AccountResult.generateExceptionMessage(_status);
            }
          } catch (e) {
            _checkProcessSignUp = false;
            return e.toString();
          }
        } else {
          return "Processing";
        }
      }
    }
    return null;
  }

  Future<void> processCreationAccount({
    required String fullName,
    required String email,
    required String password,
    required GlobalKey<FormState> formKeySignUp,
  }) async {
    String? result = await createAccount(
        fullName: fullName,
        email: email,
        password: password,
        formKeySignUp: formKeySignUp);
    if (result == "Success") {
      Get.offAllNamed(AppRoute.loginScreen);
    } else if (result != null) {
      Get.snackbar(result, "",
          snackPosition: SnackPosition.BOTTOM,
          duration: const Duration(seconds: 5));
    }
  }

  String? checkEmail(String email) => Validator.isValidEmail(email.trim());
  String? checkFullName(String fullName) =>
      Validator.isValidFullName(fullName.trim());
  String? checkPassword(String password) =>
      Validator.isValidPassword(password.trim());
}
