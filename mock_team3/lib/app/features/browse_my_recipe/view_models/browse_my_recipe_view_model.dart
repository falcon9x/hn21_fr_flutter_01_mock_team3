import 'package:get/get.dart';

import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/app/routes/app_routes.dart';
import 'package:mock_team3/app/utils/share_prefs_key.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/cook_book/model/cook_book_model.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';
import 'package:mock_team3/data/info/model/info_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:mock_team3/data/tag/model/tag_model.dart';

import '../../../../data/my_recipe/my_recipe_model.dart';
import '../../../../data/user/model/user_model.dart';

class BrowseMyRecipeViewModel extends BaseViewModel {
  final FirebaseRepository _firebaseRepository;
  final SharePrefsRepository _sharePrefsRepository;
  BrowseMyRecipeViewModel(
    this._firebaseRepository,
    this._sharePrefsRepository,
  );

  @override
  Future<void> onInit() async {
    super.onInit();
    _userId = _sharePrefsRepository.getString(SharePrefsKey.userId);
    if (Get.arguments is String) {
      _cookbookIdCurrent = Get.arguments;
    } else {
      _cookbookIdCurrent = "";
    }
    await _getCookBookNames();
    await _getUrlImage();
  }

  late String _userId;
  final _myRecipes = <MyRecipeModel>[].obs;
  List<MyRecipeModel> get getMyRecipes => _myRecipes.call();
  final _cookbBookNames = <String>[].obs;
  List<String> get getCookbBookNames => _cookbBookNames.call();
  final List<String> _cookBookIds = [];
  late String _cookbookIdCurrent;
  Future<void> _getCookBookNames() async {
    UserModel user = await _firebaseRepository.getUser(_userId);
    if (user.cookbooks != null) {
      _cookBookIds.addAll(user.cookbooks!);
    }
    if (_cookBookIds.length > 1 && _cookBookIds.contains(_cookbookIdCurrent)) {
      int indexCookbookIdCurrent = _cookBookIds.indexOf(_cookbookIdCurrent);
      _cookBookIds.insert(0, _cookBookIds.removeAt(indexCookbookIdCurrent));
    } else {
      _cookbookIdCurrent = _cookBookIds[0];
    }
    for (var cookBookId in _cookBookIds) {
      CookBookModel cookbook =
          await _firebaseRepository.getCookBook(cookBookId);
      _cookbBookNames.add(cookbook.name);
    }
  }

  Future<void> _getRecipes() async {
    _myRecipes.value = [];
    CookBookModel cookbook =
        await _firebaseRepository.getCookBook(_cookbookIdCurrent);
    List<String> recipeIds = cookbook.recipes != null ? cookbook.recipes! : [];
    if (recipeIds.isNotEmpty) {
      for (var recipeId in recipeIds) {
        RecipeModel recipe = await _firebaseRepository.getRecipe(recipeId);
        MyRecipeModel myRecipe = MyRecipeModel.withInitial();
        myRecipe.cookBookIds = List<String>.from(_cookBookIds);
        myRecipe.cookBookCurrentId = _cookbookIdCurrent;
        myRecipe.directionId = recipe.direction ?? "";
        myRecipe.addInfor = recipe.additionalInfo ?? "";
        myRecipe.recipeId = recipeId;
        myRecipe.recipeName = recipe.name;
        myRecipe.ingredients = recipe.ingredients ?? [];
        myRecipe.galleries = recipe.galleries ?? [];
        if (recipe.additionalInfo != null) {
          InfoModel infor =
              await _firebaseRepository.getInfor(recipe.additionalInfo!);
          myRecipe.time = infor.time;
          List<String> tagIds = infor.tags != null ? infor.tags! : [];
          if (tagIds.isNotEmpty) {
            for (var tagId in tagIds) {
              TagModel tag = await _firebaseRepository.getTag(tagId);
              if (!myRecipe.tagName.contains(tag.name)) {
                myRecipe.tagName.add(tag.name);
              }
            }
          }
        }
        _myRecipes.add(myRecipe);
      }
    }
  }

  Future<void> _getUrlImage() async {
    await _getRecipes();
    if (_myRecipes.isNotEmpty) {
      for (int indextGalleries = 0;
          indextGalleries < _myRecipes.length;
          indextGalleries++) {
        DataResult dataResult = await _firebaseRepository
            .getImageFromStorage(_myRecipes[indextGalleries].galleries[0]);
        if (dataResult.isSuccess) {
          _myRecipes[indextGalleries].cover = dataResult.data;
          update();
        }
      }
    }
  }

  final _showCookBooks = false.obs;
  get getShowCookBooks => _showCookBooks.value;
  void changeShowCookBook() {
    _showCookBooks.value = !getShowCookBooks;
  }

  void filterCookBooks(int index) async {
    _cookbBookNames.insert(0, getCookbBookNames.removeAt(index));
    _cookBookIds.insert(0, _cookBookIds.removeAt(index));
    _cookbookIdCurrent = _cookBookIds[0];
    changeShowCookBook();
    await _getUrlImage();
  }

  void goToViewRecipe(int index) {
    if (getMyRecipes[index].cover != "") {
      Get.offNamed(AppRoute.viewRecipeScreen, arguments: getMyRecipes[index]);
    }
  }

  void goToEditRecipe(int index) {
    if (getMyRecipes[index].cover != "") {
      Get.toNamed(AppRoute.editRecipeScreen, arguments: getMyRecipes[index]);
    }
  }
}
