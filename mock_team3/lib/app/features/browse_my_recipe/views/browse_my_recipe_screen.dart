import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/browse_my_recipe/view_models/browse_my_recipe_view_model.dart';

import '../../../core/app_assets.dart';
import '../../../core/app_padding.dart';
import '../../../../generated/locales.g.dart';

import '../../../core/app_text_theme.dart';
import 'widgets/foods.dart';
import 'widgets/hastag_filter.dart';
import 'widgets/title_browse_my_recipe.dart';

class BrowseMyRecipeScreen extends GetView<BrowseMyRecipeViewModel> {
  const BrowseMyRecipeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    return Scaffold(
      appBar: _buildAppBar(),
      body: Padding(
        padding: AppPadding.defaultPadding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const TitleBrowseMyRecipe(),
            Padding(
              padding: const EdgeInsets.only(top: 30, bottom: 25),
              child: HastagFilter(
                boxWidth: _screenWidth,
              ),
            ),
            Foods()
          ],
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      leading: const Text(""),
      leadingWidth: 0,
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: InkWell(
        onTap: () {
          _backToProfile();
        },
        child: Row(
          children: [
            SvgPicture.asset(IconBlack.iconLeft),
            Text(
              LocaleKeys.browseMyRecipeString_textBackButton.tr,
              style: appTextTheme.caption,
            )
          ],
        ),
      ),
    );
  }

  void _backToProfile() {
    Get.back();
  }
}
