import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/routes/app_routes.dart';
import 'package:mock_team3/generated/locales.g.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_text_theme.dart';

class TitleBrowseMyRecipe extends StatelessWidget {
  const TitleBrowseMyRecipe({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _heightIcon = 20;
    double _widthIcon = 20;
    return Row(
      children: [
        Text(
          LocaleKeys.browseMyRecipeString_title.tr,
          style: appTextTheme.headline3,
        ),
        const Spacer(),
        InkWell(
          onTap: () {
            _goToAddRecipe();
          },
          child: Row(
            children: [
              SizedBox(
                  height: _heightIcon,
                  width: _widthIcon,
                  child: SvgPicture.asset(IconGreen.iconAdd)),
              const SizedBox(
                width: 5,
              ),
              Text(
                LocaleKeys.browseMyRecipeString_textButton.tr,
                style: appTextTheme.button,
              )
            ],
          ),
        )
      ],
    );
  }

  void _goToAddRecipe() {
    Get.offNamed(AppRoute.newRecipeScreen);
  }
}
