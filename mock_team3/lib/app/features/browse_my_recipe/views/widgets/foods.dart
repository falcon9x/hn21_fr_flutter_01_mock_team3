import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../../view_models/browse_my_recipe_view_model.dart';
import '../../../edit_recipe/views/widgets/cache_network_image_custom.dart';
import 'package:mock_team3/generated/locales.g.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';

class Foods extends StatelessWidget {
  Foods({
    Key? key,
  }) : super(key: key);
  final _browseMyRecipeController = Get.find<BrowseMyRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double _heighCard = 219;
    double screenWidth = Get.width;
    return Expanded(
      child: Obx(
        () => _browseMyRecipeController.getMyRecipes.isEmpty
            ? Center(
                child: SizedBox(
                    height: screenWidth / 5,
                    width: screenWidth / 5,
                    child: const CircularProgressIndicator()),
              )
            : ListView.builder(
                itemCount: _browseMyRecipeController.getMyRecipes.length,
                itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.only(
                        bottom: 20,
                      ),
                      child: SizedBox(
                          height: _heighCard,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _buildImageFood(screenWidth, index),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    _buildTitleFood(_browseMyRecipeController
                                        .getMyRecipes[index].recipeName),
                                    _buildDetailFood(
                                        _browseMyRecipeController
                                            .getMyRecipes[index].time,
                                        _browseMyRecipeController
                                            .getMyRecipes[index]
                                            .ingredients
                                            .length,
                                        index)
                                  ],
                                ),
                              )
                            ],
                          )),
                    )),
      ),
    );
  }

  Padding _buildDetailFood(int time, int amountIngerdient, int index) {
    EdgeInsets _detailPadding = const EdgeInsets.only(top: 11);
    return Padding(
      padding: _detailPadding,
      child: Row(
        children: [
          Text(
            "±$time " + LocaleKeys.browseMyRecipeString_timeFood.tr,
            style: appTextTheme.subtitle2,
          ),
          SvgPicture.asset(IconRed.iconDot),
          Text(
            "$amountIngerdient " +
                LocaleKeys.browseMyRecipeString_ingredients.tr,
            style: appTextTheme.subtitle2,
          ),
          const Spacer(),
          _buildButtonCook(index)
        ],
      ),
    );
  }

  Padding _buildTitleFood(String name) {
    EdgeInsets _titlePadding = const EdgeInsets.only(top: 17);
    return Padding(
      padding: _titlePadding,
      child: Text(
        name,
        style: appTextTheme.headline6,
      ),
    );
  }

  ElevatedButton _buildButtonCook(int index) {
    double _heighIcon = 20;
    double _widthIcon = 20;
    EdgeInsets _buttonPadding = const EdgeInsets.symmetric(
      horizontal: 8,
      vertical: 4,
    );
    return ElevatedButton(
      onPressed: () {
        _browseMyRecipeController.goToViewRecipe(index);
      },
      child: Row(
        children: [
          SvgPicture.asset(
            IconGreen.iconPlay,
            height: _heighIcon,
            width: _widthIcon,
          ),
          Text(
            LocaleKeys.browseMyRecipeString_textButtonCook.tr,
            style: appTextTheme.button?.copyWith(
              fontSize: 14,
            ),
          ),
        ],
      ),
      style: ElevatedButton.styleFrom(
          elevation: 0,
          onPrimary: AppColors.mainColor,
          padding: _buttonPadding,
          primary: Colors.transparent,
          shadowColor: Colors.transparent,
          side: BorderSide(
            color: AppColors.mainColor,
          )),
    );
  }

  Widget _buildImageFood(double screenWidht, int index) {
    double _heightImage = 118;
    return InkWell(
        onTap: () {
          _browseMyRecipeController.goToEditRecipe(index);
        },
        child: SizedBox(
          height: _heightImage,
          width: screenWidht,
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8)),
            child: GetBuilder<BrowseMyRecipeViewModel>(
                builder: (_) => CacheNetworkImageCustom(
                      urlImage:
                          _browseMyRecipeController.getMyRecipes[index].cover,
                      boxFit: BoxFit.cover,
                    )),
          ),
        ));
  }
}
