import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_border_radius.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../view_models/browse_my_recipe_view_model.dart';

class HastagFilter extends StatelessWidget {
  HastagFilter({
    Key? key,
    required this.boxWidth,
  }) : super(key: key);
  final double boxWidth;
  final _browseMyRecipeController = Get.find<BrowseMyRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: boxWidth,
      decoration: BoxDecoration(
        color: AppColors.colorWhite,
        borderRadius: AppBorderRadius.appborderRadius,
        boxShadow: [
          //create shadow
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 5,
            blurRadius: 7,
            // offset shadow
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              _buildCookBookCurrent(),
              Obx(
                () => _browseMyRecipeController.getShowCookBooks
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount:
                            _browseMyRecipeController.getCookbBookNames.length -
                                1,
                        itemBuilder: (context, index) =>
                            _buildCookBookRemain(index),
                      )
                    : const SizedBox(),
              )
            ],
          )),
    );
  }

  Material _buildCookBookRemain(int index) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () => _browseMyRecipeController.filterCookBooks(index + 1),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Text(
            _browseMyRecipeController.getCookbBookNames[index + 1],
            style: appTextTheme.bodyText1,
          ),
        ),
      ),
    );
  }

  Widget _buildCookBookCurrent() {
    return Obx(() => _browseMyRecipeController.getCookbBookNames.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Row(
              children: [
                Text(
                  _browseMyRecipeController.getCookbBookNames[0],
                  style: appTextTheme.bodyText1,
                ),
                const Spacer(),
                SizedBox(
                  height: 24,
                  width: 24,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () =>
                          _browseMyRecipeController.changeShowCookBook(),
                      child: SvgPicture.asset(IconBlack.iconArrowDown),
                    ),
                  ),
                )
              ],
            ))
        : const SizedBox());
  }
}
