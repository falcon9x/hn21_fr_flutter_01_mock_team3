import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../routes/app_routes.dart';

class LoginFooter extends StatelessWidget {
  const LoginFooter({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Column(
        children: [
          Text(
            LocaleKeys.login_footer.tr,
            style: appTextTheme.subtitle2,
          ),
          TextButton(
            onPressed: () {
              _goToSignUp();
            },
            child: Text(
              LocaleKeys.login_textButtonFooter.tr,
              style: appTextTheme.button,
            ),
          ),
        ],
      ),
    );
  }

  void _goToSignUp() {
    Get.offNamed(AppRoute.signUpScreen);
  }
}
