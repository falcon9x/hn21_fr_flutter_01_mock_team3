import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/login/view_models/login_view_model.dart';

import '../../../../../generated/locales.g.dart';

import '../../../../core/app_text_theme.dart';
import '../../../../widgets/app_rounded_button.dart';
import '../../../../widgets/app_text_form_field.dart';

class LoginForm extends GetView<LoginViewModel> {
  LoginForm({
    Key? key,
  }) : super(key: key);
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    return Stack(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildSubtitle(),
            _buildEmailTextField(),
            _buildPassword(),
            _buildLoginButton(_screenWidth)
          ],
        ),
        _buildForgotPassword(),
      ],
    );
  }

  Widget _buildLoginButton(double _screenWidth) {
    return Padding(
        padding: const EdgeInsets.only(top: 30),
        child: AppRoundedButton(
          width: _screenWidth,
          isCircle: false,
          text: LocaleKeys.login_contentButton.tr,
          onPress: () async {
            controller.processLogin(
                _emailController.text.trim(), _passwordController.text);
          },
        ));
  }

  Widget _buildPassword() {
    return Padding(
      padding: const EdgeInsets.only(top: 46),
      child: AppTextFormField(
        textEditingController: _passwordController,
        isPassword: true,
        labelText: LocaleKeys.login_passwordTextField.tr,
      ),
    );
  }

  Widget _buildForgotPassword() {
    return Padding(
        padding: const EdgeInsets.only(top: 175),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
              onPressed: () {
                //TODO
              },
              child: Text(
                LocaleKeys.login_forgotPassword.tr,
                style: appTextTheme.subtitle1,
              ),
            ),
          ],
        ));
  }

  Widget _buildEmailTextField() {
    return Padding(
      padding: const EdgeInsets.only(top: 47),
      child: AppTextFormField(
        textEditingController: _emailController,
        isPassword: false,
        labelText: LocaleKeys.login_emailTextField.tr,
      ),
    );
  }

  Widget _buildSubtitle() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Text(
        LocaleKeys.login_subtitle.tr,
        style: appTextTheme.subtitle1,
      ),
    );
  }
}
