import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_text_theme.dart';

class LoginTitle extends StatelessWidget {
  const LoginTitle({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _imageHeight = _screenWidth * 285 / 375;
    EdgeInsets _logoPadding = EdgeInsets.only(
      top: 60 * _imageHeight / 285,
    );
    EdgeInsets _titlePadding = EdgeInsets.only(
      top: 45 * _imageHeight / 285,
    );
    return SizedBox(
      height: _imageHeight,
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(_screenWidth / 4)),
            child: Image.asset(LoginAssets.assetsImage),
          ),
          Padding(
            padding: AppPadding.defaultPadding,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: _logoPadding,
                  child: SvgPicture.asset(AppIcon.iconLogo),
                ),
                Padding(
                  padding: _titlePadding,
                  child: Text(
                    title,
                    style: appTextTheme.headline3,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
