import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/generated/locales.g.dart';

import '../../../core/app_padding.dart';
import 'widgets/login_footer.dart';
import 'widgets/login_form.dart';
import 'widgets/login_title.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            LoginTitle(
              title: LocaleKeys.login_title.tr,
            ),
            Padding(
              padding: AppPadding.defaultPadding,
              child: Column(
                children: [
                  LoginForm(),
                  const LoginFooter(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
