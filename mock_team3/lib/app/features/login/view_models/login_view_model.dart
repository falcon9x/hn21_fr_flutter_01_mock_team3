import 'dart:developer';

import 'package:get/get.dart';

import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/app/utils/share_prefs_key.dart';
import 'package:mock_team3/data/account/account_repository.dart';
import 'package:mock_team3/data/firebase/firebase_repository.dart';

import '../../../../data/account/service/account_result.dart';
import '../../../routes/app_routes.dart';
import '../../../utils/share_prefs_repository.dart';

class LoginViewModel extends BaseViewModel {
  final AccountRepository _accountRepository;
  final SharePrefsRepository _sharePrefsRepository;
  final FirebaseRepository _firebaseRepository;
  LoginViewModel(
    this._accountRepository,
    this._sharePrefsRepository,
    this._firebaseRepository,
  );
  Future<String?>? login(String email, String password) async {
    AuthResultStatus? _status;

    try {
      _status = await _accountRepository.signIn(email, password);
      if (_status == AuthResultStatus.successful) {
        _sharePrefsRepository.setString(
            SharePrefsKey.userId, await _firebaseRepository.getIdUser());
        _sharePrefsRepository.saveBool(SharePrefsKey.keepLogIn, true);
        log(_sharePrefsRepository.getString(SharePrefsKey.userId));
        return "Success";
      }
      return AccountResult.generateExceptionMessage(_status);
    } catch (e) {
      return e.toString();
    }
  }

  Future<void> processLogin(String email, String password) async {
    String? result = await login(email, password);
    if (result == "Success") {
      Get.offAllNamed(AppRoute.recipeFeed);
    } else {
      Get.snackbar(result ?? "", "",
          snackPosition: SnackPosition.BOTTOM,
          duration: const Duration(seconds: 5));
    }
  }
}
