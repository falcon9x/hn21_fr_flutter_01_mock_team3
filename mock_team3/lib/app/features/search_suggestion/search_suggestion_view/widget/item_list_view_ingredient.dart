import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view_model/search_suggestion_view_model.dart';
import 'package:mock_team3/data/search_suggestion/model/search_suggestion_model.dart';

import '../../../../core/app_text_theme.dart';

class ItemListViewIngredient extends GetWidget<SearchSuggestionViewModel> {
  const ItemListViewIngredient({Key? key, required this.searchSuggestionModel})
      : super(key: key);
  final SearchSuggestionModel searchSuggestionModel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: Get.width * (15 / 810)),
      child: SizedBox(
        width: Get.width * (120 / 375),
        child: Column(
          children: [
            SizedBox(
                width: Get.width * (120 / 375),
                height: Get.height * (140 / 810),
                child: Obx(() =>
                    controller.listSearchSuggestionIngredient.length ==
                            controller.lengthListSearchIngredient
                        ? CacheNetworkImageCustom(
                            urlImage: searchSuggestionModel.linkImageRecipe,
                            boxFit: BoxFit.cover,
                          )
                        : const Center(
                            child: SizedBox(
                              child: CircularProgressIndicator(),
                            ),
                          ))),
            SizedBox(
              height: Get.height * (70 / 810),
              child: Text(
                searchSuggestionModel.nameRecipe,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: appTextTheme.bodyText1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
