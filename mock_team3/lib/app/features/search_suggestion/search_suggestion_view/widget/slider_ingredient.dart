import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import '../../../../core/app_colors.dart';
import '../../search_suggestion_view_model/search_suggestion_view_model.dart';

class SliderIngredient extends StatelessWidget {
  const SliderIngredient(
      {Key? key, required this.controller, required this.radiusSlider})
      : super(key: key);

  final SearchSuggestionViewModel controller;
  final double radiusSlider;

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
        data: SliderTheme.of(context).copyWith(
          overlayShape: SliderComponentShape.noOverlay,
          trackHeight: 2.0,
          thumbColor: Colors.transparent,
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: radiusSlider),
        ),
        child: Obx(() => Slider(
              value: controller.numberIngredients.value,
              onChanged: (value) {
                controller.numberIngredients.value = value;
                controller.numberIngredientsText.value = value.toStringAsFixed(0);
              },
              max: 20.0,
              min: 0.0,
              activeColor: AppColors.mainColor,
              inactiveColor: const Color(0xffE6E6E6),
            )));
  }
}
