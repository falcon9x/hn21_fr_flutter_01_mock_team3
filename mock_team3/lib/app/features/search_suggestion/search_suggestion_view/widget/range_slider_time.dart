import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import '../../../../core/app_colors.dart';
import '../../search_suggestion_view_model/search_suggestion_view_model.dart';

class RangeSliderTime extends StatelessWidget {
  const RangeSliderTime(
      {Key? key, required this.controller, required this.radiusSlider})
      : super(key: key);

  final SearchSuggestionViewModel controller;
  final double radiusSlider;

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
        data: SliderTheme.of(context).copyWith(
          overlayShape: SliderComponentShape.noOverlay,
          trackHeight: 2.0,
          thumbColor: Colors.transparent,
          rangeThumbShape:
              RoundRangeSliderThumbShape(enabledThumbRadius: radiusSlider),
        ),
        child: Obx(
          () => RangeSlider(
            values: controller.timeRangeValue.value,
            onChanged: (value) {
              controller.timeRangeValue.value = value;

              controller.numberOfTimeMinValue.value = value.start.toInt();
              controller.numberOfTimeMaxValue.value = value.end.toInt();
            },
            min: 0,
            max: 60,
            activeColor: AppColors.mainColor,
            inactiveColor: const Color(0xffE6E6E6),
          ),
        ));
  }
}
