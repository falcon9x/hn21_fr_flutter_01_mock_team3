import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_colors.dart';

class ErrorWidgetCustom extends StatelessWidget {
  const ErrorWidgetCustom({
    required this.textError,
    Key? key,
  }) : super(key: key);
  final String textError;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        textError,
        style: Get.textTheme.bodyText1?.copyWith(color: AppColors.mainColor),
      ),
    );
  }
}
