import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/routes/app_routes.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../search_suggestion_view_model/search_suggestion_view_model.dart';
import 'dialog_filter.dart';

class AppBarSearch extends GetWidget<SearchSuggestionViewModel> {
  const AppBarSearch(
      {Key? key, required this.hintTextFiled, required this.colorIconFilter})
      : super(key: key);

  final String hintTextFiled;
  final Color colorIconFilter;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
          color: AppColors.colorWhite),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Material(
            color: Colors.transparent,
            child: IconButton(
                splashRadius: 23,
                onPressed: () {
                  Get.toNamed(AppRoute.search);
                },
                icon: SvgPicture.asset(IconBlack.iconNav1)),
          ),
          _buildTextFieldSearch(),
          const Spacer(),
          Material(
            color: Colors.transparent,
            child: IconButton(
                splashRadius: 23,
                onPressed: () {
                  controller.closeDiaLogFiller.value++;
                  if (controller.closeDiaLogFiller.value == 2) {
                    Get.back();
                    controller.closeDiaLogFiller.value = 0;
                    return;
                  }
                  showDialog(
                      context: context,
                      useRootNavigator: true,
                      builder: (context) => DialogFilter(
                            controller: controller,
                            context: context,
                          ));
                },
                icon: SvgPicture.asset(
                  IconBlack.iconFilter,
                  color: colorIconFilter,
                )),
          )
        ],
      ),
    );
  }

  SizedBox _buildTextFieldSearch() {
    return SizedBox(
      width: Get.width * (177 / 375),
      child: TextFormField(
        style: appTextTheme.bodyText2?.copyWith(color: const Color(0xffA8A8A8)),
        cursorColor: const Color(0xffA8A8A8),
        keyboardType: TextInputType.text,
        controller: controller.searchController,
        maxLines: 1,
        decoration: InputDecoration(
          enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent)),
          focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent)),
          hintText: hintTextFiled,
          labelStyle:
              appTextTheme.bodyText2?.copyWith(color: const Color(0xffA8A8A8)),
        ),
      ),
    );
  }
}
