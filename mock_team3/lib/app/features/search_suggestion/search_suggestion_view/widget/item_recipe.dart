import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view_model/search_suggestion_view_model.dart';
import 'package:mock_team3/data/search_suggestion/model/search_suggestion_model.dart';

import '../../../../core/app_text_theme.dart';

class ItemRecipe extends GetWidget<SearchSuggestionViewModel> {
  const ItemRecipe({Key? key, required this.searchSuggestionModel})
      : super(key: key);
  final SearchSuggestionModel searchSuggestionModel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: Get.width * (15 / 375)),
      child: SizedBox(
        width: Get.width * (120 / 375),
        child: Column(
          children: [_buildImage(), _buildName(context)],
        ),
      ),
    );
  }

  SizedBox _buildName(BuildContext context) {
    return SizedBox(
      height: Get.height * (70 / 810),
      child: Text(
        searchSuggestionModel.nameRecipe,
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        style: appTextTheme.bodyText1,
      ),
    );
  }

  SizedBox _buildImage() {
    return SizedBox(
        height: Get.height * (140 / 810),
        width: Get.width * (120 / 375),
        child: Obx(() =>
                controller.listSearchSuggestionTrending.length != 0
            ? CacheNetworkImageCustom(
                urlImage :searchSuggestionModel.linkImageRecipe,
                boxFit: BoxFit.cover,
              )
            : const Center(
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              )
        )
    );
  }
}
