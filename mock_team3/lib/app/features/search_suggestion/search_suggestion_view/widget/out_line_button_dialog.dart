import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/app_colors.dart';

class OutlineButtonDialog extends StatelessWidget {
  const OutlineButtonDialog({
    required this.title,
    Key? key,
  }) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {},
      child: Text(
        title,
        style: Theme.of(context)
            .textTheme
            .bodyText2
            ?.copyWith(color: AppColors.mainColor),
      ),
      style: OutlinedButton.styleFrom(
        minimumSize: Size(100, Get.height * (29 / 810)),
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        side: BorderSide(width: 2, color: AppColors.mainColor),
      ),
    );
  }
}
