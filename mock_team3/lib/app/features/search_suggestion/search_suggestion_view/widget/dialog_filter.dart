import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/app_bar_search.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/range_slider_time.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/slider_ingredient.dart';
import 'package:mock_team3/app/routes/app_routes.dart';

import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';
import '../../search_suggestion_view_model/search_suggestion_view_model.dart';
import 'out_line_button_dialog.dart';

class DialogFilter extends StatelessWidget {
  const DialogFilter({Key? key, required this.controller,required this.context}) : super(key: key);
  final SearchSuggestionViewModel controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    double widthScreen = Get.width;
    double heightScreen = Get.height;
    double _paddingHorizontal = widthScreen * (35 / 375);
    double _radiusSlider = 5;
    return SingleChildScrollView(
      child: AlertDialog(
        insetPadding: EdgeInsets.only(
            left: 0, right: 0, bottom: Get.height * (260 / 810)),
        contentPadding: EdgeInsets.zero,
        titlePadding: const EdgeInsets.all(0),
        title: SizedBox(
          width: Get.width * (325 / 375),
          height: Get.width * (340 / 375) * (475 / 325),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppBarSearch(

                  hintTextFiled: "Sweet",
                  colorIconFilter: AppColors.mainColor),
              _buildTitleSearch(heightScreen, _paddingHorizontal, context),
              _buildTitleIngredients(heightScreen, _paddingHorizontal, context),
              _buildSliderIngredients(heightScreen, context, _radiusSlider),
              _buildTitleServingTime(_paddingHorizontal, heightScreen, context),
              _buildRangeSliderTime(
                  _paddingHorizontal, heightScreen, context, _radiusSlider),
              _buildTitleButton(heightScreen, _paddingHorizontal, context),
              _buildButtonRecipeAndProfile(_paddingHorizontal),
              _buildButtonTag(_paddingHorizontal),
              _buildButtonFilter(_paddingHorizontal)
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButtonTag(double _paddingHorizontal) {
    return Padding(
      padding: EdgeInsets.only(left: _paddingHorizontal),
      child: const OutlineButtonDialog(title: '326 tag'),
    );
  }

  Widget _buildButtonRecipeAndProfile(double _paddingHorizontal) {
    return Padding(
      padding: EdgeInsets.only(left: _paddingHorizontal),
      child: Row(
        children: const [
          OutlineButtonDialog(title: '298 recipe'),
          SizedBox(
            width: 15,
          ),
          OutlineButtonDialog(title: '78 profile'),
        ],
      ),
    );
  }

  Widget _buildButtonFilter(double _paddingHorizontal) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: _paddingHorizontal, top: _paddingHorizontal),
      child: Container(
        padding: EdgeInsets.only(
            left: _paddingHorizontal, right: _paddingHorizontal),
        height: Get.height * (50 / 810),
        width: Get.width,
        child: ElevatedButton(
          onPressed: () {
            controller.filterRecipe(controller.numberIngredients.value,controller.numberOfTimeMinValue.value,controller.numberOfTimeMaxValue.value);
            Get.back();
            Get.toNamed(AppRoute.search);
          },
          child: Text(StringSearchSuggestion.btnApplyFilter),
          style: ElevatedButton.styleFrom(primary: AppColors.mainColor),
        ),
      ),
    );
  }

  Widget _buildTitleButton(
      double heightScreen, double _paddingHorizontal, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: heightScreen * (30 / 810), left: _paddingHorizontal),
      child: Text(
        StringSearchSuggestion.txtSearchFor,
        style: appTextTheme.headline5,
      ),
    );
  }

  Widget _buildRangeSliderTime(double _paddingHorizontal, double heightScreen,
      BuildContext context, double _radiusSlider) {
    return Padding(
      padding: EdgeInsets.only(
          left: _paddingHorizontal,
          right: _paddingHorizontal,
          top: heightScreen * (15 / 810)),
      child: RangeSliderTime(
        controller: controller,
        radiusSlider: _radiusSlider,
      ),
    );
  }

  Widget _buildTitleServingTime(
      double _paddingHorizontal, double heightScreen, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          left: _paddingHorizontal,
          right: _paddingHorizontal,
          top: heightScreen * (24 / 810)),
      child: Row(
        children: [
          _buildTime(context),
          const Spacer(),
          Obx(() => _buildNumberTime(context)),
        ],
      ),
    );
  }

  Widget _buildNumberTime(BuildContext context) {
    return Text(
      "${controller.numberOfTimeMinValue.value.toString()} - ${controller.numberOfTimeMaxValue.value.toString()} mins",
      style: appTextTheme.subtitle2?.copyWith(color: AppColors.textSubtle),
    );
  }

  Widget _buildTime(BuildContext context) {
    return Text(
      StringSearchSuggestion.txtServingTime,
      style: appTextTheme.bodyText2,
    );
  }

  Padding _buildSliderIngredients(
      double heightScreen, BuildContext context, double _radiusSlider) {
    return Padding(
      padding: EdgeInsets.only(
          left: Get.width * (30 / 375),
          right: Get.width * (30 / 375),
          top: heightScreen * (13 / 810)),
      child: SliderIngredient(
        controller: controller,
        radiusSlider: _radiusSlider,
      ),
    );
  }

  Padding _buildTitleIngredients(
      double heightScreen, double _paddingHorizontal, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: heightScreen * (20 / 810),
          left: _paddingHorizontal,
          right: _paddingHorizontal),
      child: Row(
        children: [
          _buildTextIngredient(context),
          const Spacer(),
          Obx(() => _buildNumberOfGradient(context))
        ],
      ),
    );
  }

  Text _buildNumberOfGradient(BuildContext context) {
    return Text(
      controller.numberIngredientsText.value,
      style: appTextTheme.subtitle2?.copyWith(color: AppColors.textSubtle),
    );
  }

  Text _buildTextIngredient(BuildContext context) {
    return Text(
      StringSearchSuggestion.txtIngredients,
      style: appTextTheme.bodyText2,
    );
  }

  Padding _buildTitleSearch(
      double heightScreen, double _paddingHorizontal, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: heightScreen * (30 / 810), left: _paddingHorizontal),
      child: Text(
        StringSearchSuggestion.txtSearchFilter,
        style: appTextTheme.headline5,
      ),
    );
  }
}
