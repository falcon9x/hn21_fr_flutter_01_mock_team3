import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../data/search_suggestion/model/title_ingredient.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../search_suggestion_view_model/search_suggestion_view_model.dart';

class ItemListViewTitleIngredient extends StatelessWidget {
  const ItemListViewTitleIngredient(
      {Key? key,
      required this.titleIngredient,
      required this.index,
      required this.controller})
      : super(key: key);
  final TitleIngredient titleIngredient;
  final SearchSuggestionViewModel controller;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: Get.width * (34 / 375)),
      child: InkWell(
        onTap: () {
          controller.indexSelectedTitle.value = index;
          controller.getListRecipeByIngredient(index);
        },
        child: Obx(() => Text(
              titleIngredient.name,
              style: appTextTheme.headline4?.copyWith(
                  color: controller.indexSelectedTitle.value == index
                      ? AppColors.colorBlack
                      : AppColors.colorBlack.withOpacity(0.3)),
            )),
      ),
    );
  }
}
