import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/core/app_strings.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/app_bar_search.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/error_widget_custom.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/item_list_view_ingredient.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/item_list_view_title_ingadient.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/item_recipe.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view_model/search_suggestion_view_model.dart';
import 'package:mock_team3/data/search_suggestion/model/search_suggestion_model.dart';
import 'package:mock_team3/data/search_suggestion/model/title_ingredient.dart';

import '../../../core/app_text_theme.dart';

class SearchSuggestionScreen extends GetView<SearchSuggestionViewModel> {
  const SearchSuggestionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: Get.height * (55 / 810),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Get.width * (25 / 375)),
              child: AppBarSearch(
                hintTextFiled: StringSearchSuggestion.titleAppBar,
                colorIconFilter: AppColors.colorBlack,
              ),
            ),
            _buildTitleTrending(),
            _buildListViewTrending(),
            _buildDivider(),
            _buildQuestionTitle(),
            _buildTitleIngredient(),
            _buildListViewIngredient()
          ],
        ),
      ),
    );
  }

  Padding _buildListViewIngredient() {
    return Padding(
      padding: EdgeInsets.only(left: Get.width * (25 / 375)),
      child: SizedBox(
          height: Get.height * (220 / 810),
          child: Obx(
            () => controller.listSearchSuggestionIngredient.length == 0
                ? const ErrorWidgetCustom(textError: "On loading ...")
                : ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: controller.listSearchSuggestionIngredient.length,
                    itemBuilder: (context, index) {
                      SearchSuggestionModel searchSuggestionModel =
                          controller.listSearchSuggestionIngredient[index];
                      return ItemListViewIngredient(
                        searchSuggestionModel: searchSuggestionModel,
                      );
                    }),
          )),
    );
  }

  Padding _buildTitleIngredient() {
    return Padding(
      padding: EdgeInsets.only(
          top: Get.height * (11 / 810),
          left: Get.width * (25 / 375),
          bottom: 20),
      child: SizedBox(
          height: Get.height * (30 / 810),
          child: ListView.builder(
              itemCount: controller.listTitleIngredient.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                TitleIngredient titleIngredient =
                    controller.listTitleIngredient[index];
                return ItemListViewTitleIngredient(
                  titleIngredient: titleIngredient,
                  index: index,
                  controller: controller,
                );
              })),
    );
  }

  Padding _buildQuestionTitle() {
    return Padding(
      padding: EdgeInsets.only(
          top: Get.height * (15 / 810), left: Get.width * (25 / 375)),
      child: SizedBox(
          width: Get.width * (172 / 375),
          child: Text(
            StringSearchSuggestion.txtWhatCanIMake,
            overflow: TextOverflow.ellipsis,
            style: appTextTheme.headline5,
          )),
    );
  }

  Padding _buildDivider() {
    return Padding(
      padding: EdgeInsets.only(
          top: Get.height * (50 / 810),
          left: Get.width * (25 / 375),
          right: Get.width * (25 / 375)),
      child: Divider(
        height: 1,
        color: AppColors.colorDivider,
        thickness: 3,
      ),
    );
  }

  Padding _buildListViewTrending() {
    return Padding(
      padding: EdgeInsets.only(left: Get.width * (25 / 375)),
      child: SizedBox(
          height: Get.height * 0.26,
          child: Obx(() => controller.listSearchSuggestionTrending.length == 0
              ? const ErrorWidgetCustom(
                  textError: "No data not found",
                )
              : ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: controller.listSearchSuggestionTrending.length,
                  itemBuilder: (context, index) {
                    SearchSuggestionModel searchSuggestionModel =
                        controller.listSearchSuggestionTrending[index];

                    return ItemRecipe(
                      searchSuggestionModel: searchSuggestionModel,
                    );
                  }))),
    );
  }

  Padding _buildTitleTrending() {
    return Padding(
      padding: EdgeInsets.only(
          top: Get.height * (30 / 810),
          bottom: Get.height * (30 / 810),
          left: Get.width * (25 / 375)),
      child: Text(
        StringSearchSuggestion.txtTrendingRecipes,
        style: Get.textTheme.headline5,
      ),
    );
  }
}
