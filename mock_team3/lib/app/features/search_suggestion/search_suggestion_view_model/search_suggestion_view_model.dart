import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:mock_team3/data/search_suggestion/model/search_suggestion_model.dart';
import 'package:mock_team3/data/search_suggestion/model/search_suggestion_profile.dart';
import 'package:mock_team3/data/search_suggestion/model/title_ingredient.dart';
import 'package:mock_team3/data/search_suggestion/search_suggestion_repository.dart';
import 'package:mock_team3/data/tag/model/tag_model.dart';
import 'package:mock_team3/data/user/model/user_model.dart';

import '../../../../data/base/data_result.dart';
import '../../../../data/search_suggestion/model/button_dialog.dart';

class SearchSuggestionViewModel extends BaseViewModel {
  final SearchSuggestionRepository _searchSuggestionRepository;

  SearchSuggestionViewModel(this._searchSuggestionRepository);

  late TextEditingController searchController;
  late TextEditingController searchDialogController;
  var _numberIngredients = 8.0.obs;
  var _numberIngredientsText = "8".obs;

  var _timeRangeValue = const RangeValues(10, 30).obs;

  var _numberOfTimeMinValue = 10.obs;
  var _numberOfTimeMaxValue = 30.obs;

  var appBarSelected = 0.obs;

  var closeDiaLogFiller = 0.obs;

  get numberOfTimeMinValue => _numberOfTimeMinValue;

  set numberOfTimeMinValue(value) {
    _numberOfTimeMinValue = value;
  }

  get timeRangeValue => _timeRangeValue;

  set timeRangeValue(value) {
    _timeRangeValue = value;
  }

  get numberIngredientsText => _numberIngredientsText;

  set numberIngredientsText(value) {
    _numberIngredientsText = value;
  }

  get numberIngredients => _numberIngredients;

  set numberIngredients(value) {
    _numberIngredients = value;
  }

  final _listTitle = ["298 recipes", "78 profiles", "326 tags"];
  var listButton = [].obs;

  var listNameIngredient = ["Meat", "Sugar", "Tomato", "Pump", "Agg"];
  var listTitleIngredient = <TitleIngredient>[].obs;
  var indexSelectedTitle = 0.obs;

  var listRecipeTrending = <RecipeModel>[].obs;
  var _listSearchSuggestionIngredient = <SearchSuggestionModel>[].obs;

  get listSearchSuggestionIngredient => _listSearchSuggestionIngredient;

  set listSearchSuggestionIngredient(value) {
    _listSearchSuggestionIngredient = value;
  }

  var lengthListSearchTrending = 0;
  var lengthListSearchIngredient = 0;

  var _listSearchSuggestionTrending = <SearchSuggestionModel>[].obs;

  get listSearchSuggestionTrending => _listSearchSuggestionTrending;

  set listSearchSuggestionTrending(value) {
    _listSearchSuggestionTrending = value;
  }

  var listRecipeIngredient = <RecipeModel>[].obs;

  var _listIDFilterNumberIngredient = [].obs;

  get listIDFilterNumberIngredient => _listIDFilterNumberIngredient;

  set listIDFilterNumberIngredient(value) {
    _listIDFilterNumberIngredient = value;
  }

  var _listIDRecipeFilterServingTime = [].obs;

  get listIDRecipeFilterServingTime => _listIDRecipeFilterServingTime;

  set listIDRecipeFilterServingTime(value) {
    _listIDRecipeFilterServingTime = value;
  }

  var listRecipeFilter = <SearchSuggestionModel>[].obs;

  var listRecipeData = <RecipeModel>[].obs;

  @override
  void onInit() {
    super.onInit();
    searchController = TextEditingController();
    searchDialogController = TextEditingController();
    getListButton();
    getListNameRecipe();
    _getListRecipeTrending();
    getListRecipeByIngredient(indexSelectedTitle.value);
    _getUserFrofile();
    _getTag();
  }

  get numberOfTimeMaxValue => _numberOfTimeMaxValue;

  set numberOfTimeMaxValue(value) {
    _numberOfTimeMaxValue = value;
  }

  void getListButton() {
    for (int i = 0; i < 3; i++) {
      ButtonDialog btn = ButtonDialog("");
      btn.title = _listTitle[i];
      listButton.add(btn);
    }
  }

  void getListNameRecipe() {
    for (int i = 0; i < listNameIngredient.length; i++) {
      TitleIngredient titleIngredient = TitleIngredient(0, "");
      titleIngredient.index = i;
      titleIngredient.name = listNameIngredient[i];
      listTitleIngredient.add(titleIngredient);
    }
  }

  void _getListRecipeTrending() async {
    var response = await _searchSuggestionRepository.getRecipesTrending();

    if (response.isSuccess) {
      if (response.data != null) {
        listRecipeTrending.addAll(response.data!);
        for (var recipeTrendingIndex = 0;
            recipeTrendingIndex < listRecipeTrending.length;
            recipeTrendingIndex++) {
          RecipeModel recipeModel = listRecipeTrending[recipeTrendingIndex];
          SearchSuggestionModel searchSuggestionModel =
              SearchSuggestionModel("", "");

          String linkImage = await _getLinkImage(recipeModel.galleries![0]);
          searchSuggestionModel.nameRecipe = recipeModel.name;
          searchSuggestionModel.linkImageRecipe = linkImage;
          _listSearchSuggestionTrending.add(searchSuggestionModel);
        }
        lengthListSearchTrending = _listSearchSuggestionTrending.length;
      } else {
        //TODO
      }
    } else if (response.isFailure) {
      var error = response.error;
      if (error is APIFailure) {
        _setListRecipeTrendingErr(error, error.errorResponse);
      }
    }
  }

  void _setListRecipeTrendingErr(APIFailure error, String errorResponse) {}

  Future<String> _getLinkImage(String idImage) async {
    var responseLinkImage =
        await _searchSuggestionRepository.getImageFromStorage(idImage);
    if (responseLinkImage.isSuccess) {
      return responseLinkImage.data!;
    } else if (responseLinkImage.isFailure) {
    } else if (responseLinkImage.isFailure) {
      var error = responseLinkImage.error;
      if (error is APIFailure) {
        _setImageErr(error, error.errorResponse);
      }
    }
    return "";
  }

  void _setImageErr(APIFailure error, String errorResponse) {}

  void getListRecipeByIngredient(int index) async {
    listSearchSuggestionIngredient.clear();
    lengthListSearchTrending = 0;
    var responseIngredient = await _searchSuggestionRepository
        .getRecipesByIngredient(listNameIngredient[index]);
    if (responseIngredient.isSuccess) {
      if (responseIngredient.data != null) {
        listRecipeIngredient.clear();
        listRecipeIngredient.addAll(responseIngredient.data!);

        for (var recipeIngredientIndex = 0;
            recipeIngredientIndex < listRecipeIngredient.length;
            recipeIngredientIndex++) {
          RecipeModel recipeModel = listRecipeIngredient[recipeIngredientIndex];
          SearchSuggestionModel searchSuggestionModel =
              SearchSuggestionModel("", "");
          String linkImage = await _getLinkImage(recipeModel.galleries![0]);
          searchSuggestionModel.nameRecipe = recipeModel.name;
          searchSuggestionModel.linkImageRecipe = linkImage;
          _listSearchSuggestionIngredient.add(searchSuggestionModel);
        }
        lengthListSearchIngredient = _listSearchSuggestionIngredient.length;
      }
      lengthListSearchTrending = _listSearchSuggestionTrending.length;
    } else {
      //TODO
    }
    if (responseIngredient.isFailure) {
      var error = responseIngredient.error;
      if (error is APIFailure) {
        _setListRecipeTrendingErr(error, error.errorResponse);
      }
    }
  }

  void filterRecipe(
      double numberOfIngredient, int numberTimeMin, int numberTimeMax) async {
    int numberOfIngredientInt = numberOfIngredient.toInt() + 1;

    await _filterIngredient(numberOfIngredientInt);

    await _filterTimeServing(numberTimeMin, numberTimeMax);

    _checkListMap();
  }

  Future _filterIngredient(int numberOfIngredient) async {
    var responseRecipeByIngredient = await _searchSuggestionRepository
        .getRecipesIDByNumberIngredient(numberOfIngredient);
    _listIDFilterNumberIngredient.clear();
    if (responseRecipeByIngredient.isSuccess) {
      if (responseRecipeByIngredient.data != null) {
        _listIDFilterNumberIngredient.addAll(responseRecipeByIngredient.data!);
        log(_listIDFilterNumberIngredient.toString());
      }
    } else {
      //TODO
    }
    if (responseRecipeByIngredient.isFailure) {
      var error = responseRecipeByIngredient.error;
      if (error is APIFailure) {
        _setListRecipeTrendingErr(error, error.errorResponse);
      }
    }
  }

  Future _filterTimeServing(int numberTimeMin, int numberTimeMax) async {
    _listIDRecipeFilterServingTime.clear();
    var responseInfo = await _searchSuggestionRepository
        .getListIDInfoByTimeServing(numberTimeMin, numberTimeMax);
    if (responseInfo.isSuccess) {
      if (responseInfo.data != null) {
        for (int i = 0; i < responseInfo.data!.length; i++) {
          var responseIDRecipe = await _searchSuggestionRepository
              .getIDRecipeByIDInfo(responseInfo.data![i]);
          if (responseIDRecipe.isSuccess) {
            if (responseIDRecipe.data != null) {
              String idRecipe = responseIDRecipe.data!;
              _listIDRecipeFilterServingTime.add(idRecipe);
            } else {
              //TODO
            }
          }
        }
      }
    } else {
      //TODO
    }
    if (responseInfo.isFailure) {
      var error = responseInfo.error;
      if (error is APIFailure) {
        _setListRecipeTrendingErr(error, error.errorResponse);
      }
    }
  }

  var listFilter = [].obs;

  void _checkListMap() async {
    listFilter.clear();
    listRecipeFilter.clear();
    listRecipeData.clear();
    listFilter.addAll(_listIDRecipeFilterServingTime.where((e) {
      return _listIDFilterNumberIngredient.contains(e);
    }).toList());

    for (int i = 0; i < listFilter.length; i++) {
      String idRecipe = listFilter[i];
      var responseRecipeFilter =
          await _searchSuggestionRepository.getRecipeByIDRecipe(idRecipe);
      if (responseRecipeFilter.data != null) {
        RecipeModel recipe = responseRecipeFilter.data!;
        listRecipeData.add(recipe);
        if (recipe.galleries != null) {
          var responseLinkImage = await _searchSuggestionRepository
              .getImageFromStorage(recipe.galleries![0]);
          if (responseLinkImage.isSuccess) {
            if (responseLinkImage.data != null) {
              SearchSuggestionModel searchSuggestionModel =
                  SearchSuggestionModel("", "");
              searchSuggestionModel.linkImageRecipe = responseLinkImage.data!;
              searchSuggestionModel.nameRecipe = recipe.name;
              listRecipeFilter.add(searchSuggestionModel);
            } else {
              //TODO
            }
          }
        }

        log(listRecipeFilter.toString());
      } else {
        //TODO
      }
    }
    await _getIDCookBookByIDRecipe();
  }

  List<String> _listIDCookBook = [];

  List<String> get listIDCookBook => _listIDCookBook;

  set listIDCookBook(List<String> value) {
    _listIDCookBook = value;
  }

  Future _getIDCookBookByIDRecipe() async {
    _listIDCookBook.clear();
    for (int i = 0; i < listFilter.length; i++) {
      var responseIDCookBook = await _searchSuggestionRepository
          .getIDCookBookByIDRecipe(listFilter[i]);
      if (responseIDCookBook.isSuccess) {
        if (responseIDCookBook.data != null) {
          _listIDCookBook.addAll(responseIDCookBook.data!);
        } else {
          //TODO
        }
      }
    }
    _removeElementListIDCookBook();
  }

  void _removeElementListIDCookBook() {
    var listIDLike = [];
    for (int i = 0; i < _listIDCookBook.length; i++) {
      for (int j = i + 1; j < _listIDCookBook.length; j++) {
        if (_listIDCookBook[i] == _listIDCookBook[j]) {
          listIDLike.add(_listIDCookBook[i]);
        }
      }
    }
  }

  var listUser = <SearchSuggestionProfile>[].obs;
  var listAvatar = <String>[].obs;

  void _getUserFrofile() async {
    listUser.clear();
    var responseUser = await _searchSuggestionRepository.getUser();
    if (responseUser.isSuccess) {
      if (responseUser.data != null) {
        for (int i = 0; i < responseUser.data!.length; i++) {
          UserModel userModel = responseUser.data![i];

          var responseLinkImage = await _searchSuggestionRepository
              .getImageFromStorage(userModel.avatar!);
          if (responseLinkImage.isSuccess) {
            if (responseLinkImage.data != null) {
              String linkImage = responseLinkImage.data!;

              listAvatar.add(linkImage);
              SearchSuggestionProfile searchSuggestionProfile =
                  SearchSuggestionProfile("", "", "", 0, 0);
              searchSuggestionProfile.nameUser = userModel.account.fullname;
              searchSuggestionProfile.linkAvata = linkImage;
              searchSuggestionProfile.numberFollowers = userModel.likes!;
              searchSuggestionProfile.numberRecipe = userModel.likes!;
              listUser.add(searchSuggestionProfile);
            }
          } else {
            //TODO
          }
        }
      } else {
        //TODO
      }
    }
  }

  var _listTag = <TagModel>[].obs;

  get listTag => _listTag;

  set listTag(value) {
    _listTag = value;
  }

  void _getTag() async {
    _listTag.clear();
    var responseTag = await _searchSuggestionRepository.getTag();

    if (responseTag.isSuccess) {
      if (responseTag.data != null) {
        _listTag.addAll(responseTag.data!);
      } else {
        //TODO
      }
    }
  }
}
