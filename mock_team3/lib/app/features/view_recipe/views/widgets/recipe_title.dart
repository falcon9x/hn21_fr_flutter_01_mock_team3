import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../edit_recipe/view_models/edit_recipe_view_model.dart';
import '../../../edit_recipe/views/widgets/cache_network_image_custom.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';

class RecipeTitle extends StatelessWidget {
  RecipeTitle({
    Key? key,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();

  @override
  Widget build(BuildContext context) {
    double _widthScreen = Get.width;
    double imageHeight = _widthScreen * 358 / 375;
    return SizedBox(
      height: imageHeight,
      width: _widthScreen,
      child: Stack(
        children: [
          _buildImage(imageHeight, _widthScreen),
          _buildBackground(),
          _buildRecipeName()
        ],
      ),
    );
  }

  Padding _buildRecipeName() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            _editRecipeController.getMyRecipeEdit.recipeName,
            style: appTextTheme.headline3?.copyWith(
              color: AppColors.colorWhite,
            ),
          ),
        ],
      ),
    );
  }

  Container _buildBackground() {
    return Container(
      color: const Color(0xFF282928).withOpacity(0.5),
    );
  }

  SizedBox _buildImage(double imageHeight, double _widthScreen) {
    return SizedBox(
      height: imageHeight,
      width: _widthScreen,
      child: Obx(() => CacheNetworkImageCustom(
          urlImage: _editRecipeController.getMyRecipeEdit.cover,
          boxFit: BoxFit.fill)),
    );
  }
}
