import 'package:flutter/material.dart';

import 'package:mock_team3/app/core/app_assets.dart';

class Ingredients extends StatelessWidget {
  const Ingredients({
    Key? key,
    required this.ingredients,
  }) : super(key: key);
  final List<String> ingredients;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: ingredients.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(bottom: 17),
              child: Row(
                children: [
                  _buildIngredientImages(),
                  _buildIngredientNames(index),
                ],
              ),
            );
          }),
    );
  }

  Padding _buildIngredientNames(int index) {
    return Padding(
      padding: const EdgeInsets.only(left: 5),
      child: Text(ingredients[index]),
    );
  }

  SizedBox _buildIngredientImages() {
    return SizedBox(
      height: 50,
      width: 50,
      child: CircleAvatar(
        backgroundImage: AssetImage(ViewRecipeAssets.imageProfile),
      ),
    );
  }
}
