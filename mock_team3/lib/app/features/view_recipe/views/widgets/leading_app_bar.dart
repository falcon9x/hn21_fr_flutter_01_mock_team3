import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';

class LeadingAppBar extends StatelessWidget {
  const LeadingAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SvgPicture.asset(IconBlack.iconLeft, color: AppColors.colorWhite),
        Text(
          LocaleKeys.viewRecipeString_textBackButton.tr,
          style: appTextTheme.caption?.copyWith(color: AppColors.colorWhite),
        )
      ],
    );
  }
}
