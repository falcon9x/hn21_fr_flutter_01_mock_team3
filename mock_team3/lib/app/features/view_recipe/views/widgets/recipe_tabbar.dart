import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../generated/locales.g.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../widgets/custom_tab_bar_indicator.dart';
import '../../../edit_recipe/view_models/edit_recipe_view_model.dart';
import '../../../edit_recipe/views/widgets/fourth_section.dart';
import '../../../edit_recipe/views/widgets/third_section.dart';
import 'ingredients.dart';

class RecipeTabBar extends StatelessWidget {
  RecipeTabBar({
    Key? key,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, top: 20),
      child: DefaultTabController(
        length: 3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _buildTabBar(),
            Divider(
              height: 0,
              thickness: 1,
              color: AppColors.colorDivider,
            ),
            _buildTabBarView()
          ],
        ),
      ),
    );
  }

  Padding _buildTabBarView() {
    return Padding(
      padding: const EdgeInsets.only(top: 25),
      child: SizedBox(
        height: 500,
        child: TabBarView(children: [
          Ingredients(
              ingredients: _editRecipeController.getMyRecipeEdit.ingredients),
          ThirdSection(showTitle: false),
          FourthSection(showTitle: false)
        ]),
      ),
    );
  }

  Widget _buildTabBar() {
    return TabBar(
      isScrollable: true,
      onTap: (value) {
        _editRecipeController.updateIndexTabBar(value);
      },
      indicator: CircleTabIndicator(
          color: AppColors.mainColor, radius: 100, indicatorHeight: 4),
      tabs: [
        _buildContextTabBar(LocaleKeys.viewRecipeString_firstNameTabbar.tr, 0),
        _buildContextTabBar(LocaleKeys.viewRecipeString_secondNameTabbar.tr, 1),
        _buildContextTabBar(LocaleKeys.viewRecipeString_thirdNameTabbar.tr, 2),
      ],
    );
  }

  Tab _buildContextTabBar(String tabBarContext, int index) {
    return Tab(
      height: kToolbarHeight,
      child: Obx(
        () => Text(
          tabBarContext,
          style: _editRecipeController.getIndexTabBar == index
              ? appTextTheme.bodyText1
              : appTextTheme.bodyText1!
                  .copyWith(color: AppColors.colorBlackOpacity),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
