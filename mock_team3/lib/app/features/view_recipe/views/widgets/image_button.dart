import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_border_radius.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_text_theme.dart';

class ImageButton extends StatelessWidget {
  const ImageButton({
    Key? key,
    required this.onpress,
  }) : super(key: key);
  final VoidCallback onpress;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(left: 0),
      child: Container(
        decoration: BoxDecoration(
            color: const Color(0x00000000).withOpacity(0.4),
            borderRadius: AppBorderRadius.appborderRadius,
            border: Border.all(color: AppColors.colorWhite)),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 6),
          child: InkWell(
            onTap: () {
              onpress();
            },
            child: Row(
              children: [
                _buildIcon(),
                _buildButtonTitle(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Text _buildButtonTitle() {
    return Text(
      LocaleKeys.viewRecipeString_textButtonCook.tr,
      style: appTextTheme.caption
          ?.copyWith(color: AppColors.colorWhite, fontWeight: FontWeight.w700),
    );
  }

  SizedBox _buildIcon() {
    double heightIcon = 24;
    return SizedBox(
      height: heightIcon,
      width: heightIcon,
      child: Padding(
        padding: const EdgeInsets.all(3),
        child: SvgPicture.asset(
          IconWhile.iconWhitePlay,
        ),
      ),
    );
  }
}
