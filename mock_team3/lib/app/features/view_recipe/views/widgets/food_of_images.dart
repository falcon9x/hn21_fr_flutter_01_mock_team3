import 'package:flutter/material.dart';

import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';

import '../../../../core/app_text_theme.dart';

class ImagesOfFood extends StatelessWidget {
  const ImagesOfFood({
    Key? key,
    required this.widthImage,
    required this.heightImage,
    required this.urlImage,
  }) : super(key: key);
  final double widthImage;
  final double heightImage;
  final List<String> urlImage;
  @override
  Widget build(BuildContext context) {
    int imageRemain = urlImage.length > 3 ? (urlImage.length - 3) : 0;
    return SizedBox(
      height: heightImage,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: imageRemain > 0 ? 3 : urlImage.length,
        itemBuilder: ((context, index) => index != 2
            ? Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: _buildImage(index),
              )
            : _buildImages(imageRemain > 0, imageRemain, index)),
      ),
    );
  }

  SizedBox _buildImage(int index) {
    return SizedBox(
        width: widthImage,
        height: heightImage,
        child: CacheNetworkImageCustom(
          urlImage: urlImage[index],
          boxFit: BoxFit.fill,
        ));
  }

  SizedBox _buildImages(bool isShow, int imageRemain, int index) {
    return SizedBox(
      width: widthImage,
      height: heightImage,
      child: Stack(
        children: [
          _buildImage(index),
          isShow
              ? Container(
                  color: AppColors.colorWhite.withOpacity(0.6),
                  child: Center(
                    child: Text(
                      "$imageRemain+",
                      style: appTextTheme.headline4,
                    ),
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }
}
