import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/view_models/edit_recipe_view_model.dart';

import '../../../core/app_padding.dart';
import 'widgets/food_of_images.dart';
import 'widgets/image_button.dart';
import 'widgets/leading_app_bar.dart';
import 'widgets/recipe_tabbar.dart';
import 'widgets/recipe_title.dart';

class ViewRecipeScreen extends GetView<EditRecipeViewModel> {
  const ViewRecipeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _widthScreen = Get.width;
    double _widthImage = (_widthScreen - 64) / 3;
    double _heightImage = _widthImage * 94 / 103;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: _buildAppBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            RecipeTitle(),
            Obx(() => Padding(
                  padding: AppPadding.defaultPadding.copyWith(top: 25),
                  child: ImagesOfFood(
                    widthImage: _widthImage,
                    heightImage: _heightImage,
                    urlImage: controller.getUrlImagesEdit,
                  ),
                )),
            RecipeTabBar(),
          ],
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      leadingWidth: 0,
      leading: const Text(""),
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: Row(
        children: [
          InkWell(
            onTap: () {
              controller.backToProfile();
            },
            child: const LeadingAppBar(),
          ),
          const Spacer(),
          ImageButton(
            onpress: () {
              controller.goToCookMode();
            },
          )
        ],
      ),
    );
  }
}
