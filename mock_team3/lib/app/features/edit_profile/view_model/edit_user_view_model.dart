import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/user/user_repository.dart';

import '../../../../data/user/model/user_model.dart';
import '../../../utils/share_prefs_key.dart';
import '../../../utils/share_prefs_repository.dart';

typedef CallBackWithParams = Function(String value);

class EditUserViewModel extends BaseViewModel with StateMixin<UserModel> {
  final UserRepository _userRepository;
  final SharePrefsRepository _sharePrefsRepository;

  final userModel = UserModel.withInitial().obs;
  final userAvatar = "".obs;
  String idUserCurrent = "";

  EditUserViewModel(this._userRepository, this._sharePrefsRepository);

  void getUserModel() async {
    idUserCurrent = _sharePrefsRepository.getString(SharePrefsKey.userId);
    DataResult result = await _userRepository.getUserProfile(idUserCurrent);
    change(userModel.value, status: RxStatus.loading());
    if (result.isSuccess) {
      userModel.value = (result.data as UserModel);
      userAvatar.value =
          await _getImageDownloadUrl(userModel.value.avatar ?? "");
      change(userModel.value, status: RxStatus.success());
    } else {
      if (result is APIFailure) {
        if ((result as APIFailure).errorResponse == "data null") {
          change(userModel.value, status: RxStatus.empty());
        } else {
          change(userModel.value,
              status: RxStatus.error((result as APIFailure).errorResponse));
        }
      }
    }
  }

  void updateUserProfile(UserModel user, VoidCallback showSuccess,
      CallBackWithParams showFailure) async {
    final dataResult =
        await _userRepository.updateUserProfile(user, idUserCurrent);
    if (dataResult.isSuccess) {
      showSuccess();
    } else if (dataResult.isFailure && dataResult.error is APIFailure) {
      showFailure((dataResult as APIFailure).errorResponse);
    }
  }

  Future<String> _getImageDownloadUrl(String imagePath) async {
    DataResult result = await _userRepository.getImageFromStorage(imagePath);
    if (result.isSuccess) {
      return result.data;
    } else {
      return "";
    }
  }

  @override
  void onInit() {
    super.onInit();
    getUserModel();
  }
}
