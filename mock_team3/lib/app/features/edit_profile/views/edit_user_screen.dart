import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../../core/app_colors.dart';
import '../../../core/app_padding.dart';
import '../../../core/app_text_theme.dart';
import '../../edit_recipe/views/widgets/cache_network_image_custom.dart';
import '../../user_profile/view_model/user_profile_view_model.dart';
import '../../../widgets/app_rounded_button.dart';
import '../../../widgets/app_text_form_field.dart';
import '../../../../generated/locales.g.dart';

import '../../../core/app_assets.dart';

class EditUserScreen extends GetView<UserProfileViewModel> {
  const EditUserScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxHeight: Get.height),
          child: Padding(
            padding: AppPadding.defaultPadding,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildHeader(),
                const Spacer(),
                _buildAvatar(),
                _buildTextUnderAvatar(),
                _buildFullNameField(),
                _buildBioField(),
                _buildEmailField(),
                _buildPhoneField(),
                const Spacer(),
                _buildButtonSaveProfile(),
                const Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildButtonSaveProfile() {
    return Flexible(
      flex: 3,
      child: SizedBox(
        width: double.infinity,
        child: AppRoundedButton(
          text: LocaleKeys.editUserString_textButton.tr,
          isCircle: false,
          onPress: () => _onButtonSaveProfile(),
        ),
      ),
    );
  }

  Widget _buildPhoneField() {
    return Flexible(
      flex: 3,
      child: Obx(() => AppTextFormField(
            labelText: LocaleKeys.editUserString_fourthLabelFormField.tr,
            isPassword: false,
            initialValue: controller.userModel.value.phone,
            keyboardType: TextInputType.phone,
            onchange: (value) {
              controller.userModel.value.phone = value;
            },
          )),
    );
  }

  Widget _buildEmailField() {
    return Flexible(
      flex: 3,
      child: Obx(() => AppTextFormField(
          labelText: LocaleKeys.editUserString_thirdLabelFormField.tr,
          isPassword: false,
          initialValue: controller.userModel.value.account.email,
          onchange: (value) {
            controller.userModel.value.account.email = value;
          })),
    );
  }

  Widget _buildBioField() {
    return Flexible(
      flex: 3,
      child: Obx(() => AppTextFormField(
          labelText: LocaleKeys.editUserString_secondLabelFormField.tr,
          isPassword: false,
          initialValue: controller.userModel.value.bio,
          onchange: (value) {
            controller.userModel.value.bio = value;
          })),
    );
  }

  Widget _buildFullNameField() {
    return Flexible(
      flex: 3,
      child: Obx(() => AppTextFormField(
          labelText: LocaleKeys.editUserString_firstLabelFormField.tr,
          isPassword: false,
          initialValue: controller.userModel.value.account.fullname,
          onchange: (value) {
            controller.userModel.value.account.fullname = value;
          })),
    );
  }

  Widget _buildTextUnderAvatar() {
    return Flexible(
      flex: 2,
      child: GestureDetector(
        onTap: () => _openImageGallery(),
        child: Center(
          child: Text(
            LocaleKeys.editUserString_titleUnderAvatar.tr,
            style: appTextTheme.headline5!.copyWith(color: AppColors.mainColor),
          ),
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return Flexible(
      flex: 3,
      fit: FlexFit.tight,
      child: Center(
        child: AspectRatio(
          aspectRatio: 1 / 1,
          child: Obx(
            () => !(controller.imagePath.value == "")
                ? ClipOval(
                    child: CircleAvatar(
                    backgroundImage:
                        FileImage(File(controller.imagePath.value)),
                  ))
                : ClipOval(
                    child: CacheNetworkImageCustom(
                        urlImage: controller.userAvatar.value,
                        boxFit: BoxFit.cover)),
          ),
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Flexible(
      flex: 3,
      child: Text(
        LocaleKeys.editUserString_titleHeader.tr,
        style: appTextTheme.headline3,
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      leadingWidth: 0,
      leading: const Text(""),
      title: InkWell(
        onTap: () => _backToProfile(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(IconBlack.iconLeft),
            Text(
              LocaleKeys.editUserString_titleAppBar.tr,
              style: appTextTheme.caption,
            )
          ],
        ),
      ),
    );
  }

  void _backToProfile() {
    Get.back();
  }

  void _onButtonSaveProfile() {
    controller.updateUserProfile(() {
      _showSuccess();
    }, (value) => _showFailure(value));
  }

  void _openImageGallery() {
    controller.getImageFromGallery();
  }

  void _showFailure(String value) {
    Get.snackbar("Notification", value, snackPosition: SnackPosition.BOTTOM);
  }

  void _showSuccess() {
    Get.back();
  }
}
