import 'package:flutter/cupertino.dart';

import '../../../base/base_viewmodel.dart';
import '../../../utils/share_prefs_key.dart';

import '../../../utils/share_prefs_repository.dart';

class OnBoardingViewModel extends BaseViewModel {
  final SharePrefsRepository _sharedPrefsRepository;
  OnBoardingViewModel(
    this._sharedPrefsRepository,
  );
  void delayScreen(VoidCallback goToLogin) {
    Future.delayed(const Duration(seconds: 3), () {
      goToLogin();
    });
  }

  Future<bool> checkLogin() async {
    return _sharedPrefsRepository.getBool(SharePrefsKey.keepLogIn);
  }
}
