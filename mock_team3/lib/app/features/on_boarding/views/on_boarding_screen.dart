import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_assets.dart';

import '../../../core/app_colors.dart';
import '../../../routes/app_routes.dart';
import '../view_models/on_boarding_view_model.dart';

class OnBoardingScreen extends GetView<OnBoardingViewModel> {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = Get.height;
    double screenWidth = Get.width;
    EdgeInsets _logoPadding = EdgeInsets.only(
        top: screenHeight * 360 / 812, left: screenWidth * 116 / 375);
    double _imageHeight = screenHeight * 475 / 812;
    controller.delayScreen(() => _goToLogin());
    return Container(
      color: AppColors.colorWhite,
      child: Stack(
        children: [
          _buildImage(screenWidth, _imageHeight),
          _buildGradientImage(screenHeight),
          _buildLogo(_logoPadding),
        ],
      ),
    );
  }

  Widget _buildLogo(EdgeInsets _logoPadding) {
    return Padding(
      padding: _logoPadding,
      child: SvgPicture.asset(
        OnBoardingAssets.assetsLogo,
      ),
    );
  }

  Widget _buildGradientImage(double screenHeight) {
    List<double> _gradientStops = [0, 0.55, 0.65, 1];
    return Container(
      height: screenHeight,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          stops: _gradientStops,
          colors: [
            AppColors.colorWhite,
            AppColors.colorWhite,
            AppColors.colorWhite.withOpacity(0.01),
            AppColors.colorWhite.withOpacity(0.01),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
    );
  }

  Widget _buildImage(double screenWidth, double _imageHeight) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        SizedBox(
          width: screenWidth,
          height: _imageHeight,
          child: Image.asset(
            OnBoardingAssets.assetsImage,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }

  Future<void> _goToLogin() async {
    if (await controller.checkLogin()) {
      Get.offNamed(AppRoute.recipeFeed);
    } else {
      Get.offNamed(AppRoute.loginScreen);
    }
  }
}
