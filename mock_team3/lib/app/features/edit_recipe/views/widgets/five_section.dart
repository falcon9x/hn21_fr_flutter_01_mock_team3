import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../../browse_my_recipe/views/widgets/hastag_filter.dart';

class FiveSection extends StatelessWidget {
  const FiveSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Text(
            LocaleKeys.editRecipe_fourthDialogAdd.tr,
            style: appTextTheme.subtitle1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              HastagFilter(
                boxWidth: 190 * _screenWidth / 375,
              ),
              _buildButton(_screenWidth)
            ],
          ),
        )
      ],
    );
  }

  Container _buildButton(double _screenWidth) {
    return Container(
      height: 50,
      width: 120 * _screenWidth / 375,
      decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(color: AppColors.mainColor),
          borderRadius: BorderRadius.circular(8)),
      child: ElevatedButton(
        onPressed: () {},
        child: Text(
          LocaleKeys.editRecipe_contentButtonOfAdd.tr,
          style: appTextTheme.button?.copyWith(overflow: TextOverflow.ellipsis),
        ),
        style: ElevatedButton.styleFrom(
          primary: Colors.transparent,
          elevation: 0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        ),
      ),
    );
  }
}
