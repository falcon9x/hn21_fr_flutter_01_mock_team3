import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:mock_team3/app/core/app_padding.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cook_steps.dart';

import '../../../../../generated/locales.g.dart';
import '../../view_models/edit_recipe_view_model.dart';
import 'section_title.dart';

class ThirdSection extends StatelessWidget {
  ThirdSection({
    Key? key,
    required this.showTitle,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  final bool showTitle;
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _textWdith = _screenWidth - 117;
    return Padding(
      padding: EditRecipePadding.editRecipePadding,
      child: SizedBox(
        child: Padding(
          padding: EditRecipePadding.cardPadding,
          child: Column(
            children: [
              showTitle
                  ? Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: SectionTitle(
                          text: LocaleKeys.editRecipe_thirdSection.tr,
                          onpress: () {
                            _editRecipeController.forwarAnimation(
                                _editRecipeController
                                    .thirdSectionAnimationController);
                          }),
                    )
                  : const SizedBox(),
              Obx(
                () => Column(
                  children: CookSteps.generateSteps(
                      _textWdith, _editRecipeController.getDirectionDetails),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
