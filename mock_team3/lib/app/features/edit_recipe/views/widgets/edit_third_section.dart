import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_text_theme.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_border_radius.dart';
import '../../../../core/app_colors.dart';
import '../../../../widgets/app_rounded_button.dart';
import '../../../../widgets/app_text_form_field.dart';
import '../../../../widgets/container_border_dash.dart';
import '../../view_models/edit_recipe_view_model.dart';
import 'edit_section_title.dart';

class EditThirdSection extends StatelessWidget {
  EditThirdSection({Key? key}) : super(key: key);

  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _scrennHeight = Get.height;
    return Container(
      decoration:
          BoxDecoration(color: const Color(0xFF282928).withOpacity(0.5)),
      child: Column(
        children: [
          SizedBox(
            height: _scrennHeight * 0.15,
            width: _screenWidth,
          ),
          SizedBox(
            height: _scrennHeight * 0.85,
            width: _screenWidth,
            child: Container(
              width: _screenWidth,
              decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      children: [
                        EditSectionTitle(
                          onpress: () => _editRecipeController.reverseAnimation(
                              _editRecipeController
                                  .thirdSectionAnimationController),
                          text: LocaleKeys.editDirections_title.tr,
                        ),
                        _buildVideoImage(_screenWidth),
                        _buildVideoName(_screenWidth),
                        _buildDivider(),
                        Obx(
                          () => Column(children: [
                            for (int indexDirectionDetail = 0;
                                indexDirectionDetail <
                                    _editRecipeController
                                        .getDirectionDetails.length;
                                indexDirectionDetail++)
                              _buildVideoContent(
                                  _screenWidth,
                                  indexDirectionDetail,
                                  _editRecipeController.getShowEditDiretions[
                                      indexDirectionDetail])
                          ]),
                        ),
                        _buildAddDirection(_screenWidth),
                        _buildSaveDirection(_screenWidth)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding _buildSaveDirection(double _screenWidth) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 20),
      child: AppRoundedButton(
        onPress: () => _editRecipeController.saveDirection(),
        width: _screenWidth,
        isCircle: false,
        text: LocaleKeys.editDirections_contentButton.tr,
      ),
    );
  }

  Padding _buildAddDirection(double screenWidth) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 60),
      child: InkWell(
        onTap: () {
          _editRecipeController.addDirection();
        },
        child: ContainerBorderDash(
          boxHeight: 50,
          boxWidth: screenWidth - 50,
          color: AppColors.colorGrey,
          thickness: 1,
          circularValue: 8,
          child: Center(
              child: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Row(
              children: [
                SvgPicture.asset(IconBlack.iconAdd),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: SizedBox(
                    width: screenWidth * 0.65,
                    child: Text(
                      LocaleKeys.editDirections_labelText.tr,
                      style: appTextTheme.bodyText1?.copyWith(
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )),
        ),
      ),
    );
  }

  Padding _buildVideoContent(
      double _screenWidth, int index, bool showTextFiled) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 20,
            width: 20,
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(
                color: AppColors.mainColor,
              ),
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text(
                "${index + 1}",
                style:
                    appTextTheme.caption?.copyWith(color: AppColors.mainColor),
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 15),
              child: AppTextFormField(
                showTextField: showTextFiled,
                initialValue:
                    _editRecipeController.getDirectionDetails[index].name,
                onchange: (value) {
                  _editRecipeController.changeDirection(index, value);
                },
                focusedBorderColor: AppColors.mainColor,
                isPassword: false,
                child: Row(
                  children: [
                    SizedBox(
                      width: _screenWidth * 0.55,
                      child: Text(
                        showTextFiled
                            ? "Write Ingredient"
                            : _editRecipeController
                                .getDirectionDetails[index].name,
                        style: appTextTheme.subtitle2?.copyWith(
                            overflow: TextOverflow.clip,
                            color: showTextFiled
                                ? AppColors.textGrey
                                : AppColors.colorBlack),
                      ),
                    ),
                    const Spacer(),
                    showTextFiled
                        ? InkWell(
                            onTap: () {
                              _editRecipeController.deleteEditDirection(index);
                            },
                            child: SvgPicture.asset(IconRed.iconRedDelete))
                        : const SizedBox(),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: InkWell(
                          onTap: () {
                            _editRecipeController
                                .changeShowEditDirection(index);
                          },
                          child: SizedBox(
                              height: 24,
                              width: 24,
                              child: SvgPicture.asset(showTextFiled
                                  ? IconGreen.iconEdit
                                  : IconBlack.iconEdit))),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding _buildDivider() {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 30),
      child: Divider(
        color: AppColors.colorDivider,
        height: 0,
      ),
    );
  }

  Padding _buildVideoName(double _screenWidth) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 10),
      child: Row(
        children: [
          SvgPicture.asset(IconBlack.iconUpload),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: SizedBox(
              width: _screenWidth * 0.75,
              child: Text(
                _editRecipeController.getDirection.video,
                style: appTextTheme.bodyText1
                    ?.copyWith(overflow: TextOverflow.ellipsis),
              ),
            ),
          )
        ],
      ),
    );
  }

  Padding _buildVideoImage(double _screenWidth) {
    double heighImage = (_screenWidth - 50) * 168 / 325;
    double heighIcon = 24;
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 30),
      child: SizedBox(
          height: heighImage,
          width: _screenWidth,
          child: Stack(
            children: [
              Obx(
                () => CacheNetworkImageCustom(
                    urlImage: _editRecipeController.getMyRecipeEdit.cover,
                    boxFit: BoxFit.fill),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                          height: 30,
                          decoration: BoxDecoration(
                              color: const Color(0x00000000).withOpacity(0.4),
                              borderRadius: AppBorderRadius.appborderRadius,
                              border: Border.all(color: AppColors.colorWhite)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 3, horizontal: 6),
                            child: Row(
                              children: [
                                SizedBox(
                                  height: heighIcon,
                                  width: heighIcon,
                                  child: SvgPicture.asset(
                                    IconBlack.iconEdit,
                                    color: AppColors.colorWhite,
                                  ),
                                ),
                                Text(
                                  LocaleKeys
                                      .editDirections_contentImageButton.tr,
                                  style: appTextTheme.caption?.copyWith(
                                      color: AppColors.colorWhite,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {},
                          child: Container(
                            height: 30,
                            decoration: BoxDecoration(
                                color: const Color(0x00000000).withOpacity(0.4),
                                borderRadius: AppBorderRadius.appborderRadius,
                                border:
                                    Border.all(color: AppColors.colorWhite)),
                            child: SizedBox(
                              width: 30,
                              child: SvgPicture.asset(
                                IconBlack.iconDelete,
                                color: AppColors.colorWhite,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
