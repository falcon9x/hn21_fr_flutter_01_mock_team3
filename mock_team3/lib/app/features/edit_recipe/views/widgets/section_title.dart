import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_text_theme.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({Key? key, required this.text, required this.onpress})
      : super(key: key);
  final String text;
  final VoidCallback onpress;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text,
          style: appTextTheme.headline5,
        ),
        InkWell(
          onTap: onpress,
          child: SvgPicture.asset(IconGreen.iconEdit),
        ),
      ],
    );
  }
}
