import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CacheNetworkImageCustom extends StatelessWidget {
  const CacheNetworkImageCustom({
    Key? key,
    required this.urlImage,
    required this.boxFit,
  }) : super(key: key);

  final String urlImage;
  final BoxFit boxFit;
  @override
  Widget build(BuildContext context) {
    double heightCircularProgress = 50;
    return urlImage.isNotEmpty
        ? CachedNetworkImage(
            imageUrl: urlImage,
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                Center(
              child: SizedBox(
                height: heightCircularProgress,
                width: heightCircularProgress,
                child:
                    CircularProgressIndicator(value: downloadProgress.progress),
              ),
            ),
            errorWidget: (context, url, error) => const Icon(Icons.error),
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: boxFit,
                ),
              ),
            ),
          )
        : Center(
            child: SizedBox(
              height: heightCircularProgress,
              width: heightCircularProgress,
              child: const CircularProgressIndicator(),
            ),
          );
  }
}
