import 'package:flutter/material.dart';

import '../../../../../data/direction_detail/model/direction_detail_model.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';

class CookSteps {
  static List<Widget> generateSteps(
      double textWidth, List<DirectionDetailModel> stepContent) {
    List<Widget> _steps = [];
    for (int i = 0; i < stepContent.length; i++) {
      _steps.add(
        Padding(
          padding:
              EdgeInsets.only(bottom: (i + 1) == stepContent.length ? 0 : 17),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  border: Border.all(
                    color: AppColors.mainColor,
                  ),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Text(
                    "${i + 1}",
                    style: appTextTheme.caption
                        ?.copyWith(color: AppColors.mainColor),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 17),
                child: SizedBox(
                  width: textWidth,
                  child: Text(
                    stepContent[i].name,
                    style: appTextTheme.bodyText2
                        ?.copyWith(overflow: TextOverflow.clip),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }
    return _steps;
  }
}
