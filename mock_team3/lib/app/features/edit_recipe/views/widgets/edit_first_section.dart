import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_padding.dart';
import '../../view_models/edit_recipe_view_model.dart';
import '../../../../widgets/app_rounded_button.dart';
import '../../../../../generated/locales.g.dart';
import 'first_section_images.dart';
import 'first_section_remove.dart';
import 'first_section_set_as_cover.dart';
import 'edit_section_title.dart';
import 'upload.dart';

class EditFirstSection extends StatelessWidget {
  EditFirstSection({Key? key}) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _scrennHeight = Get.height;
    return Container(
      decoration:
          BoxDecoration(color: const Color(0xFF282928).withOpacity(0.5)),
      child: Column(
        children: [
          SizedBox(
            height: _scrennHeight * 5 / 16,
            width: _screenWidth,
          ),
          SizedBox(
            height: _scrennHeight * 11 / 16,
            width: _screenWidth,
            child: Container(
              width: _screenWidth,
              decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: Material(
                color: Colors.transparent,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      children: [
                        EditSectionTitle(
                          onpress: () => _editRecipeController.reverseAnimation(
                              _editRecipeController
                                  .firstSectionAnimationController),
                          text: LocaleKeys.editGallery_title.tr,
                        ),
                        FirstSectionImages(),
                        FirstSectionSetAsCover(),
                        FirstSectionRemove(),
                        const UpLoad(),
                        _buildButton(_screenWidth)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildButton(double _screenWidth) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 20),
      child: AppRoundedButton(
        onPress: () => _editRecipeController.saveGallery(),
        width: _screenWidth,
        isCircle: false,
        text: LocaleKeys.editGallery_contentButton.tr,
      ),
    );
  }
}
