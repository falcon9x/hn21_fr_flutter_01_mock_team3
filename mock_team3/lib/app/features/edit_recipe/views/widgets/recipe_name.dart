import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/view_models/edit_recipe_view_model.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../widgets/app_text_form_field.dart';
import 'cache_network_image_custom.dart';

class RecipeName extends StatelessWidget {
  RecipeName({
    Key? key,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double heightImage = 65;
    double widhtImage = 63;
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
              height: heightImage,
              width: widhtImage,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Obx(() => CacheNetworkImageCustom(
                      urlImage: _editRecipeController.getMyRecipeEdit.cover,
                      boxFit: BoxFit.fill,
                    )),
              )),
          Flexible(
            child: Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Obx(
                  () => AppTextFormField(
                      initialValue:
                          _editRecipeController.getMyRecipeEdit.recipeName,
                      onchange: (value) {
                        _editRecipeController.changeNameRecipe(value);
                      },
                      labelText: LocaleKeys.editRecipe_recipeName.tr,
                      isPassword: false),
                )),
          ),
        ],
      ),
    );
  }
}
