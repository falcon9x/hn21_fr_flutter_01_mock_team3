import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/section_title.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_padding.dart';
import '../../../view_recipe/views/widgets/food_of_images.dart';
import '../../view_models/edit_recipe_view_model.dart';
import 'cache_network_image_custom.dart';

class FirstSection extends StatelessWidget {
  FirstSection({
    Key? key,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double _widthScreen = Get.width;
    double _widthImage = (_widthScreen - 94) / 3;
    double _heightImage = _widthImage;
    return Padding(
      padding: EditRecipePadding.editRecipePadding,
      child: SizedBox(
        child: Padding(
          padding: EditRecipePadding.cardPadding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SectionTitle(
                  text: LocaleKeys.editRecipe_firstSection.tr,
                  onpress: () {
                    _editRecipeController.forwarAnimation(
                        _editRecipeController.firstSectionAnimationController);
                  }),
              _buildFoodImage(_widthScreen),
              Padding(
                padding: const EdgeInsets.only(top: 7),
                child: Obx(
                  () => ImagesOfFood(
                    widthImage: _widthImage,
                    heightImage: _heightImage,
                    urlImage: _editRecipeController.getUrlImagesEdit,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFoodImage(double _widthScreen) {
    return Padding(
      padding: EditRecipePadding.editRecipePadding,
      child: SizedBox(
        height: 125 * (_widthScreen - 40 * 2) / 295,
        width: _widthScreen,
        child: Obx(
          () => CacheNetworkImageCustom(
            urlImage: _editRecipeController.getMyRecipeEdit.cover,
            boxFit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
