import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_padding.dart';
import 'package:mock_team3/app/core/app_text_theme.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_colors.dart';
import '../../view_models/edit_recipe_view_model.dart';
import 'section_title.dart';

class SecondSection extends StatelessWidget {
  SecondSection({
    Key? key,
  }) : super(key: key);
  final _editRecipecontroller = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _imageWidth = (_screenWidth - 80 - 32) / 5;
    List<String> ingredients =
        _editRecipecontroller.getMyRecipeEdit.ingredients;
    int ingredientsRemain = ingredients.length > 5 ? ingredients.length - 5 : 0;
    List<Widget> images = [];
    int amountImage = ingredientsRemain > 0 ? 3 : ingredients.length - 1;
    for (int i = 0; i <= amountImage; i++) {
      images.add(Padding(
        padding: EdgeInsets.only(right: i == 4 ? 0 : 8.0),
        child: _buildImageFood(_imageWidth),
      ));
    }
    EdgeInsets _ingredientsPadding = const EdgeInsets.only(
      top: 20,
    );
    return Padding(
      padding: _ingredientsPadding,
      child: SizedBox(
        child: Padding(
          padding: EditRecipePadding.cardPadding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SectionTitle(
                  text: LocaleKeys.editRecipe_secondSection.tr,
                  onpress: () {
                    _editRecipecontroller.forwarAnimation(
                        _editRecipecontroller.secondSectionAnimationController);
                  }),
              _buildIngredientImages(images, _imageWidth, ingredientsRemain > 0,
                  ingredientsRemain),
              _buildIngredientNames(),
            ],
          ),
        ),
      ),
    );
  }

  SizedBox _buildImageFood(double _imageWidth) {
    return SizedBox(
      height: _imageWidth,
      width: _imageWidth,
      child: Obx(
        () => ClipOval(
          child: CacheNetworkImageCustom(
              urlImage: _editRecipecontroller.getUrlImagesEdit[0],
              boxFit: BoxFit.fill),
        ),
      ),
    );
  }

  Padding _buildIngredientNames() {
    return Padding(
      padding: EditRecipePadding.editRecipePadding,
      child: Obx(
        () => Text(
          _editRecipecontroller.getIngredientsString,
          style: appTextTheme.caption?.copyWith(color: AppColors.colorBlack),
        ),
      ),
    );
  }

  Padding _buildIngredientImages(List<Widget> images, double _imageWidth,
      bool showImageEnd, int ingredientsRemain) {
    return Padding(
      padding: EditRecipePadding.editRecipePadding,
      child: Row(
        children: [
          ...images,
          showImageEnd
              ? SizedBox(
                  height: _imageWidth,
                  width: _imageWidth,
                  child: Stack(
                    children: [
                      _buildImageFood(_imageWidth),
                      Container(
                        decoration: BoxDecoration(
                            color: AppColors.colorWhite.withOpacity(0.6),
                            shape: BoxShape.circle),
                        child: Center(
                          child: Text(
                            "$ingredientsRemain+",
                            style: appTextTheme.headline5,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }
}
