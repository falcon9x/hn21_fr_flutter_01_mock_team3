import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_strings.dart';
import 'package:mock_team3/app/core/app_text_theme.dart';
import 'package:mock_team3/data/info/model/info_model.dart';
import 'package:mock_team3/generated/locales.g.dart';

class ContentAdditonals {
  static List<Widget> generateContent(
      double _boxWidth, InfoModel infor, String tags) {
    List<Widget> _contents = [];
    List<String> _dialogs = [
      LocaleKeys.editRecipe_firstDialogAdd.tr,
      LocaleKeys.editRecipe_secondDialogAdd.tr,
      LocaleKeys.editRecipe_thirdDialogAdd.tr
    ];
    List<List<String>> addInfor = [];
    addInfor.add([infor.time.toString() + " Mins"]);
    addInfor.add(infor.facts ?? []);
    addInfor.add([tags]);

    for (int i = 0; i < EditRecipeString.contentAdditionals.length; i++) {
      _contents.add(Padding(
        padding: EdgeInsets.only(bottom: (i + 1) == addInfor.length ? 0 : 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 95,
              child: Text(
                _dialogs[i],
                style: appTextTheme.subtitle2,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Column(
                children: [
                  for (int j = 0; j < addInfor[i].length; j++) ...[
                    SizedBox(
                      width: _boxWidth - 120,
                      child: Text(
                        addInfor[i][j],
                        style: appTextTheme.bodyText2
                            ?.copyWith(overflow: TextOverflow.clip),
                      ),
                    )
                  ]
                ],
              ),
            ),
          ],
        ),
      ));
    }
    return _contents;
  }
}
