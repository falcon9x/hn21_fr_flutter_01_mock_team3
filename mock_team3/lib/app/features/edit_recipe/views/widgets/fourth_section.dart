import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_padding.dart';
import '../../view_models/edit_recipe_view_model.dart';
import 'content_additionals.dart';
import 'section_title.dart';

class FourthSection extends StatelessWidget {
  FourthSection({
    Key? key,
    required this.showTitle,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  final bool showTitle;
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _boxWidth = _screenWidth - 80;
    return Padding(
      padding: EditRecipePadding.editRecipePadding,
      child: SizedBox(
        child: Padding(
          padding: EditRecipePadding.cardPadding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              showTitle
                  ? Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: SectionTitle(
                          text: LocaleKeys.editRecipe_fourthSection.tr,
                          onpress: () {
                            _editRecipeController.forwarAnimation(
                                _editRecipeController
                                    .fourthSectionAnimationController);
                          }),
                    )
                  : const SizedBox(),
              Obx(
                () => Column(
                    children: ContentAdditonals.generateContent(
                  _boxWidth,
                  _editRecipeController.getInfor,
                  _editRecipeController.getMyRecipeEdit.tagName
                      .join(", ")
                      .trim(),
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
