import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/core/app_padding.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_text_theme.dart';
import '../../view_models/edit_recipe_view_model.dart';
import 'cache_network_image_custom.dart';

class FirstSectionImages extends StatelessWidget {
  FirstSectionImages({
    Key? key,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTitle(),
          _buildImages(),
        ],
      ),
    );
  }

  Padding _buildImages() {
    double heightImage = 98;
    double widhtImage = 140;
    return Padding(
      padding: const EdgeInsets.only(top: 15, left: 25),
      child: SizedBox(
        height: heightImage,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Obx(
            () => Row(children: [
              for (int index = 0;
                  index < _editRecipeController.getUrlImagesEdit.length;
                  index++)
                InkWell(
                  onTap: () {
                    _editRecipeController.changeSelected(index);
                  },
                  child: Container(
                    decoration: _editRecipeController.getSelected == index
                        ? BoxDecoration(
                            border: Border.all(
                                color: AppColors.mainColor, width: 2),
                            borderRadius: BorderRadius.circular(4),
                          )
                        : null,
                    width: widhtImage,
                    child: Padding(
                      padding: const EdgeInsets.all(6),
                      child: CacheNetworkImageCustom(
                          urlImage:
                              _editRecipeController.getUrlImagesEdit[index],
                          boxFit: BoxFit.fill),
                    ),
                  ),
                )
            ]),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Padding(
      padding: AppPadding.defaultPadding,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Obx(
            () => Text(
              LocaleKeys.editGallery_images.tr +
                  " (${_editRecipeController.getUrlImagesEdit.length})",
              style: appTextTheme.headline4,
            ),
          ),
          TextButton(
            onPressed: () {},
            child: Text(
              LocaleKeys.editGallery_textButton.tr,
              style: appTextTheme.button,
            ),
          )
        ],
      ),
    );
  }
}
