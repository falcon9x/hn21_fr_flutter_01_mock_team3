import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_assets.dart';
import 'package:mock_team3/app/core/app_padding.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/edit_section_title.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../widgets/app_rounded_button.dart';
import '../../../../widgets/app_text_form_field.dart';
import '../../../../widgets/container_border_dash.dart';
import '../../view_models/edit_recipe_view_model.dart';

class EditSecondSection extends StatelessWidget {
  EditSecondSection({Key? key, required this.showEdit}) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  final bool showEdit;
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _scrennHeight = Get.height;
    return Container(
      decoration:
          BoxDecoration(color: const Color(0xFF282928).withOpacity(0.5)),
      child: Column(
        children: [
          SizedBox(
            height: _scrennHeight * 5 / 16,
            width: _screenWidth,
          ),
          SizedBox(
            height: _scrennHeight * 11 / 16,
            width: _screenWidth,
            child: Container(
              width: _screenWidth,
              decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      children: [
                        EditSectionTitle(
                          onpress: () => _editRecipeController.reverseAnimation(
                              _editRecipeController
                                  .secondSectionAnimationController),
                          text: LocaleKeys.editIngredients_title.tr,
                        ),
                        Obx(
                          () => Column(children: [
                            for (int indexIngredients = 0;
                                indexIngredients <
                                    _editRecipeController
                                        .getMyRecipeEdit.ingredients.length;
                                indexIngredients++)
                              _buildIngredientContent(
                                _screenWidth,
                                indexIngredients,
                                _editRecipeController
                                    .getShowEditIngredients[indexIngredients],
                              ),
                          ]),
                        ),
                        _buildAddIngredient(_screenWidth),
                        _buildSaveIngredients(_screenWidth)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding _buildSaveIngredients(double _screenWidth) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 20),
      child: AppRoundedButton(
        onPress: () => _editRecipeController.saveIngredient(),
        width: _screenWidth,
        isCircle: false,
        text: LocaleKeys.editIngredients_contentButton.tr,
      ),
    );
  }

  Padding _buildAddIngredient(double _screenWidth) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 60),
      child: InkWell(
        onTap: () {
          _editRecipeController.showAddIngredient();
        },
        child: ContainerBorderDash(
          boxHeight: 50,
          boxWidth: _screenWidth - 50,
          color: AppColors.colorGrey,
          thickness: 1,
          circularValue: 8,
          child: Center(
              child: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Row(
              children: [
                SvgPicture.asset(IconBlack.iconAdd),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: SizedBox(
                    width: _screenWidth * 0.65,
                    child: Text(
                      LocaleKeys.editIngredients_labelText.tr,
                      style: appTextTheme.bodyText1?.copyWith(
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )),
        ),
      ),
    );
  }

  Padding _buildIngredientContent(
    double _screenWidth,
    int index,
    bool showTextFiled,
  ) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 30),
      child: AppTextFormField(
        showTextField: showTextFiled,
        initialValue: _editRecipeController.getMyRecipeEdit.ingredients[index],
        onchange: (value) {
          _editRecipeController.changeIngredient(index, value);
        },
        focusedBorderColor: AppColors.mainColor,
        isPassword: false,
        child: Row(
          children: [
            SizedBox(
              width: _screenWidth * 0.55,
              child: Text(
                showTextFiled
                    ? "Write Ingredient"
                    : "${index + 1} " +
                        _editRecipeController
                            .getMyRecipeEdit.ingredients[index],
                style: appTextTheme.subtitle2?.copyWith(
                    overflow: TextOverflow.clip,
                    color: showTextFiled
                        ? AppColors.textGrey
                        : AppColors.colorBlack),
              ),
            ),
            const Spacer(),
            showTextFiled
                ? InkWell(
                    onTap: () {
                      _editRecipeController.deleteIngredeint(index);
                    },
                    child: SvgPicture.asset(IconRed.iconRedDelete))
                : const SizedBox(),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: InkWell(
                  onTap: () {
                    _editRecipeController.changeShowEditIngredients(index);
                  },
                  child: SizedBox(
                      height: 24,
                      width: 24,
                      child: SvgPicture.asset(showTextFiled
                          ? IconGreen.iconEdit
                          : IconBlack.iconEdit))),
            ),
          ],
        ),
      ),
    );
  }
}
