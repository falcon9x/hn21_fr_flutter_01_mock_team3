import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../widgets/app_rounded_button.dart';
import '../../view_models/edit_recipe_view_model.dart';

class Footer extends StatelessWidget {
  Footer({
    Key? key,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: AppRoundedButton(
              onPress: () => _editRecipeController.saveEditRecipe(),
              width: _screenWidth,
              text: LocaleKeys.editRecipe_contentButton.tr,
              isCircle: false),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(IconBlack.iconDelete),
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: TextButton(
                  child: Text(
                    LocaleKeys.editRecipe_titleFooter.tr,
                    style: appTextTheme.button,
                  ),
                  onPressed: () {},
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
