import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_text_theme.dart';
import '../../view_models/edit_recipe_view_model.dart';

class FirstSectionRemove extends StatelessWidget {
  FirstSectionRemove({
    Key? key,
  }) : super(key: key);
  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              _editRecipeController.deleteGallery();
            },
            child: SvgPicture.asset(IconBlack.iconDelete),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 11),
            child: Text(
              LocaleKeys.editGallery_remove.tr,
              style: appTextTheme.bodyText1,
            ),
          )
        ],
      ),
    );
  }
}
