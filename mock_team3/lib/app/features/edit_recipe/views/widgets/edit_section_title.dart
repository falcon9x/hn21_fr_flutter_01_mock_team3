import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_text_theme.dart';

class EditSectionTitle extends StatelessWidget {
  const EditSectionTitle({
    Key? key,
    required this.onpress,
    required this.text,
  }) : super(key: key);
  final VoidCallback onpress;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            text,
            style: appTextTheme.headline4,
          ),
          InkWell(
            onTap: onpress,
            child: SvgPicture.asset(IconBlack.iconClose),
          ),
        ],
      ),
    );
  }
}
