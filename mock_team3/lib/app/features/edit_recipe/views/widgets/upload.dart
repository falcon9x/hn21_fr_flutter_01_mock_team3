import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../widgets/container_border_dash.dart';

class UpLoad extends StatelessWidget {
  const UpLoad({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: InkWell(
        onTap: () {},
        child: ContainerBorderDash(
          boxHeight: 50,
          boxWidth: _screenWidth - 50,
          color: AppColors.colorGrey,
          thickness: 1,
          circularValue: 8,
          child: Center(
              child: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Row(
              children: [
                SvgPicture.asset(IconBlack.iconAdd),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: SizedBox(
                    width: _screenWidth * 0.65,
                    child: Text(
                      LocaleKeys.editGallery_labelText.tr,
                      style: appTextTheme.bodyText1?.copyWith(
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )),
        ),
      ),
    );
  }
}
