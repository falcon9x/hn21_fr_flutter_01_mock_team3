import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/widgets/app_text_form_field.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_padding.dart';
import '../../../../widgets/app_rounded_button.dart';
import '../../view_models/edit_recipe_view_model.dart';
import 'edit_section_title.dart';

class EditFourthSection extends StatelessWidget {
  EditFourthSection({Key? key}) : super(key: key);

  final _editRecipeController = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    double _screenWidth = Get.width;
    double _scrennHeight = Get.height;
    return Container(
      decoration:
          BoxDecoration(color: const Color(0xFF282928).withOpacity(0.5)),
      child: Column(
        children: [
          SizedBox(
            height: _scrennHeight * 0.4,
            width: _screenWidth,
          ),
          SizedBox(
            height: _scrennHeight * 0.6,
            width: _screenWidth,
            child: Container(
              width: _screenWidth,
              decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      children: [
                        EditSectionTitle(
                          onpress: () => _editRecipeController.reverseAnimation(
                              _editRecipeController
                                  .fourthSectionAnimationController),
                          text: LocaleKeys.editRecipe_fourthSection.tr,
                        ),
                        Obx(
                          () => _buildEditTime(),
                        ),
                        Obx(
                          () => _buildEditFacts(),
                        ),
                        Obx(
                          () => _buildTags(),
                        ),
                        _buildButton(_screenWidth)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding _buildButton(double _screenWidth) {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 30),
      child: AppRoundedButton(
        onPress: () => _editRecipeController.saveAdditionalInfor(),
        width: _screenWidth,
        isCircle: false,
        text: LocaleKeys.additionalInfo_contentButton.tr,
      ),
    );
  }

  Padding _buildTags() {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 20),
      child: AppTextFormField(
          onchange: (value) {
            _editRecipeController.changeTags(value);
          },
          initialValue:
              _editRecipeController.getMyRecipeEdit.tagName.join(", "),
          focusedBorderColor: AppColors.mainColor,
          isPassword: false,
          labelText: LocaleKeys.additionalInfo_thirdLabelTextFiled.tr),
    );
  }

  Padding _buildEditFacts() {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 20),
      child: AppTextFormField(
          initialValue: _editRecipeController.getInfor.facts?.join("\n"),
          onchange: (value) {
            _editRecipeController.changeFacts(value);
          },
          focusedBorderColor: AppColors.mainColor,
          keyboardType: TextInputType.multiline,
          isPassword: false,
          labelText: LocaleKeys.additionalInfo_secondLabelTextFiled.tr),
    );
  }

  Padding _buildEditTime() {
    return Padding(
      padding: AppPadding.defaultPadding.copyWith(top: 30),
      child: AppTextFormField(
          keyboardType: TextInputType.number,
          initialValue: _editRecipeController.getInfor.time.toString(),
          onchange: (value) {
            _editRecipeController.changeTime(value);
          },
          focusedBorderColor: AppColors.mainColor,
          isPassword: false,
          labelText: LocaleKeys.additionalInfo_firstLabelTextFiled.tr),
    );
  }
}
