import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../generated/locales.g.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../view_models/edit_recipe_view_model.dart';

class FirstSectionSetAsCover extends StatelessWidget {
  FirstSectionSetAsCover({
    Key? key,
  }) : super(key: key);
  final _controller = Get.find<EditRecipeViewModel>();
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        child: InkWell(
          onTap: () {
            _controller.setCover();
          },
          child: Row(
            children: [
              Container(
                height: 16,
                width: 16,
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.mainColor),
                  shape: BoxShape.circle,
                ),
                child: _controller.getIsSetCover
                    ? Padding(
                        padding: const EdgeInsets.all(4),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: AppColors.mainColor),
                            shape: BoxShape.circle,
                            color: AppColors.mainColor,
                          ),
                        ),
                      )
                    : null,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  LocaleKeys.editGallery_setAsCover.tr,
                  style: appTextTheme.bodyText1,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
