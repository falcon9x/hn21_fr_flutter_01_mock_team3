import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_padding.dart';
import 'package:mock_team3/app/features/edit_recipe/view_models/edit_recipe_view_model.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/edit_fourth_section.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/edit_third_section.dart';

import '../../../../generated/locales.g.dart';
import '../../../core/app_assets.dart';
import '../../../core/app_text_theme.dart';
import 'widgets/edit_first_section.dart';
import 'widgets/edit_second_section.dart';
import 'widgets/first_sesction.dart';
import 'widgets/five_section.dart';
import 'widgets/footer.dart';
import 'widgets/fourth_section.dart';
import 'widgets/third_section.dart';
import 'widgets/second_section.dart';
import 'widgets/recipe_name.dart';

class EditRecipScreen extends GetView<EditRecipeViewModel> {
  const EditRecipScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: Listenable.merge([
        controller.firstSectionAnimationController,
        controller.secondSectionAnimationController,
        controller.thirdSectionAnimationController,
        controller.fourthSectionAnimationController,
      ]),
      builder: (context, child) => Stack(
        children: [
          Scaffold(
            appBar: _buildAppBar(),
            body: SingleChildScrollView(
              child: Padding(
                padding: AppPadding.defaultPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _buildTitle(),
                    RecipeName(),
                    FirstSection(),
                    SecondSection(),
                    ThirdSection(showTitle: true),
                    FourthSection(showTitle: true),
                    const FiveSection(),
                    Footer(),
                  ],
                ),
              ),
            ),
          ),
          Obx(
            () => _buildShowEdit(
                controller.animationFirstSection.value, EditFirstSection()),
          ),
          Obx(
            () => _buildShowEdit(
                controller.animationSecondSection.value,
                EditSecondSection(
                  showEdit: false,
                )),
          ),
          Obx(
            () => _buildShowEdit(
                controller.animationThirdSection.value, EditThirdSection()),
          ),
          Obx(
            () => _buildShowEdit(
                controller.animationFourthSection.value, EditFourthSection()),
          ),
        ],
      ),
    );
  }

  Widget _buildShowEdit(Animation animation, Widget child) {
    return (1 - animation.value) == 1
        ? const SizedBox()
        : Opacity(
            opacity: animation.value,
            child: child,
          );
  }

  Text _buildTitle() {
    return Text(
      LocaleKeys.editRecipe_title.tr,
      style: appTextTheme.headline3,
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      leadingWidth: 0,
      leading: const Text(""),
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: InkWell(
        onTap: () {
          _backToRecipe();
        },
        child: Row(
          children: [
            SvgPicture.asset(IconBlack.iconLeft),
            Text(
              LocaleKeys.editRecipe_textBackButton.tr,
              style: appTextTheme.caption,
            )
          ],
        ),
      ),
    );
  }

  void _backToRecipe() {
    Get.back();
  }
}
