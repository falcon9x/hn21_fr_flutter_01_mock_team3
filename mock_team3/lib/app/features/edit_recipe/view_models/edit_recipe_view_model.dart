import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mock_team3/app/base/base_viewmodel.dart';
import 'package:mock_team3/data/direction_detail/model/direction_detail_model.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_collection_name.dart';
import 'package:mock_team3/data/info/model/info_model.dart';
import 'package:mock_team3/data/my_recipe/my_recipe_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:uuid/uuid.dart';

import '../../../../data/base/data_result.dart';
import '../../../../data/direction/model/direction_model.dart';
import '../../../../data/firebase/firebase_repository.dart';
import '../../../routes/app_routes.dart';

class EditRecipeViewModel extends BaseViewModel
    with GetTickerProviderStateMixin {
  final FirebaseRepository _firebaseRepository;
  EditRecipeViewModel(this._firebaseRepository);

  final _myRecipeEdit = MyRecipeModel.withInitial().obs;
  MyRecipeModel get getMyRecipeEdit => _myRecipeEdit.value;
  set setMyRecipeEdit(value) => _myRecipeEdit.value = value;
  late MyRecipeModel _myRecipeEditReview;

  final _isSetCover = true.obs;
  bool get getIsSetCover => _isSetCover.value;

  final defaultDuration = const Duration(milliseconds: 300);

  @override
  void onInit() {
    super.onInit();
    setUpFirstAnimation();
    setUpSecondAnimation();
    setUpThirdAnimation();
    setUpFourthAnimation();
    if (Get.arguments is MyRecipeModel?) {
      _myRecipeEdit.value = MyRecipeModel.fromMyCipeModel(Get.arguments);
      _myRecipeEditReview = MyRecipeModel.fromMyCipeModel(getMyRecipeEdit);
      getMyRecipeEdit.cover != ""
          ? _checkLoadCover.value = true
          : _checkLoadCover.value = false;
      _initalImageUrls();
      _getUrlImageEdit();
      _getAdditionInfor();
      _getDirection();
      _convertIngredient();
      _showEditIngredientsIntial();
    }
  }

  void forwarAnimation(AnimationController animationController) {
    animationController.forward();
  }

  void reverseAnimation(AnimationController animationController) {
    animationController.reverse();
    if (!_isSaveGallery && firstSectionAnimationController.value != 0) {
      _refreshGallery();
    }
    if (!_isSaveIngredient && secondSectionAnimationController.value != 0) {
      _refreshIngredients();
    }
    if (!_isSaveDirection && thirdSectionAnimationController.value != 0) {
      _refreshDirection();
    }
    if (!_isSaveAddInfor && fourthSectionAnimationController.value != 0) {
      _refreshAdditionalInfor();
    }
    _isSaveGallery = false;
    _isSaveIngredient = false;
    _isSaveDirection = false;
    _isSaveAddInfor = false;
  }

  final _urlImagesEdit = <String>[].obs;
  List<String> get getUrlImagesEdit => _urlImagesEdit.call();
  List<String> _urlImageEditReview = [];
  void _initalImageUrls() {
    for (int index = 0; index < getMyRecipeEdit.galleries.length; index++) {
      _urlImagesEdit.add("");
    }
  }

  Future<void> _getUrlImageEdit() async {
    if (getUrlImagesEdit.isNotEmpty) {
      int lengGalleries = getUrlImagesEdit.length;
      for (int indextGalleries = 0;
          indextGalleries < lengGalleries;
          indextGalleries++) {
        DataResult dataResult = await _firebaseRepository
            .getImageFromStorage(getMyRecipeEdit.galleries[indextGalleries]);
        if (dataResult.isSuccess) {
          _urlImagesEdit[indextGalleries] = dataResult.data;
        }
      }
      _urlImageEditReview = List.from(getUrlImagesEdit);
    }
  }

  //Cover Content

  final _checkLoadCover = false.obs;
  get getCheckLoadCover => _checkLoadCover.value;
  void changeNameRecipe(String value) {
    _myRecipeEdit.value.recipeName = value;
  }

  //saved Gallery
  late AnimationController firstSectionAnimationController;
  Rxn animationFirstSection = Rxn(Animation);
  bool _isSaveGallery = false;
  void setUpFirstAnimation() {
    firstSectionAnimationController =
        AnimationController(vsync: this, duration: defaultDuration);
    animationFirstSection.value = CurvedAnimation(
      parent: firstSectionAnimationController,
      curve: const Interval(0.0, 1),
    );
  }

  void changeSelected(int index) {
    _secleted.value = index;
    update();
  }

  void deleteGallery() {
    _urlImagesEdit.removeAt(getSelected);
    _myRecipeEdit.value.cover = getUrlImagesEdit[0];
    _myRecipeEdit.value.galleries.removeAt(getSelected);
  }

  void _refreshGallery() {
    _urlImagesEdit.value = List.from(_urlImageEditReview);
    _myRecipeEdit.value.galleries = List.from(_myRecipeEditReview.galleries);
    _myRecipeEdit.value.cover = _myRecipeEditReview.cover;
  }

  void saveGallery() {
    _isSaveGallery = true;
    _myRecipeEdit.value.cover = getUrlImagesEdit[getSelected];
    _urlImagesEdit.insert(0, _urlImagesEdit.removeAt(getSelected));
    _myRecipeEdit.value.galleries
        .insert(0, _myRecipeEdit.value.galleries.removeAt(getSelected));
    _myRecipeEditReview.galleries = List.from(getMyRecipeEdit.galleries);
    _urlImageEditReview = List.from(getUrlImagesEdit);
    reverseAnimation(firstSectionAnimationController);
  }

  void setCover() {
    _isSetCover.value = !getIsSetCover;
  }

  //finish Gallery
  //saved Ingredient
  late AnimationController secondSectionAnimationController;
  Rxn animationSecondSection = Rxn(Animation);
  void setUpSecondAnimation() {
    secondSectionAnimationController =
        AnimationController(vsync: this, duration: defaultDuration);
    animationSecondSection.value = CurvedAnimation(
      parent: secondSectionAnimationController,
      curve: const Interval(0.0, 1),
    );
  }

  bool _isSaveIngredient = false;
  final _ingredientsString = "".obs;
  get getIngredientsString => _ingredientsString.value;
  final _showEditIngredients = <bool>[].obs;
  List<bool> get getShowEditIngredients => _showEditIngredients.call();
  List<bool> _showShowEditIngredientsRevew = [];
  void _convertIngredient() {
    int lengthIngredient = getMyRecipeEdit.ingredients.length;
    int ingredientsRemain = lengthIngredient > 5 ? lengthIngredient - 5 : 0;
    _ingredientsString.value = ingredientsRemain > 0
        ? getMyRecipeEdit.ingredients.sublist(0, 5).join(", ") +
            " +$ingredientsRemain" +
            "more"
        : getMyRecipeEdit.ingredients.join(", ");
  }

  void _showEditIngredientsIntial() {
    int lengthShowEditIngredient = getMyRecipeEdit.ingredients.length;
    for (int index = 0; index < lengthShowEditIngredient; index++) {
      _showEditIngredients.add(false);
    }
    _showShowEditIngredientsRevew = List<bool>.from(getShowEditIngredients);
  }

  void changeIngredient(int index, String value) {
    _myRecipeEdit.value.ingredients[index] = value;
  }

  void changeShowEditIngredients(int index) {
    _showEditIngredients[index] = !_showEditIngredients[index];
  }

  void showAddIngredient() {
    _myRecipeEdit.value.ingredients.add("");
    _showEditIngredients.add(true);
  }

  void deleteIngredeint(int index) {
    _showEditIngredients.removeAt(index);
    _myRecipeEdit.value.ingredients.removeAt(index);
  }

  void _refreshIngredients() {
    _showEditIngredients.value = List.from(_showShowEditIngredientsRevew);
    _myRecipeEdit.value.ingredients =
        List<String>.from(_myRecipeEditReview.ingredients);
  }

  void saveIngredient() {
    _convertIngredient();
    _isSaveIngredient = true;
    _showEditDirectionsRevew = List.from(getShowEditIngredients);
    _myRecipeEditReview.ingredients = List.from(getMyRecipeEdit.ingredients);
    reverseAnimation(secondSectionAnimationController);
  }

  //Fisnish Ingredient
  //saved Direction
  late AnimationController thirdSectionAnimationController;
  Rxn animationThirdSection = Rxn(Animation);

  bool _isSaveDirection = false;
  final _direction = Direction.withInitial().obs;
  Direction get getDirection => _direction.value;
  final _directionDetails = <DirectionDetailModel>[].obs;
  List<DirectionDetailModel> get getDirectionDetails =>
      _directionDetails.call();
  final _showEditDirections = <bool>[].obs;
  List<bool> get getShowEditDiretions => _showEditDirections.call();
  List<DirectionDetailModel> _directionDetailsReview = [];
  List<bool> _showEditDirectionsRevew = [];
  void setUpThirdAnimation() {
    thirdSectionAnimationController =
        AnimationController(vsync: this, duration: defaultDuration);
    animationThirdSection.value = CurvedAnimation(
      parent: thirdSectionAnimationController,
      curve: const Interval(0.0, 1),
    );
  }

  Future<void> _getDirection() async {
    _direction.value =
        await _firebaseRepository.getDirection(getMyRecipeEdit.directionId);
    if (getDirection.directionDetails.isNotEmpty) {
      for (var directionDetailId in getDirection.directionDetails) {
        DirectionDetailModel detailModel =
            await _firebaseRepository.getDirectionDetail(directionDetailId);
        _directionDetails.add(detailModel);
        _directionDetailsReview.add(
            DirectionDetailModel.fromDirectionDetailModelOther(detailModel));
        _showEditDirections.add(false);
      }
      _showEditDirectionsRevew = List.from(getShowEditDiretions);
    }
  }

  void changeDirection(int index, String value) {
    _directionDetails[index].name = value;
  }

  void changeShowEditDirection(int index) {
    _showEditDirections[index] = !getShowEditDiretions[index];
  }

  void deleteEditDirection(int index) {
    _directionDetails.removeAt(index);
    _showEditDirections.removeAt(index);
    _direction.value.directionDetails.removeAt(index);
  }

  void addDirection() {
    _directionDetails.add(DirectionDetailModel.withInitial());
    _showEditDirections.add(true);
    _direction.value.directionDetails.add(const Uuid().v4());
  }

  void _refreshDirection() {
    _showEditDirections.value = List.from(_showEditDirectionsRevew);
    _directionDetails.value = [];
    for (var direciton in _directionDetailsReview) {
      _directionDetails
          .add(DirectionDetailModel.fromDirectionDetailModelOther(direciton));
    }
  }

  void saveDirection() {
    _isSaveDirection = true;
    _showEditDirectionsRevew = List.from(getShowEditDiretions);
    _directionDetailsReview = [];
    for (var direciton in getDirectionDetails) {
      _directionDetailsReview
          .add(DirectionDetailModel.fromDirectionDetailModelOther(direciton));
    }
    reverseAnimation(thirdSectionAnimationController);
  }

  //Finish Direction
  //Save Addition
  late AnimationController fourthSectionAnimationController;
  Rxn animationFourthSection = Rxn(Animation);
  bool _isSaveAddInfor = false;

  final _infor = InfoModel.withInitial().obs;
  InfoModel get getInfor => _infor.value;
  InfoModel _inforBase = InfoModel.withInitial();
  Future<void> _getAdditionInfor() async {
    _infor.value = await _firebaseRepository.getInfor(getMyRecipeEdit.addInfor);
    _inforBase = InfoModel.fromInforOther(getInfor);
  }

  void setUpFourthAnimation() {
    fourthSectionAnimationController =
        AnimationController(vsync: this, duration: defaultDuration);
    animationFourthSection.value = CurvedAnimation(
      parent: fourthSectionAnimationController,
      curve: const Interval(0.0, 1),
    );
  }

  void changeTime(String value) {
    if (value.isNotEmpty) {
      _infor.value.time = int.parse(value);
    } else {
      _infor.value.time = 0;
    }
  }

  void changeFacts(String value) {
    _infor.value.facts = List.from(value.split("/t"));
  }

  void changeTags(String value) {
    _myRecipeEdit.value.tagName = List.from(value.split(", "));
  }

  void _refreshAdditionalInfor() {
    _myRecipeEdit.value.tagName = List.from(_myRecipeEditReview.tagName);
    _infor.value = InfoModel.fromInforOther(_inforBase);
  }

  void saveAdditionalInfor() {
    _isSaveAddInfor = true;
    _inforBase = InfoModel.fromInforOther(getInfor);
    _myRecipeEditReview.tagName = List.from(getMyRecipeEdit.tagName);
    reverseAnimation(fourthSectionAnimationController);
  }

  //Finishe Addition
  // viewRecipe
  final _indexTabBar = 0.obs;
  int get getIndexTabBar => _indexTabBar.value;

  final _secleted = 0.obs;
  int get getSelected => _secleted.value;

  void backToProfile() {
    Get.offNamed(AppRoute.recipeFeed);
  }

  void goToCookMode() {
    Get.toNamed(AppRoute.cookingModeScreen,
        arguments: getMyRecipeEdit.recipeId);
  }

  void updateIndexTabBar(int indexTabBarNew) {
    _indexTabBar.value = indexTabBarNew;
  }

  //Save Edit Recipe
  bool _isSave = false;
  Future<void> saveEditRecipe() async {
    _isSave = true;
    try {
      if (_isSave) {
        RecipeModel recipeUpdate =
            await _firebaseRepository.getRecipe(getMyRecipeEdit.recipeId);
        recipeUpdate.galleries = List.from(getMyRecipeEdit.galleries);
        recipeUpdate.name = getMyRecipeEdit.recipeName;
        recipeUpdate.ingredients = List.from(getMyRecipeEdit.ingredients);
        recipeUpdate.ingredientCount = getMyRecipeEdit.ingredients.length;
        await _firebaseRepository.updateData(
            "${FirebaseCollectionName.recipe}/${getMyRecipeEdit.recipeId}",
            recipeUpdate.toJson());
        _isSave = false;
        Get.back();
      }
    } catch (e) {
      log(e.toString());
    }
  }
}

// upload Image
final imagePath = "".obs;
Future<void> getImageFromLocal(ImageSource source) async {
  try {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: source);
    if (image != null) {
      imagePath.value = image.path;
    }
  } catch (ex) {
    log(ex.toString());
  }
}
