import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/recipe_feed/recipe_feed_view_model/recipe_feed_view_model.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view_model/search_suggestion_view_model.dart';
import 'package:mock_team3/app/routes/app_routes.dart';

import '../../../../../data/search_suggestion/model/search_suggestion_profile.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';

class ItemProfile extends StatelessWidget {
  ItemProfile(
      {Key? key, required this.searchSuggestionProfile, required this.index})
      : super(key: key);
  final controller = Get.find<SearchSuggestionViewModel>();
  final _recipeFeedController = Get.find<RecipeFeedViewModel>();
  final SearchSuggestionProfile searchSuggestionProfile;
  final int index;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: Get.width * (15 / 375)),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                _buildBackGround(),
                _buildBackGroundOpacity(),
                _buildDetailProfile(context)
              ],
            )
          ],
        ),
      ),
    );
  }

  SizedBox _buildDetailProfile(BuildContext context) {
    return SizedBox(
      width: Get.width * (172 / 375),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildSpaceTopToImage(),
          _buildImageAvatar(),
          _buildUserName(context),
          Padding(
            padding: EdgeInsets.only(
                top: Get.height * (11 / 810),
                left: Get.width * (30 / 375),
                right: Get.width * (30 / 375)),
            child: Row(
              children: [
                Column(
                  children: [
                    _buildNumberRecipe(context),
                    _buildTextRecipe(context),
                  ],
                ),
                const Spacer(),
                Column(
                  children: [
                    buildNumberFollower(context),
                    _buildTextFollowerProfile(context)
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  SizedBox _buildTextFollowerProfile(BuildContext context) {
    return SizedBox(
      height: Get.height * (22 / 810),
      child: Text(
        StringSearch.txtFollowers,
        style: appTextTheme.headline5?.copyWith(
            color: AppColors.colorBlack.withOpacity(0.6), fontSize: 10),
      ),
    );
  }

  SizedBox buildNumberFollower(BuildContext context) =>
      _buildTextFollower(context);

  SizedBox _buildTextFollower(BuildContext context) =>
      _buildNumberOfFollower(context);

  SizedBox _buildNumberOfFollower(BuildContext context) {
    return SizedBox(
      height: Get.height * (22 / 810),
      child: Text(
        searchSuggestionProfile.numberFollowers.toString(),
        style: appTextTheme.bodyText2,
      ),
    );
  }

  SizedBox _buildTextRecipe(BuildContext context) {
    return SizedBox(
      height: Get.height * (22 / 810),
      child: Text(
        StringSearch.txtRecipe,
        style: appTextTheme.headline5?.copyWith(
            color: AppColors.colorBlack.withOpacity(0.6), fontSize: 10),
      ),
    );
  }

  SizedBox _buildNumberRecipe(BuildContext context) {
    return SizedBox(
      height: Get.height * (22 / 810),
      child: Text(
        searchSuggestionProfile.numberRecipe.toString(),
        style: appTextTheme.bodyText2,
      ),
    );
  }

  SizedBox _buildSpaceTopToImage() {
    return SizedBox(
      height: Get.height * (55 / 810),
    );
  }

  SizedBox _buildUserName(BuildContext context) {
    return SizedBox(
      child: Text(
        searchSuggestionProfile.nameUser,
        style: appTextTheme.bodyText1,
      ),
    );
  }

  Widget _buildImageAvatar() {
    return InkWell(
      onTap: () {
        _recipeFeedController.appBarSelected.value = 3;
        _recipeFeedController.iconSelected.value = 2;
        Get.toNamed(AppRoute.recipeFeed,
            arguments: _recipeFeedController.userId[index]);
      },
      child: SizedBox(
        width: Get.width * (90 / 375),
        height: Get.width * (90 / 375),
        child: CircleAvatar(
          radius: 15,
          child: Obx(() => Container(
                width: Get.width * (90 / 375),
                height: Get.width * (90 / 375),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: NetworkImage(controller.listAvatar[index]),
                        fit: BoxFit.cover)),
              )),
        ),
      ),
    );
  }

  Container _buildBackGroundOpacity() {
    return Container(
      width: Get.width * (172 / 375),
      height: Get.height * (110 / 810),
      color: Colors.white.withOpacity(0.5),
    );
  }

  SizedBox _buildBackGround() {
    return SizedBox(
      width: Get.width * (172 / 375),
      height: Get.height * (110 / 810),
      child: Image.asset("assets/images/img_search_suggestion.png",
          fit: BoxFit.fill),
    );
  }
}
