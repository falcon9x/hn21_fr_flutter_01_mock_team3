import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../../data/tag/model/tag_model.dart';
import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';

class ItemTag extends StatelessWidget {
  const ItemTag({
    Key? key,
    required this.tagModel
  }) : super(key: key);
  final TagModel tagModel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: Get.height * (20 / 810)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "#${tagModel.name}",
            style: appTextTheme.bodyText2,
          ),
          Row(
            children: [
              Text(
                "${tagModel.follower} followers",
                style: appTextTheme.bodyText2
                    ?.copyWith(color: AppColors.colorGrey),
              ),
              SvgPicture.asset(IconRed.iconDot),
              Text(
                "${tagModel.recipe} Recipe",
                style: appTextTheme.bodyText2
                    ?.copyWith(color: AppColors.colorGrey),
              )
            ],
          )
        ],
      ),
    );
  }
}
