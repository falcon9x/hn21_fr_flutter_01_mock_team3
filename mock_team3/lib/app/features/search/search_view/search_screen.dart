import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/core/app_colors.dart';
import 'package:mock_team3/app/core/app_strings.dart';
import 'package:mock_team3/app/features/search/search_view/widget/item_profile.dart';
import 'package:mock_team3/app/features/search/search_view/widget/item_tag.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/app_bar_search.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/error_widget_custom.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view/widget/item_recipe.dart';
import 'package:mock_team3/app/features/search_suggestion/search_suggestion_view_model/search_suggestion_view_model.dart';
import 'package:mock_team3/data/search_suggestion/model/search_suggestion_profile.dart';
import 'package:mock_team3/data/tag/model/tag_model.dart';

import '../../../../data/search_suggestion/model/search_suggestion_model.dart';
import '../../../core/app_text_theme.dart';

class SearchScreen extends GetWidget<SearchSuggestionViewModel> {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _heightScreen = Get.height;
    double _widthScreen = Get.width;
    double _paddingHorizontal = _widthScreen * (25 / 375);

    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            _buildAppBar(_heightScreen, _widthScreen),
            _buildTitleRecipe(_heightScreen, _widthScreen, context),
            _buildListRecipe(_heightScreen, _paddingHorizontal),
            _buildDivider(_paddingHorizontal, _heightScreen),
            _buildTitleProfile(_heightScreen, _paddingHorizontal, context),
            _buildListViewProfile(_heightScreen, _paddingHorizontal),
            _buildDividerProfile(_heightScreen, _widthScreen),
            _buildTag(_heightScreen, _paddingHorizontal, context),
            _buildListTag(_paddingHorizontal, _heightScreen)
          ],
        ),
      ),
    );
  }

  Padding _buildTag(
      double _heightScreen, double _paddingHorizontal, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: _heightScreen * (25 / 810),
          left: _paddingHorizontal,
          right: _paddingHorizontal),
      child: _buildTitleTag(context),
    );
  }

  Row _buildTitleTag(BuildContext context) {
    return Row(
      children: [
        Text(
          StringSearch.txtTags,
          style: appTextTheme.headline5,
        ),
        const Spacer(),
        Text(
          "show all ",
          style: appTextTheme.headline5?.copyWith(color: AppColors.mainColor),
        ),
      ],
    );
  }

  Padding _buildListTag(double _paddingHorizontal, double _heightScreen) {
    return Padding(
      padding: EdgeInsets.only(left: _paddingHorizontal),
      child: SizedBox(
          height: 300,
          child: Obx(
            () => controller.listTag.length == 0
                ? const ErrorWidgetCustom(
                    textError: "No data not found , check internet")
                : ListView.builder(
                    itemCount: controller.listTag.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      TagModel tagModel = controller.listTag[index];
                      return ItemTag(
                        tagModel: tagModel,
                      );
                    }),
          )),
    );
  }

  Padding _buildDividerProfile(double _heightScreen, double _widthScreen) {
    return Padding(
      padding: EdgeInsets.only(
          top: _heightScreen * (50 / 810),
          left: _widthScreen * (25 / 375),
          right: _widthScreen * (25 / 375)),
      child: const Divider(
        color: Color(0xffE6E6E6),
        thickness: 2,
      ),
    );
  }

  Padding _buildListViewProfile(
      double _heightScreen, double _paddingHorizontal) {
    return Padding(
      padding: EdgeInsets.only(
          top: _heightScreen * (15 / 810),
          left: _paddingHorizontal,
          right: _paddingHorizontal),
      child: SizedBox(
          height: Get.height * (231 / 810),
          child: Obx(() => ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: controller.listUser.length,
              itemBuilder: (context, index) {
                SearchSuggestionProfile searchSuggestionProfile =
                    controller.listUser[index];
                return ItemProfile(
                    searchSuggestionProfile: searchSuggestionProfile,
                    index: index);
              }))),
    );
  }

  Padding _buildTitleProfile(
      double _heightScreen, double _paddingHorizontal, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: _heightScreen * (25 / 810),
          left: _paddingHorizontal,
          right: _paddingHorizontal),
      child: Row(
        children: [
          Text(
            StringSearch.txtProfiles,
            style: appTextTheme.headline5,
          ),
          const Spacer(),
          Text(
            "show all",
            style: appTextTheme.headline5?.copyWith(color: AppColors.mainColor),
          ),
        ],
      ),
    );
  }

  Padding _buildDivider(double _paddingHorizontal, double _heightScreen) {
    return Padding(
      padding: EdgeInsets.only(
          left: _paddingHorizontal,
          right: _paddingHorizontal,
          top: _heightScreen * (50 / 810)),
      child: const Divider(
        color: Color(0xffE6E6E6),
        thickness: 2,
      ),
    );
  }

  Padding _buildListRecipe(double _heightScreen, double _paddingHorizontal) {
    return Padding(
      padding: EdgeInsets.only(
          top: _heightScreen * (15 / 810), left: _paddingHorizontal),
      child: SizedBox(
          height: Get.height * 0.26,
          child: Obx(() => controller.listRecipeFilter.isEmpty
              ? const ErrorWidgetCustom(textError: "Loading.....")
              : ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: controller.listRecipeFilter.length,
                  itemBuilder: (context, index) {
                    SearchSuggestionModel searchSuggestionModel =
                        controller.listRecipeFilter[index];
                    return ItemRecipe(
                      searchSuggestionModel: searchSuggestionModel,
                    );
                  }))),
    );
  }

  Padding _buildTitleRecipe(
      double _heightScreen, double _widthScreen, BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: _heightScreen * (25 / 810),
          left: _widthScreen * (25 / 375),
          right: _widthScreen * (25 / 375)),
      child: Row(
        children: [
          Text(
            StringSearch.txtRecipe,
            style: appTextTheme.headline5,
          ),
          const Spacer(),
          Text(
            "show all",
            style: appTextTheme.headline5?.copyWith(color: AppColors.mainColor),
          ),
        ],
      ),
    );
  }

  Padding _buildAppBar(double _heightScreen, double _widthScreen) {
    return Padding(
      padding: EdgeInsets.only(
          top: _heightScreen * (55 / 810),
          left: _widthScreen * (25 / 375),
          right: _widthScreen * (25 / 375)),
      child: AppBarSearch(
          hintTextFiled: "Sweep", colorIconFilter: AppColors.colorBlack),
    );
  }
}
