import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mock_team3/app/features/edit_recipe/views/widgets/cache_network_image_custom.dart';
import 'package:mock_team3/app/features/user_profile/view_model/user_profile_view_model.dart';
import 'package:mock_team3/app/routes/app_routes.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../utils/number_format.dart';

class UserInfo extends GetView<UserProfileViewModel> {
  const UserInfo({
    Key? key,
    required this.hasMoreOption,
  }) : super(key: key);
  final bool hasMoreOption;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        _buildAvatar(),
        const Spacer(),
        _buildInfo(),
        const Spacer(),
        _buildEditIcon()
      ],
    );
  }

  Flexible _buildEditIcon() {
    return Flexible(
      flex: 4,
      child: hasMoreOption
          ? IconButton(
              icon: SvgPicture.asset(IconBlack.iconEdit),
              onPressed: () => _goToEditUserProfile(),
            )
          : const Center(),
    );
  }

  Flexible _buildInfo() {
    return Flexible(
      flex: 18,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 2,
            child: Obx(
              () => Text(
                controller.userModel.value.account.fullname,
                style: appTextTheme.bodyText1!
                    .copyWith(fontWeight: FontWeight.w700),
              ),
            ),
          ),
          Flexible(
            flex: 2,
            child: Obx(
              () => Text(
                controller.userModel.value.bio ?? "",
                style: appTextTheme.subtitle1,
              ),
            ),
          ),
          const Spacer(),
          Flexible(
            flex: 2,
            child: Row(
              children: [
                Flexible(
                  flex: 6,
                  child: Obx(
                    () => Text(
                      NumberFormat.numberToString(
                              controller.userModel.value.following?.length ??
                                  0) +
                          " followers",
                      style: appTextTheme.subtitle1,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Flexible(child: SvgPicture.asset(IconRed.iconDot)),
                Flexible(
                  flex: 4,
                  child: Obx(
                    () => Text(
                      NumberFormat.numberToString(
                              controller.userModel.value.likes ?? 0) +
                          " likes",
                      style: appTextTheme.subtitle1,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Flexible _buildAvatar() {
    return Flexible(
      flex: 10,
      child: AspectRatio(
        aspectRatio: 1 / 1,
        child: Obx(
          () => (controller.imagePath.value != "")
              ? ClipOval(
                  child: CircleAvatar(
                    backgroundImage:
                        FileImage(File(controller.imagePath.value)),
                  ),
                )
              : ClipOval(
                  child: CircleAvatar(
                    child: CacheNetworkImageCustom(
                      urlImage: controller.userAvatar.value,
                      boxFit: BoxFit.cover,
                    ),
                    backgroundColor: Colors.white,
                  ),
                ),
        ),
      ),
    );
  }

  void _goToEditUserProfile() {
    Get.toNamed(AppRoute.editUserProfileScreen);
  }
}
