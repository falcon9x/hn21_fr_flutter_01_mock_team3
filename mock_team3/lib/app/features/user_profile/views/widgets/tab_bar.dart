import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'follow_tab.dart';
import 'saved_tab.dart';
import '../../../../../generated/locales.g.dart';

import '../../../../core/app_colors.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../utils/number_format.dart';
import '../../../../widgets/custom_tab_bar_indicator.dart';
import '../../view_model/user_profile_view_model.dart';
import 'recipe_tab.dart';

class TabBarUser extends GetView<UserProfileViewModel> {
  const TabBarUser({
    Key? key,
    required this.isUser,
  }) : super(key: key);
  final bool isUser;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(child: _buildTabBar()),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: AppColors.colorDivider, width: 0.5),
                ),
              ),
              child: const TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: [
                  RecipesTab(),
                  SavedTab(),
                  FollowTab(),
                ],
              ),
            ),
          ),
        ],
      ),
      length: 3,
    );
  }

  Widget _buildTabBar() {
    return TabBar(
      onTap: (value) {
        controller.updateIndexTabBar(value);
      },
      indicator: CircleTabIndicator(
          color: AppColors.mainColor, radius: 100, indicatorHeight: 4),
      tabs: [
        Tab(
          height: kToolbarHeight,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(
                () => Text(
                  NumberFormat.numberToString(
                      controller.userModel.value.cookbooks?.length ?? 0),
                  style: controller.indexTabBar.value == 0
                      ? appTextTheme.headline4
                      : appTextTheme.headline4!
                          .copyWith(color: AppColors.colorBlackOpacity),
                ),
              ),
              Obx(
                () => Text(
                  LocaleKeys.userProfileString_firstTitleTabBar.tr,
                  style: controller.indexTabBar.value == 0
                      ? appTextTheme.bodyText1
                      : appTextTheme.bodyText1!
                          .copyWith(color: AppColors.colorBlackOpacity),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
        Tab(
          height: kToolbarHeight,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(
                () => Text(
                  NumberFormat.numberToString(
                      controller.userModel.value.saved?.length ?? 0),
                  style: controller.indexTabBar.value == 1
                      ? appTextTheme.headline4
                      : appTextTheme.headline4!
                          .copyWith(color: AppColors.colorBlackOpacity),
                ),
              ),
              Obx(
                () => Text(LocaleKeys.userProfileString_secondTitleTabBar.tr,
                    style: controller.indexTabBar.value == 1
                        ? appTextTheme.bodyText1
                        : appTextTheme.bodyText1!
                            .copyWith(color: AppColors.colorBlackOpacity),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis),
              )
            ],
          ),
        ),
        Tab(
          height: kToolbarHeight,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(
                () => Text(
                  NumberFormat.numberToString(
                      controller.userModel.value.following?.length ?? 0),
                  style: controller.indexTabBar.value == 2
                      ? appTextTheme.headline4
                      : appTextTheme.headline4!
                          .copyWith(color: AppColors.colorBlackOpacity),
                ),
              ),
              Obx(
                () => Text(
                  LocaleKeys.userProfileString_thirdTitleTabBar.tr,
                  style: controller.indexTabBar.value == 2
                      ? appTextTheme.bodyText1
                      : appTextTheme.bodyText1!
                          .copyWith(color: AppColors.colorBlackOpacity),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
