import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../core/app_assets.dart';
import '../../../../core/app_colors.dart';
import '../../../../core/app_padding.dart';
import '../../../../core/app_strings.dart';
import '../../../../core/app_text_theme.dart';
import '../../../../routes/app_routes.dart';

class CustomUserAppBar {
  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      leadingWidth: 0,
      title: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            UserProfileString.titleAppBar,
            style:
                appTextTheme.headline3!.copyWith(color: AppColors.colorBlack),
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
      actions: [
        Padding(
          padding: AppPadding.appBarPaddingOnlyRight,
          child: GestureDetector(
            onTap: () => _goToSettings(),
            child: Row(
              children: [
                SvgPicture.asset(IconBlack.iconSetting),
                Text(
                  UserProfileString.textEndAppBar,
                  style: appTextTheme.headline5!
                      .copyWith(color: AppColors.mainColor),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  void _goToSettings() {
    Get.toNamed(AppRoute.settingsScreen);
  }
}
