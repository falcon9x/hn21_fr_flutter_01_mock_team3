import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/app_padding.dart';
import '../view_model/user_profile_view_model.dart';
import 'widgets/user_info.dart';
import '../../../core/app_colors.dart';
import 'widgets/tab_bar.dart';

class UserProfileScreen extends GetView<UserProfileViewModel> {
  const UserProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: AppPadding.defaultPadding,
      child: Column(
        children: [
          const Expanded(
            flex: 5,
            child: UserInfo(
              hasMoreOption: true,
            ),
          ),
          Expanded(
            flex: 2,
            child: Center(
              child: Divider(
                thickness: 2,
                color: AppColors.colorDivider,
                height: 1,
              ),
            ),
          ),
          const Expanded(
            flex: 27,
            child: TabBarUser(
              isUser: true,
            ),
          )
        ],
      ),
    );
  }

// AppBar _buildAppBar() {
//   return AppBar(
//     elevation: 0,
//     backgroundColor: Colors.transparent,
//     leadingWidth: 0,
//     title: Row(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Text(
//           UserProfileString.titleAppBar,
//           style:
//               appTextTheme.headline3!.copyWith(color: AppColors.colorBlack),
//           overflow: TextOverflow.ellipsis,
//         ),
//       ],
//     ),
//     actions: [
//       Padding(
//         padding: AppPadding.appBarPaddingOnlyRight,
//         child: GestureDetector(
//           onTap: () => _goToSettings(),
//           child: Row(
//             children: [
//               SvgPicture.asset(IconBlack.iconSetting),
//               Text(
//                 UserProfileString.textEndAppBar,
//                 style: appTextTheme.headline5!
//                     .copyWith(color: AppColors.mainColor),
//               )
//             ],
//           ),
//         ),
//       )
//     ],
//   );
// }

// void _goToSettings() {
//   Get.toNamed(AppRoute.settingsScreen);
// }
}
