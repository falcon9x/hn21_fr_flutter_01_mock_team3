import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mock_team3/app/utils/share_prefs_key.dart';
import 'package:mock_team3/app/utils/share_prefs_repository.dart';
import 'package:mock_team3/data/cook_book/model/cook_book_model.dart';
import 'package:mock_team3/data/cook_book/model/dto/cook_book_ui_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:mock_team3/data/user/dto/user_follow_ui_model.dart';
import '../../../../data/base/data_result.dart';
import '../../../../data/user/model/user_model.dart';
import '../../../base/base_viewmodel.dart';
import 'package:mock_team3/data/user/user_repository.dart';

typedef CallBackWithParams = Function(String value);

class UserProfileViewModel extends BaseViewModel {
  final UserRepository _userRepository;
  final SharePrefsRepository _sharePrefsRepository;
  RxList<UserModel> listFollowing = RxList();
  RxList<CookBookModel> listSaved = RxList();
  RxList<CookBookModel> listCookbook = RxList();

  RxList<CookBookUIModel> listCookbookUIModel = RxList();
  RxList<CookBookUIModel> listCookbookSavedUIModel = RxList();
  RxList<UserFollowUIModel> listUserFollowUIModel = RxList();

  final indexTabBar = 0.obs;
  final userModel = UserModel.withInitial().obs;
  final userAvatar = "".obs;
  final imagePath = "".obs;
  String idUserCurrent = "";

  UserProfileViewModel(this._userRepository, this._sharePrefsRepository);

  void getUserModel() async {
    idUserCurrent = _sharePrefsRepository.getString(SharePrefsKey.userId);
    DataResult result = await _userRepository.getUserProfile(idUserCurrent);
    if (result.isSuccess) {
      userModel.value = (result.data as UserModel);
      _updateSettings();
      userAvatar.value =
          await _getImageDownloadUrl(userModel.value.avatar ?? "");
      await _getListCookbook(userModel.value.cookbooks);
      await _getListRecipe(listCookbook);
    } else {
      if (result is APIFailure) {
        if ((result as APIFailure).errorResponse == "data null") {
        } else {}
      }
    }
  }

  Future<String> _getImageDownloadUrl(String imagePath) async {
    DataResult result = await _userRepository.getImageFromStorage(imagePath);
    log(result.data);
    if (result.isSuccess) {
      return result.data;
    } else {
      //TODO
      return "";
    }
  }

  void updateIndexTabBar(int indexTabBarNew) {
    indexTabBar.value = indexTabBarNew;
    if (indexTabBar.value == 1) {
      if (listSaved.isEmpty) {
        _getSavedCookBooks();
      }
    } else if (indexTabBar.value == 2) {
      if (listUserFollowUIModel.isEmpty) {
        _getListUserFollow();
      }
    }
  }

  void updateUserProfile(
      VoidCallback showSuccess, CallBackWithParams showFailure) async {
    if (imagePath.value != "") {
      DataResult result =
          await _userRepository.uploadImageToFirebase(imagePath.value);
      if (result.isSuccess) {
        userModel.value.avatar = result.data;
      } else if (result.isFailure && result.error is APIFailure) {
        showFailure((result.error as APIFailure).errorResponse);
      }
    }
    DataResult dataResult =
        await _userRepository.updateUserProfile(userModel.value, idUserCurrent);
    userModel.refresh();
    if (dataResult.isSuccess) {
      showSuccess();
    } else if (dataResult.isFailure && dataResult.error is APIFailure) {
      showFailure((dataResult.error as APIFailure).errorResponse);
    }
  }

  Future<void> _getListCookbook(List<String>? cookbooks) async {
    DataResult result =
        await _userRepository.getListCookbook(cookbooks ?? List.empty());
    if (result.isSuccess) {
      listCookbook.value = (result.data as List<CookBookModel>);
    } else {
      if (result is APIFailure) {}
    }
  }

  Future<void> _getListRecipe(RxList<CookBookModel> listCookbook) async {
    RecipeModel recipeModel;
    for (int indexCookbook = 0;
        indexCookbook < listCookbook.length;
        indexCookbook++) {
      DataResult dataResult = await _userRepository
          .getRecipeById(listCookbook[indexCookbook].recipes![0]);
      if (dataResult.isSuccess) {
        recipeModel = dataResult.data;
        listCookbookUIModel.add(CookBookUIModel(
            id: userModel.value.cookbooks![indexCookbook],
            name: listCookbook[indexCookbook].name,
            imageUrl: await _getImageDownloadUrl(
                recipeModel.galleries![recipeModel.indexCover])));
      } else {}
    }
  }

  void _updateSettings() {
    _sharePrefsRepository.saveBool(SharePrefsKey.isWhenSomeoneSendMeAMessage,
        userModel.value.settings.isWhenSomeOneSendMeAMessage);
    _sharePrefsRepository.saveBool(
        SharePrefsKey.isFollowersCanSeeProfilesIFollow,
        userModel.value.settings.isFollowersCanSeeFollowings);
    _sharePrefsRepository.saveBool(SharePrefsKey.isWhenSomeoneDoLiveCooking,
        userModel.value.settings.isSomeOneLiveCook);
    _sharePrefsRepository.saveBool(
        SharePrefsKey.isFollowersCanSeeMySavedRecipes,
        userModel.value.settings.isFollowersCanSeeMyRecipes);
    _sharePrefsRepository.saveBool(SharePrefsKey.isNotifyMeForFollowers,
        userModel.value.settings.isNotifyMeForFollowers);
  }

  void _getSavedCookBooks() async {
    await _getListSavedCookbook(userModel.value.saved);
    await _getListSavedRecipe(listSaved);
  }

  Future<void> _getListSavedCookbook(List<String>? saved) async {
    DataResult result =
        await _userRepository.getListCookbook(saved ?? List.empty());
    if (result.isSuccess) {
      listSaved.value = (result.data as List<CookBookModel>);
    } else {
      if (result is APIFailure) {}
    }
  }

  Future<void> _getListSavedRecipe(RxList<CookBookModel> listSaved) async {
    RecipeModel recipeModel;
    for (int indexCookbook = 0;
        indexCookbook < listSaved.length;
        indexCookbook++) {
      DataResult dataResult = await _userRepository
          .getRecipeById(listSaved[indexCookbook].recipes![0]);
      if (dataResult.isSuccess) {
        recipeModel = dataResult.data;
        listCookbookSavedUIModel.add(CookBookUIModel(
            id: userModel.value.cookbooks![indexCookbook],
            name: listSaved[indexCookbook].name,
            imageUrl: await _getImageDownloadUrl(
                recipeModel.galleries![recipeModel.indexCover])));
      } else {}
    }
  }

  Future<void> getImageFromGallery() async {
    try {
      final ImagePicker _picker = ImagePicker();
      final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
      if (image != null) {
        imagePath.value = image.path;
      }
    } catch (ex) {
      log(ex.toString());
    }
  }

  Future<void> uploadImageToFirebase() async {
    DataResult result =
        await _userRepository.uploadImageToFirebase(imagePath.value);
    if (result.isSuccess) {
    } else {}
  }

  void _getListUserFollow() async {
    DataResult result =
        await _userRepository.getListUserFollow(userModel.value.following!);
    if (result.isSuccess) {
      List<UserModel> listUserModel = result.data;
      for (int indexList = 0; indexList < listUserModel.length; indexList++) {
        listUserFollowUIModel.add(UserFollowUIModel(
            uid: userModel.value.following![indexList],
            name: listUserModel[indexList].account.fullname,
            avatar: await _getImageDownloadUrl(
                listUserModel[indexList].avatar ?? "")));
      }
    } else {}
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getUserModel();
  }
}
