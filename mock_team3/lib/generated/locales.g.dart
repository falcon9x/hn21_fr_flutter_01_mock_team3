// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars, constant_identifier_names
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'en_US': Locales.en_US,
    'ja_JP': Locales.ja_JP,
    'vi_VN': Locales.vi_VN,
    'zh_CN': Locales.zh_CN,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const recipeFeedString_btnSave = 'recipeFeedString_btnSave';
  static const recipeFeedString_titleDialog = 'recipeFeedString_titleDialog';
  static const recipeFeedString_txtAddNewCookBook =
      'recipeFeedString_txtAddNewCookBook';
  static const userProfileString_titleAppBar = 'userProfileString_titleAppBar';
  static const userProfileString_textEndAppBar =
      'userProfileString_textEndAppBar';
  static const userProfileString_firstTitleTabBar =
      'userProfileString_firstTitleTabBar';
  static const userProfileString_secondTitleTabBar =
      'userProfileString_secondTitleTabBar';
  static const userProfileString_thirdTitleTabBar =
      'userProfileString_thirdTitleTabBar';
  static const otherUserProfileString_titleAppBar =
      'otherUserProfileString_titleAppBar';
  static const otherUserProfileString_textButton =
      'otherUserProfileString_textButton';
  static const editUserString_titleAppBar = 'editUserString_titleAppBar';
  static const editUserString_titleHeader = 'editUserString_titleHeader';
  static const editUserString_titleUnderAvatar =
      'editUserString_titleUnderAvatar';
  static const editUserString_firstLabelFormField =
      'editUserString_firstLabelFormField';
  static const editUserString_secondLabelFormField =
      'editUserString_secondLabelFormField';
  static const editUserString_thirdLabelFormField =
      'editUserString_thirdLabelFormField';
  static const editUserString_fourthLabelFormField =
      'editUserString_fourthLabelFormField';
  static const editUserString_textButton = 'editUserString_textButton';
  static const signUpString_titleFirst = 'signUpString_titleFirst';
  static const signUpString_titleSecond = 'signUpString_titleSecond';
  static const signUpString_subtitle = 'signUpString_subtitle';
  static const signUpString_fullNameTextField =
      'signUpString_fullNameTextField';
  static const signUpString_emailTextField = 'signUpString_emailTextField';
  static const signUpString_passwordTextField =
      'signUpString_passwordTextField';
  static const signUpString_textButton = 'signUpString_textButton';
  static const signUpString_footer = 'signUpString_footer';
  static const signUpString_textButtonFooter = 'signUpString_textButtonFooter';
  static const browseMyRecipeString_textBackButton =
      'browseMyRecipeString_textBackButton';
  static const browseMyRecipeString_title = 'browseMyRecipeString_title';
  static const browseMyRecipeString_textButton =
      'browseMyRecipeString_textButton';
  static const browseMyRecipeString_timeFood = 'browseMyRecipeString_timeFood';
  static const browseMyRecipeString_ingredients =
      'browseMyRecipeString_ingredients';
  static const browseMyRecipeString_textButtonCook =
      'browseMyRecipeString_textButtonCook';
  static const viewRecipeString_textBackButton =
      'viewRecipeString_textBackButton';
  static const viewRecipeString_textButtonCook =
      'viewRecipeString_textButtonCook';
  static const viewRecipeString_firstNameTabbar =
      'viewRecipeString_firstNameTabbar';
  static const viewRecipeString_secondNameTabbar =
      'viewRecipeString_secondNameTabbar';
  static const viewRecipeString_thirdNameTabbar =
      'viewRecipeString_thirdNameTabbar';
  static const editRecipe_textBackButton = 'editRecipe_textBackButton';
  static const editRecipe_title = 'editRecipe_title';
  static const editRecipe_recipeName = 'editRecipe_recipeName';
  static const editRecipe_firstSection = 'editRecipe_firstSection';
  static const editRecipe_secondSection = 'editRecipe_secondSection';
  static const editRecipe_thirdSection = 'editRecipe_thirdSection';
  static const editRecipe_fourthSection = 'editRecipe_fourthSection';
  static const editRecipe_firstDialogAdd = 'editRecipe_firstDialogAdd';
  static const editRecipe_secondDialogAdd = 'editRecipe_secondDialogAdd';
  static const editRecipe_thirdDialogAdd = 'editRecipe_thirdDialogAdd';
  static const editRecipe_fourthDialogAdd = 'editRecipe_fourthDialogAdd';
  static const editRecipe_contentButtonOfAdd = 'editRecipe_contentButtonOfAdd';
  static const editRecipe_contentButton = 'editRecipe_contentButton';
  static const editRecipe_titleFooter = 'editRecipe_titleFooter';
  static const editGallery_title = 'editGallery_title';
  static const editGallery_images = 'editGallery_images';
  static const editGallery_textButton = 'editGallery_textButton';
  static const editGallery_setAsCover = 'editGallery_setAsCover';
  static const editGallery_remove = 'editGallery_remove';
  static const editGallery_labelText = 'editGallery_labelText';
  static const editGallery_contentButton = 'editGallery_contentButton';
  static const editIngredients_title = 'editIngredients_title';
  static const editIngredients_labelTextFiled =
      'editIngredients_labelTextFiled';
  static const editIngredients_labelText = 'editIngredients_labelText';
  static const editIngredients_contentButton = 'editIngredients_contentButton';
  static const editDirections_title = 'editDirections_title';
  static const editDirections_contentImageButton =
      'editDirections_contentImageButton';
  static const editDirections_labelTextFiled = 'editDirections_labelTextFiled';
  static const editDirections_labelText = 'editDirections_labelText';
  static const editDirections_contentButton = 'editDirections_contentButton';
  static const additionalInfo_title = 'additionalInfo_title';
  static const additionalInfo_firstLabelTextFiled =
      'additionalInfo_firstLabelTextFiled';
  static const additionalInfo_secondLabelTextFiled =
      'additionalInfo_secondLabelTextFiled';
  static const additionalInfo_thirdLabelTextFiled =
      'additionalInfo_thirdLabelTextFiled';
  static const additionalInfo_contentButton = 'additionalInfo_contentButton';
  static const newRecipe_title = 'newRecipe_title';
  static const newRecipe_contentBackButton = 'newRecipe_contentBackButton';
  static const newRecipe_labelText = 'newRecipe_labelText';
  static const cookingMode_title = 'cookingMode_title';
  static const cookingMode_contentBackButton = 'cookingMode_contentBackButton';
  static const cookingMode_labelText = 'cookingMode_labelText';
  static const cookingMode_setps = 'cookingMode_setps';
  static const cookingMode_textButton = 'cookingMode_textButton';
  static const searchSuggestion_contentSearchBar =
      'searchSuggestion_contentSearchBar';
  static const searchSuggestion_titleFirstSection =
      'searchSuggestion_titleFirstSection';
  static const searchSuggestion_titleSecondSection =
      'searchSuggestion_titleSecondSection';
  static const search_titleFirstSection = 'search_titleFirstSection';
  static const search_titleSecondSection = 'search_titleSecondSection';
  static const search_textButton = 'search_textButton';
  static const searchFilter_titleFirstSection =
      'searchFilter_titleFirstSection';
  static const searchFilter_labelFirstSlider = 'searchFilter_labelFirstSlider';
  static const searchFilter_labelSecondSlider =
      'searchFilter_labelSecondSlider';
  static const searchFilter_titleSecondSection =
      'searchFilter_titleSecondSection';
  static const searchFilter_contentButton = 'searchFilter_contentButton';
  static const settings_titleLeadingAppBar = 'settings_titleLeadingAppBar';
  static const settings_titleActionAppBar = 'settings_titleActionAppBar';
  static const settings_titleHeader = 'settings_titleHeader';
  static const settings_titleFirstGroup = 'settings_titleFirstGroup';
  static const settings_titleFirstGroupFirstTitle =
      'settings_titleFirstGroupFirstTitle';
  static const settings_titleFirstGroupSecondTitle =
      'settings_titleFirstGroupSecondTitle';
  static const settings_titleFirstGroupThirdTitle =
      'settings_titleFirstGroupThirdTitle';
  static const settings_titleSecondGroup = 'settings_titleSecondGroup';
  static const settings_titleSecondGroupFirstTitle =
      'settings_titleSecondGroupFirstTitle';
  static const settings_titleSecondGroupFirstSubTitle =
      'settings_titleSecondGroupFirstSubTitle';
  static const settings_titleSecondGroupSecondSubTitle =
      'settings_titleSecondGroupSecondSubTitle';
  static const settings_titleSecondGroupSecondTitle =
      'settings_titleSecondGroupSecondTitle';
  static const settings_titleFooter = 'settings_titleFooter';
  static const login_title = 'login_title';
  static const login_subtitle = 'login_subtitle';
  static const login_emailTextField = 'login_emailTextField';
  static const login_passwordTextField = 'login_passwordTextField';
  static const login_forgotPassword = 'login_forgotPassword';
  static const login_contentButton = 'login_contentButton';
  static const login_footer = 'login_footer';
  static const login_textButtonFooter = 'login_textButtonFooter';
}

class Locales {
  static const en_US = {
    'recipeFeedString_btnSave': 'Save',
    'recipeFeedString_titleDialog': 'Save to',
    'recipeFeedString_txtAddNewCookBook': 'Add New Cookbook',
    'userProfileString_titleAppBar': 'My Kitchen',
    'userProfileString_textEndAppBar': 'Settings',
    'userProfileString_firstTitleTabBar': 'Recipes',
    'userProfileString_secondTitleTabBar': 'Saved',
    'userProfileString_thirdTitleTabBar': 'Following',
    'otherUserProfileString_titleAppBar': 'Back',
    'otherUserProfileString_textButton': 'Follow',
    'editUserString_titleAppBar': 'Back to Profile',
    'editUserString_titleHeader': 'Edit Profile',
    'editUserString_titleUnderAvatar': 'Edit Profile Picture',
    'editUserString_firstLabelFormField': 'Full Name',
    'editUserString_secondLabelFormField': 'Bio',
    'editUserString_thirdLabelFormField': 'Email',
    'editUserString_fourthLabelFormField': 'Phone',
    'editUserString_textButton': 'Save Profile',
    'signUpString_titleFirst': 'Start',
    'signUpString_titleSecond': 'from Scratch',
    'signUpString_subtitle': 'Create account to continue.',
    'signUpString_fullNameTextField': 'Full Name',
    'signUpString_emailTextField': 'Email',
    'signUpString_passwordTextField': 'Password',
    'signUpString_textButton': 'Create Account',
    'signUpString_footer': 'Already have an account?',
    'signUpString_textButtonFooter': 'Login Here',
    'browseMyRecipeString_textBackButton': 'Back to My Profile',
    'browseMyRecipeString_title': 'My Recipes',
    'browseMyRecipeString_textButton': 'Add New',
    'browseMyRecipeString_timeFood': 'mins',
    'browseMyRecipeString_ingredients': 'ingredients',
    'browseMyRecipeString_textButtonCook': 'Cook',
    'viewRecipeString_textBackButton': 'Back to My Profile',
    'viewRecipeString_textButtonCook': 'Cook Now',
    'viewRecipeString_firstNameTabbar': 'Ingredients',
    'viewRecipeString_secondNameTabbar': 'How to Cook',
    'viewRecipeString_thirdNameTabbar': 'Add',
    'editRecipe_textBackButton': 'Back to My Recipes',
    'editRecipe_title': 'Edit Recipe',
    'editRecipe_recipeName': 'Recipe Name',
    'editRecipe_firstSection': 'Gallery',
    'editRecipe_secondSection': 'Ingredients',
    'editRecipe_thirdSection': 'How to Cook',
    'editRecipe_fourthSection': 'Additional Info',
    'editRecipe_firstDialogAdd': 'Serving Time',
    'editRecipe_secondDialogAdd': 'Nutrition Facts',
    'editRecipe_thirdDialogAdd': 'Tags',
    'editRecipe_fourthDialogAdd': 'Save to',
    'editRecipe_contentButtonOfAdd': 'Save Recipe',
    'editRecipe_contentButton': 'Post to Feed',
    'editRecipe_titleFooter': 'Remove from Cookbook',
    'editGallery_title': 'Edit Gallery',
    'editGallery_images': 'Images',
    'editGallery_textButton': 'View All',
    'editGallery_setAsCover': 'Set as Cover',
    'editGallery_remove': 'Remove',
    'editGallery_labelText': 'Upload Images or Open Camera',
    'editGallery_contentButton': 'Save Gallery',
    'editIngredients_title': 'Edit Ingredients',
    'editIngredients_labelTextFiled': 'Write Ingredient',
    'editIngredients_labelText': 'Add Ingredient',
    'editIngredients_contentButton': 'Save Ingredients',
    'editDirections_title': 'Edit Directions',
    'editDirections_contentImageButton': 'Edit Annotations',
    'editDirections_labelTextFiled': 'Write Directions',
    'editDirections_labelText': 'Add Directions',
    'editDirections_contentButton': 'Save Directions',
    'additionalInfo_title': 'Edit Directions',
    'additionalInfo_firstLabelTextFiled': 'Serving Time (±)',
    'additionalInfo_secondLabelTextFiled': 'Nutrition Facts',
    'additionalInfo_thirdLabelTextFiled': 'Tags',
    'additionalInfo_contentButton': 'Save Info',
    'newRecipe_title': 'New Recipe',
    'newRecipe_contentBackButton': 'Back to My Recipes',
    'newRecipe_labelText': 'Add Info',
    'cookingMode_title': 'Cooking Mode',
    'cookingMode_contentBackButton': 'Back to Recipe',
    'cookingMode_labelText': 'Add Info',
    'cookingMode_setps': 'Steps',
    'cookingMode_textButton': 'View Ingredients',
    'searchSuggestion_contentSearchBar': 'Search recipe, people, or tag',
    'searchSuggestion_titleFirstSection': 'Trending Recipes',
    'searchSuggestion_titleSecondSection': 'What can I make with..',
    'search_titleFirstSection': 'Recipes',
    'search_titleSecondSection': 'Profiles',
    'search_textButton': 'show all',
    'searchFilter_titleFirstSection': 'Search Filter',
    'searchFilter_labelFirstSlider': 'Ingredients',
    'searchFilter_labelSecondSlider': 'Serving Time',
    'searchFilter_titleSecondSection': 'Search for',
    'searchFilter_contentButton': 'Apply Filter',
    'settings_titleLeadingAppBar': 'Back',
    'settings_titleActionAppBar': 'Log Out',
    'settings_titleHeader': 'Settings',
    'settings_titleFirstGroup': 'Push Notifications',
    'settings_titleFirstGroupFirstTitle': 'Notify me for followers',
    'settings_titleFirstGroupSecondTitle': 'When someone send me a message',
    'settings_titleFirstGroupThirdTitle': 'When someone do live cooking',
    'settings_titleSecondGroup': 'Privacy Settings',
    'settings_titleSecondGroupFirstTitle': 'Followers can see my saved recipes',
    'settings_titleSecondGroupFirstSubTitle':
        'If disabled, you won\'t be able to see recipes saved by other profiles. Leave this enabled to share your collected recipes to others.',
    'settings_titleSecondGroupSecondSubTitle': 'why this matter?',
    'settings_titleSecondGroupSecondTitle':
        'Followers can see profiles I follow',
    'settings_titleFooter': 'Change Password',
    'login_title': 'Welcome Back!',
    'login_subtitle': 'Please login to continue.',
    'login_emailTextField': 'Email address',
    'login_passwordTextField': 'Password',
    'login_forgotPassword': 'Forgot password?',
    'login_contentButton': 'Login',
    'login_footer': 'New to Scratch?',
    'login_textButtonFooter': 'Create Account Here',
  };
  static const ja_JP = {
    'recipeFeedString_btnSave': '保存',
    'recipeFeedString_titleDialog': 'に保存',
    'recipeFeedString_txtAddNewCookBook': '新しいクックブックの追加',
    'userProfileString_titleAppBar': '私台所',
    'userProfileString_textEndAppBar': '設定',
    'userProfileString_firstTitleTabBar': 'レシピ',
    'userProfileString_secondTitleTabBar': '保存した',
    'userProfileString_thirdTitleTabBar': '続く',
    'otherUserProfileString_titleAppBar': '戻る',
    'otherUserProfileString_textButton': '従う',
    'editUserString_titleAppBar': 'プロファイルに戻る',
    'editUserString_titleHeader': 'プロファイル編集',
    'editUserString_titleUnderAvatar': 'プロフィール写真の編集',
    'editUserString_firstLabelFormField': 'フルネーム',
    'editUserString_secondLabelFormField': 'バイオ',
    'editUserString_thirdLabelFormField': 'Eメール',
    'editUserString_fourthLabelFormField': '電話',
    'editUserString_textButton': 'プロファイルを保存',
    'signUpString_titleFirst': '始める',
    'signUpString_titleSecond': 'Scratchから',
    'signUpString_subtitle': 'アカウントを作成して続行。',
    'signUpString_fullNameTextField': 'フルネーム',
    'signUpString_emailTextField': 'Eメール',
    'signUpString_passwordTextField': 'パスワード',
    'signUpString_textButton': 'アカウントを作成する',
    'signUpString_footer': 'すでにアカウントをお持ちですか?',
    'signUpString_textButtonFooter': 'ここでログイン',
    'browseMyRecipeString_textBackButton': 'プロフィールに戻る',
    'browseMyRecipeString_title': '私のレシピ',
    'browseMyRecipeString_textButton': '新しく追加する',
    'browseMyRecipeString_timeFood': '分',
    'browseMyRecipeString_ingredients': '材料',
    'browseMyRecipeString_textButtonCook': '料理',
    'viewRecipeString_textBackButton': 'プロフィールに戻る',
    'viewRecipeString_textButtonCook': '今すぐ調理',
    'viewRecipeString_firstNameTabbar': '材料',
    'viewRecipeString_secondNameTabbar': '料理の仕方',
    'viewRecipeString_thirdNameTabbar': '追加',
    'editRecipe_textBackButton': 'レシピに戻る',
    'editRecipe_title': 'レシピの編集',
    'editRecipe_recipeName': 'レシピ名',
    'editRecipe_firstSection': 'ギャラリー',
    'editRecipe_secondSection': '材料',
    'editRecipe_thirdSection': '料理の仕方',
    'editRecipe_fourthSection': '追加情報',
    'editRecipe_firstDialogAdd': 'サービングタイム',
    'editRecipe_secondDialogAdd': '栄養成分表',
    'editRecipe_thirdDialogAdd': 'タグ',
    'editRecipe_fourthDialogAdd': 'に保存',
    'editRecipe_contentButtonOfAdd': 'レシピを保存',
    'editRecipe_contentButton': 'フィードに投稿',
    'editRecipe_titleFooter': 'クックブックから削除',
    'editGallery_title': 'ギャラリーを編集',
    'editGallery_images': '画像',
    'editGallery_textButton': 'すべて表示',
    'editGallery_setAsCover': 'カバーとして設定',
    'editGallery_remove': '削除',
    'editGallery_labelText': '画像をアップロードするか,カメラを開く',
    'editGallery_contentButton': 'ギャラリーを保存',
    'editIngredients_title': '材料を編集',
    'editIngredients_labelTextFiled': '材料を書く',
    'editIngredients_labelText': '材料を追加',
    'editIngredients_contentButton': '材料を保存',
    'editDirections_title': '道順を編集',
    'editDirections_contentImageButton': '注釈を編集する',
    'editDirections_labelTextFiled': '道順を書く',
    'editDirections_labelText': '道順を追加',
    'editDirections_contentButton': '道順を保存',
    'additionalInfo_title': '道順を編集',
    'additionalInfo_firstLabelTextFiled': 'サービング時間 (±)',
    'additionalInfo_secondLabelTextFiled': '栄養成分表',
    'additionalInfo_thirdLabelTextFiled': 'タグ',
    'additionalInfo_contentButton': '情報を保存',
    'newRecipe_title': '新しいレシピ',
    'newRecipe_contentBackButton': 'レシピに戻る',
    'newRecipe_labelText': '情報を追加',
    'cookingMode_title': '調理モード',
    'cookingMode_contentBackButton': 'レシピに戻る',
    'cookingMode_labelText': '情報を追加',
    'cookingMode_setps': 'ステップ',
    'cookingMode_textButton': '材料を見る',
    'searchSuggestion_contentSearchBar': 'レシピ、人、またはタグを検索する',
    'searchSuggestion_titleFirstSection': 'トレンドレシピ',
    'searchSuggestion_titleSecondSection': '何で作れますか。',
    'search_titleFirstSection': 'レシピ',
    'search_titleSecondSection': 'プロファイル',
    'search_textButton': 'すべて表示する',
    'searchFilter_titleFirstSection': '検索フィルター',
    'searchFilter_labelFirstSlider': '材料',
    'searchFilter_labelSecondSlider': 'サービングタイム',
    'searchFilter_titleSecondSection': '検索する',
    'searchFilter_contentButton': 'フィルタを適用する',
    'settings_titleLeadingAppBar': '戻る',
    'settings_titleActionAppBar': 'ログアウト',
    'settings_titleHeader': '設定',
    'settings_titleFirstGroup': 'プッシュ通知',
    'settings_titleFirstGroupFirstTitle': 'フォロワーに通知する',
    'settings_titleFirstGroupSecondTitle': '誰かが私にメッセージを送ったとき',
    'settings_titleFirstGroupThirdTitle': '誰かがライブクッキングをするとき',
    'settings_titleSecondGroup': 'プライバシー設定',
    'settings_titleSecondGroupFirstTitle': 'フォロワーは私の保存したレシピを見る',
    'settings_titleSecondGroupFirstSubTitle':
        '無効にすると、他のプロファイルによって保存されたレシピを表示できなくなります。 収集したレシピを他の人と共有するには、これを有効のままにしておきます。',
    'settings_titleSecondGroupSecondSubTitle': 'なぜこれが重要なのですか?',
    'settings_titleSecondGroupSecondTitle': 'フォロワーは私がフォローしているプロファイルを見る',
    'settings_titleFooter': 'パスワードを変更する',
    'login_title': 'いらっしゃいませ!',
    'login_subtitle': '続行するにはログインしてください。',
    'login_emailTextField': '電子メールアドレス',
    'login_passwordTextField': 'パスワード',
    'login_forgotPassword': 'パスワードをお忘れですか?',
    'login_contentButton': 'ログイン',
    'login_footer': 'スクラッチは初めてですか?',
    'login_textButtonFooter': 'ここでアカウントを作成',
  };
  static const vi_VN = {
    'recipeFeedString_btnSave': 'Lưu',
    'recipeFeedString_titleDialog': 'Lưu vào',
    'recipeFeedString_txtAddNewCookBook': 'Thêm sách dạy nấu ăn mới',
    'userProfileString_titleAppBar': 'Nhà bếp',
    'userProfileString_textEndAppBar': 'Cài đặt',
    'userProfileString_firstTitleTabBar': 'Công thức nấu ăn',
    'userProfileString_secondTitleTabBar': 'Đã lưu',
    'userProfileString_thirdTitleTabBar': 'Tiếp theo',
    'otherUserProfileString_titleAppBar': 'Trở lại',
    'otherUserProfileString_textButton': 'Theo',
    'editUserString_titleAppBar': 'Quay lại hồ sơ',
    'editUserString_titleHeader': 'Chỉnh sửa hồ sơ',
    'editUserString_titleUnderAvatar': 'Chỉnh sửa ảnh đại diện',
    'editUserString_firstLabelFormField': 'Họ và tên',
    'editUserString_secondLabelFormField': 'Tiểu sử',
    'editUserString_thirdLabelFormField': 'E-mail',
    'editUserString_fourthLabelFormField': 'Điện thoại',
    'editUserString_textButton': 'Lưu hồ sơ',
    'signUpString_titleFirst': 'Bắt đầu',
    'signUpString_titleSecond': 'từ Scratch',
    'signUpString_subtitle': 'Tạo tài khoản để tiếp tục.',
    'signUpString_fullNameTextField': 'Họ và tên',
    'signUpString_emailTextField': 'E-mail',
    'signUpString_passwordTextField': 'Mật khẩu',
    'signUpString_textButton': 'Tạo tài khoản',
    'signUpString_footer': 'Tạo tài khoản mới?',
    'signUpString_textButtonFooter': 'Đăng nhập',
    'browseMyRecipeString_textBackButton': 'Quay lại hồ sơ',
    'browseMyRecipeString_title': 'Công thức nấu ăn',
    'browseMyRecipeString_textButton': 'Thêm',
    'browseMyRecipeString_timeFood': 'phút',
    'browseMyRecipeString_ingredients': 'thành phần',
    'browseMyRecipeString_textButtonCook': 'Nấu ăn',
    'viewRecipeString_textBackButton': 'Quay lại hồ sơ',
    'viewRecipeString_textButtonCook': 'Nấu ngay',
    'viewRecipeString_firstNameTabbar': 'Thành phần',
    'viewRecipeString_secondNameTabbar': 'Cách nấu',
    'viewRecipeString_thirdNameTabbar': 'Thêm',
    'editRecipe_textBackButton': 'Quay lại công thức',
    'editRecipe_title': 'Chỉnh sửa công thức',
    'editRecipe_recipeName': 'Tên công thức',
    'editRecipe_firstSection': 'Bộ sưu tập',
    'editRecipe_secondSection': 'Thành phần',
    'editRecipe_thirdSection': 'Cách nấu',
    'editRecipe_fourthSection': 'Thông tin bổ sung',
    'editRecipe_firstDialogAdd': 'Thời gian phục vụ',
    'editRecipe_secondDialogAdd': 'Giá trị dinh dưỡng',
    'editRecipe_thirdDialogAdd': 'Thẻ',
    'editRecipe_fourthDialogAdd': 'Lưu vào',
    'editRecipe_contentButtonOfAdd': 'Lưu',
    'editRecipe_contentButton': 'Đăng lên',
    'editRecipe_titleFooter': 'Xóa khỏi danh sách',
    'editGallery_title': 'Chỉnh sửa Thư viện',
    'editGallery_images': 'Hình ảnh',
    'editGallery_textButton': 'Xem tất cả',
    'editGallery_setAsCover': 'Đặt làm bìa',
    'editGallery_remove': 'Xóa',
    'editGallery_labelText': 'Tải lên hình ảnh hoặc mở máy ảnh',
    'editGallery_contentButton': 'Lưu thư viện',
    'editIngredients_title': 'Chỉnh sửa thành phần',
    'editIngredients_labelTextFiled': 'Viết thành phần',
    'editIngredients_labelText': 'Thêm thành phần',
    'editIngredients_contentButton': 'Lưu thành phần',
    'editDirections_title': 'Chỉnh sửa chỉ đường',
    'editDirections_contentImageButton': 'Chỉnh sửa chú thích',
    'editDirections_labelTextFiled': 'Viết chỉ đường',
    'editDirections_labelText': 'Thêm chỉ đường',
    'editDirections_contentButton': 'Lưu chỉ đường',
    'additionalInfo_title': 'Chỉnh sửa chỉ đường',
    'additionalInfo_firstLabelTextFiled': 'Thời gian phục vụ (±)',
    'additionalInfo_secondLabelTextFiled': 'Giá trị dinh dưỡng',
    'additionalInfo_thirdLabelTextFiled': 'Thẻ',
    'additionalInfo_contentButton': 'Lưu thông tin',
    'newRecipe_title': 'Công thức mới',
    'newRecipe_contentBackButton': 'Quay lại công thức nấu ăn',
    'newRecipe_labelText': 'Thêm thông tin',
    'cookingMode_title': 'Chế độ nấu ăn',
    'cookingMode_contentBackButton': 'Quay lại công thức',
    'cookingMode_labelText': 'Thêm thông tin',
    'cookingMode_setps': 'Các bước',
    'cookingMode_textButton': 'Xem thành phần',
    'searchSuggestion_contentSearchBar': 'Tìm kiếm công thức, người hoặc thẻ',
    'searchSuggestion_titleFirstSection': 'Công thức nấu ăn thịnh hành',
    'searchSuggestion_titleSecondSection': 'Tôi có thể làm gì với ..',
    'search_titleFirstSection': 'Công thức nấu ăn',
    'search_titleSecondSection': 'Hồ sơ',
    'search_textButton': 'hiển thị tất cả',
    'searchFilter_titleFirstSection': 'Bộ lọc tìm kiếm',
    'searchFilter_labelFirstSlider': 'Thành phần',
    'searchFilter_labelSecondSlider': 'Thời gian phục vụ',
    'searchFilter_titleSecondSection': 'Tìm kiếm',
    'searchFilter_contentButton': 'Áp dụng bộ lọc',
    'settings_titleLeadingAppBar': 'Trở lại',
    'settings_titleActionAppBar': 'Đăng xuất',
    'settings_titleHeader': 'Cài đặt',
    'settings_titleFirstGroup': 'Đẩy thông báo',
    'settings_titleFirstGroupFirstTitle':
        'Thông báo cho tôi về những người theo dõi',
    'settings_titleFirstGroupSecondTitle': 'Khi ai đó gửi tin nhắn cho tôi',
    'settings_titleFirstGroupThirdTitle': 'Khi ai đó nấu ăn trực tiếp',
    'settings_titleSecondGroup': 'Những thiết lập riêng tư',
    'settings_titleSecondGroupFirstTitle':
        'Người theo dõi có thể xem các công thức nấu ăn đã lưu',
    'settings_titleSecondGroupFirstSubTitle':
        'Nếu bị vô hiệu hóa, bạn sẽ không thể xem các công thức nấu ăn được lưu bởi các hồ sơ khác. Bật tính năng này để chia sẻ công thức nấu ăn đã thu thập của bạn cho người khác.',
    'settings_titleSecondGroupSecondSubTitle': 'tại sao vấn đề này?',
    'settings_titleSecondGroupSecondTitle':
        'Người theo dõi có thể xem hồ sơ tôi theo dõi',
    'settings_titleFooter': 'Đổi mật khẩu',
    'login_title': 'Chào mừng trở lại!',
    'login_subtitle': 'Làm ơn đăng nhập để tiếp tục.',
    'login_emailTextField': 'Email',
    'login_passwordTextField': 'Mật khẩu',
    'login_forgotPassword': 'Quên mật khẩu?',
    'login_contentButton': 'Đăng nhập',
    'login_footer': 'Mới sử dụng Scratch?',
    'login_textButtonFooter': 'Tạo tài khoản tại đây',
  };
  static const zh_CN = {
    'recipeFeedString_btnSave': '保存',
    'recipeFeedString_titleDialog': '存到',
    'recipeFeedString_txtAddNewCookBook': '添加新食谱',
    'userProfileString_titleAppBar': '我的厨房',
    'userProfileString_textEndAppBar': '设置',
    'userProfileString_firstTitleTabBar': '食谱',
    'userProfileString_secondTitleTabBar': '已保存',
    'userProfileString_thirdTitleTabBar': '下列的',
    'otherUserProfileString_titleAppBar': '后退',
    'otherUserProfileString_textButton': '跟随',
    'editUserString_titleAppBar': '返回个人资料',
    'editUserString_titleHeader': '编辑个人资料',
    'editUserString_titleUnderAvatar': '编辑头像',
    'editUserString_firstLabelFormField': '全名',
    'editUserString_secondLabelFormField': '生物',
    'editUserString_thirdLabelFormField': '电子邮件',
    'editUserString_fourthLabelFormField': '电话',
    'editUserString_textButton': '保存个人信息',
    'signUpString_titleFirst': '开始',
    'signUpString_titleSecond': '从Scratch',
    'signUpString_subtitle': '创建帐户以继续。',
    'signUpString_fullNameTextField': '全名',
    'signUpString_emailTextField': '电子邮件',
    'signUpString_passwordTextField': '密码',
    'signUpString_textButton': '创建帐号',
    'signUpString_footer': '已经有一个帐户?',
    'signUpString_textButtonFooter': '这里登录',
    'browseMyRecipeString_textBackButton': '返回我的个人资料',
    'browseMyRecipeString_title': '我的食谱',
    'browseMyRecipeString_textButton': '添新',
    'browseMyRecipeString_timeFood': '分钟',
    'browseMyRecipeString_ingredients': '配料',
    'browseMyRecipeString_textButtonCook': '厨师',
    'viewRecipeString_textBackButton': '返回我的个人资料',
    'viewRecipeString_textButtonCook': '现在做饭',
    'viewRecipeString_firstNameTabbar': '原料',
    'viewRecipeString_secondNameTabbar': '如何做饭',
    'viewRecipeString_thirdNameTabbar': '添加',
    'editRecipe_textBackButton': '回到我的食谱',
    'editRecipe_title': '编辑食谱',
    'editRecipe_recipeName': '配方名称',
    'editRecipe_firstSection': '画廊',
    'editRecipe_secondSection': '原料',
    'editRecipe_thirdSection': '如何做饭',
    'editRecipe_fourthSection': '附加信息',
    'editRecipe_firstDialogAdd': '服务时间',
    'editRecipe_secondDialogAdd': '营养成分',
    'editRecipe_thirdDialogAdd': '标签',
    'editRecipe_fourthDialogAdd': '存到',
    'editRecipe_contentButtonOfAdd': '保存食谱',
    'editRecipe_contentButton': '发布到饲料',
    'editRecipe_titleFooter': '从食谱中删除',
    'editGallery_title': '编辑画廊',
    'editGallery_images': '图片',
    'editGallery_textButton': '查看全部',
    'editGallery_setAsCover': '设为封面',
    'editGallery_remove': '消除',
    'editGallery_labelText': '上传图片或打开相机',
    'editGallery_contentButton': '保存画廊',
    'editIngredients_title': '编辑成分',
    'editIngredients_labelTextFiled': '写成分',
    'editIngredients_labelText': '添加成分',
    'editIngredients_contentButton': '保存成分',
    'editDirections_title': '编辑方向',
    'editDirections_contentImageButton': '编辑注释',
    'editDirections_labelTextFiled': '写方向',
    'editDirections_labelText': '添加路线',
    'editDirections_contentButton': '保存路线',
    'additionalInfo_title': '编辑方向',
    'additionalInfo_firstLabelTextFiled': '服务时间 (±)',
    'additionalInfo_secondLabelTextFiled': '营养成分',
    'additionalInfo_thirdLabelTextFiled': '标签',
    'additionalInfo_contentButton': '保存信息',
    'newRecipe_title': '新食谱',
    'newRecipe_contentBackButton': '回到我的食谱',
    'newRecipe_labelText': '添加信息',
    'cookingMode_title': '烹饪模式',
    'cookingMode_contentBackButton': '回到食谱',
    'cookingMode_labelText': '添加信息',
    'cookingMode_setps': '脚步',
    'cookingMode_textButton': '查看成分',
    'searchSuggestion_contentSearchBar': '搜索食谱、人员或标签',
    'searchSuggestion_titleFirstSection': '趋势食谱',
    'searchSuggestion_titleSecondSection': '我能做什么。。',
    'search_titleFirstSection': '食谱',
    'search_titleSecondSection': '简介',
    'search_textButton': '显示所有',
    'searchFilter_titleFirstSection': '搜索过滤器',
    'searchFilter_labelFirstSlider': '原料',
    'searchFilter_labelSecondSlider': '服务时间',
    'searchFilter_titleSecondSection': '搜索',
    'searchFilter_contentButton': '应用过滤器',
    'settings_titleLeadingAppBar': '后退',
    'settings_titleActionAppBar': '登出',
    'settings_titleHeader': '设置',
    'settings_titleFirstGroup': '推送通知',
    'settings_titleFirstGroupFirstTitle': '通知我关注者',
    'settings_titleFirstGroupSecondTitle': '当有人给我发消息时',
    'settings_titleFirstGroupThirdTitle': '当有人做现场烹饪时',
    'settings_titleSecondGroup': '隐私设置',
    'settings_titleSecondGroupFirstTitle': '关注者可以看到我保存的食谱',
    'settings_titleSecondGroupFirstSubTitle':
        '如果禁用, 您将无法查看其他配置文件保存的食谱。 将此启用以将您收集的食谱分享给其他人。',
    'settings_titleSecondGroupSecondSubTitle': '为什么这件事 ?',
    'settings_titleSecondGroupSecondTitle': '关注者可以看到我关注的个人资料',
    'settings_titleFooter': '更改密码',
    'login_title': '欢迎回来!',
    'login_subtitle': '请登录访问更多内容。',
    'login_emailTextField': '电子邮件地址',
    'login_passwordTextField': '密码',
    'login_forgotPassword': '忘记密码?',
    'login_contentButton': '登录',
    'login_footer': '从头开始?',
    'login_textButtonFooter': '在此处创建帐户',
  };
}
