class MyRecipeModel {
  String cookBookCurrentId;
  List<String> cookBookIds;
  String recipeId;
  String recipeName;
  int time;
  List<String> ingredients;
  String cover;
  List<String> galleries;
  List<String> tagName;
  String directionId;
  String addInfor;
  MyRecipeModel({
    required this.cookBookCurrentId,
    required this.cookBookIds,
    required this.recipeId,
    required this.recipeName,
    required this.time,
    required this.ingredients,
    required this.cover,
    required this.galleries,
    required this.tagName,
    required this.directionId,
    required this.addInfor,
  });
  factory MyRecipeModel.withInitial() => MyRecipeModel(
      recipeName: "",
      time: 0,
      ingredients: [],
      cover: "",
      galleries: [],
      tagName: [],
      recipeId: '',
      directionId: '',
      addInfor: '',
      cookBookIds: [],
      cookBookCurrentId: '');
  factory MyRecipeModel.fromMyCipeModel(MyRecipeModel myRecipeModelOther) =>
      MyRecipeModel(
          recipeName: myRecipeModelOther.recipeName,
          time: myRecipeModelOther.time,
          ingredients: List<String>.from(myRecipeModelOther.ingredients),
          cover: myRecipeModelOther.cover,
          galleries: List<String>.from(myRecipeModelOther.galleries),
          tagName: List<String>.from(myRecipeModelOther.tagName),
          recipeId: myRecipeModelOther.recipeId,
          directionId: myRecipeModelOther.directionId,
          addInfor: myRecipeModelOther.addInfor,
          cookBookIds: List<String>.from(myRecipeModelOther.cookBookIds),
          cookBookCurrentId: myRecipeModelOther.cookBookCurrentId);

  @override
  String toString() {
    return 'MyRecipeModel(cookBookCurrentId: $cookBookCurrentId, cookBookIds: $cookBookIds, recipeId: $recipeId, recipeName: $recipeName, time: $time, ingredients: $ingredients, cover: $cover, galleries: $galleries, tagName: $tagName, directionId: $directionId, addInfor: $addInfor)';
  }
}
