import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:mock_team3/data/base/base_service.dart';

import '../../base/data_result.dart';
import '../../base/service_state.dart';
import '../model/cook_book_model.dart';

class CookBookService extends BaseService {
  Future<DataResult<List<CookBookModel>>> getListCookbook(
      List<String> cookbookIds) async {
    try {
      List<CookBookModel> cookbooks = [];
      DocumentSnapshot documentSnapshot;
      for (int indexCookbook = 0;
          indexCookbook < cookbookIds.length;
          indexCookbook++) {
        documentSnapshot = await super
            .fireStore
            .doc("cookbook/${cookbookIds[indexCookbook]}")
            .get();
        if (documentSnapshot.exists) {
          cookbooks.add(CookBookModel.fromDocumentSnapshot(
              documentSnapshot: documentSnapshot));
        }
      }
      return DataResult.success(cookbooks);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }
}
