class CookBookUIModel{
  String id;
  String name;
  String imageUrl;

  CookBookUIModel({required this.id, required this.name, required this.imageUrl});

  @override
  String toString() {
    return 'CookBookUIModel{id: $id, name: $name, imageUrl: $imageUrl}';
  }
}