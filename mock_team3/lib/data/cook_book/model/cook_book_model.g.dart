// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cook_book_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CookBookModel _$CookBookModelFromJson(Map<String, dynamic> json) =>
    CookBookModel(
      recipes:
          (json['recipes'] as List<dynamic>?)?.map((e) => e as String).toList(),
      name: json['name'] as String,
    );

Map<String, dynamic> _$CookBookModelToJson(CookBookModel instance) =>
    <String, dynamic>{
      'recipes': instance.recipes,
      'name': instance.name,
    };
