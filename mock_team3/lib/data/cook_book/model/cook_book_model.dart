import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cook_book_model.g.dart';

@JsonSerializable()
class CookBookModel {
  List<String>? recipes;
  String name;

  CookBookModel({
    this.recipes,
    required this.name,
  });

  factory CookBookModel.fromJson(Map<String, dynamic> json) =>
      _$CookBookModelFromJson(json);

  factory CookBookModel.withInitial() => CookBookModel(name: "");

  Map<String, dynamic> toJson() => _$CookBookModelToJson(this);

  factory CookBookModel.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return CookBookModel.fromJson(data);
  }

  @override
  String toString() => 'CookBookModel(recipes: $recipes, name: $name)';
}
