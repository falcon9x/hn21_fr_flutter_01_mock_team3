import 'package:mock_team3/data/cook_book/service/cook_book_service.dart';

abstract class CookBookRepository {}

class CookBookRepositoryImpl implements CookBookRepository {
  final CookBookService cookBookService;
  CookBookRepositoryImpl({
    required this.cookBookService,
  });
}
