import 'package:json_annotation/json_annotation.dart';
part 'setting_model.g.dart';

@JsonSerializable()
class SettingModel {
  bool isNotifyMeForFollowers;
  bool isWhenSomeOneSendMeAMessage;
  bool isSomeOneLiveCook;
  bool isFollowersCanSeeMyRecipes;
  bool isFollowersCanSeeFollowings;

  SettingModel({
    required this.isNotifyMeForFollowers,
    required this.isWhenSomeOneSendMeAMessage,
    required this.isSomeOneLiveCook,
    required this.isFollowersCanSeeMyRecipes,
    required this.isFollowersCanSeeFollowings,
  });
  factory SettingModel.fromJson(Map<String, dynamic> json) =>
      _$SettingModelFromJson(json);
  Map<String, dynamic> toJson() => _$SettingModelToJson(this);

  factory SettingModel.withInitial() {
    return SettingModel(
        isFollowersCanSeeFollowings: true,
        isFollowersCanSeeMyRecipes: true,
        isNotifyMeForFollowers: true,
        isSomeOneLiveCook: true,
        isWhenSomeOneSendMeAMessage: true);
  }

  @override
  String toString() {
    return 'SettingModel{isNotifyMeForFollowers: $isNotifyMeForFollowers, isWhenSomeOneSendMeAMessage: $isWhenSomeOneSendMeAMessage, isSomeOneLiveCook: $isSomeOneLiveCook, isFollowersCanSeeMyRecipes: $isFollowersCanSeeMyRecipes, isFollowersCanSeeFollowings: $isFollowersCanSeeFollowings}';
  }
}
