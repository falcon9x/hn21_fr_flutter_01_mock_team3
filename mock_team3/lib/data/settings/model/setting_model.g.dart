// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingModel _$SettingModelFromJson(Map<String, dynamic> json) => SettingModel(
      isNotifyMeForFollowers: json['isNotifyMeForFollowers'] as bool,
      isWhenSomeOneSendMeAMessage: json['isWhenSomeOneSendMeAMessage'] as bool,
      isSomeOneLiveCook: json['isSomeOneLiveCook'] as bool,
      isFollowersCanSeeMyRecipes: json['isFollowersCanSeeMyRecipes'] as bool,
      isFollowersCanSeeFollowings: json['isFollowersCanSeeFollowings'] as bool,
    );

Map<String, dynamic> _$SettingModelToJson(SettingModel instance) =>
    <String, dynamic>{
      'isNotifyMeForFollowers': instance.isNotifyMeForFollowers,
      'isWhenSomeOneSendMeAMessage': instance.isWhenSomeOneSendMeAMessage,
      'isSomeOneLiveCook': instance.isSomeOneLiveCook,
      'isFollowersCanSeeMyRecipes': instance.isFollowersCanSeeMyRecipes,
      'isFollowersCanSeeFollowings': instance.isFollowersCanSeeFollowings,
    };
