import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/base/service_state.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_collection_name.dart';

import 'model/info_model.dart';

class InfoService extends BaseService {
  Future<DataResult<InfoModel>> getInfo(String infoId) async {
    try {
      DocumentSnapshot snapshot = await super.fireStore.doc(
          "${FirebaseCollectionName.info}/$infoId").get();
      if(snapshot.exists){
        InfoModel infoModel = InfoModel.fromDocumentSnapshot(documentSnapshot: snapshot);
        return Future.value(DataResult.success(infoModel));
      }
      return Future.value(DataResult.failure(APIFailure(invalidResponse, "data null")));
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }
}