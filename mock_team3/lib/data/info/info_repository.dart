import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/info/info_service.dart';
import 'package:mock_team3/data/info/model/info_model.dart';

abstract class InfoRepository{
  Future<DataResult<InfoModel>> getInfo(String infoId);
}

class InfoRepositoryImpl implements InfoRepository{
  final InfoService _infoService;

  InfoRepositoryImpl(this._infoService);

  @override
  Future<DataResult<InfoModel>> getInfo(String infoId) {
    return _infoService.getInfo(infoId);
  }
}