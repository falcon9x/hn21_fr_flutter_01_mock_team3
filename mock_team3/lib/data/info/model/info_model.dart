import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'info_model.g.dart';

@JsonSerializable()
class InfoModel {
  int time;
  List<String>? facts;
  List<String>? tags;
  InfoModel({
    required this.time,
    this.facts,
    this.tags,
  });

  factory InfoModel.fromJson(Map<String, dynamic> json) =>
      _$InfoModelFromJson(json);
  Map<String, dynamic> toJson() => _$InfoModelToJson(this);
  factory InfoModel.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return InfoModel.fromJson(data);
  }
  factory InfoModel.withInitial() => InfoModel(time: 0);

  factory InfoModel.fromInforOther(InfoModel inforOther) => InfoModel(
      time: inforOther.time, facts: inforOther.facts, tags: inforOther.tags);
  @override
  String toString() {
    return 'InfoModel{time: $time, facts: $facts, tags: $tags}';
  }
}
