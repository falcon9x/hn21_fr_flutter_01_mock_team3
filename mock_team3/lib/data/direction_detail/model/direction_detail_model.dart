import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'direction_detail_model.g.dart';

@JsonSerializable()
class DirectionDetailModel {
  String name;
  int timer;
  DirectionDetailModel({
    required this.name,
    required this.timer,
  });

  factory DirectionDetailModel.fromDirectionDetailModelOther(
          DirectionDetailModel other) =>
      DirectionDetailModel(name: other.name, timer: other.timer);

  factory DirectionDetailModel.fromJson(Map<String, dynamic> json) =>
      _$DirectionDetailModelFromJson(json);
  Map<String, dynamic> toJson() => _$DirectionDetailModelToJson(this);
  factory DirectionDetailModel.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return DirectionDetailModel.fromJson(data);
  }
  factory DirectionDetailModel.withInitial() =>
      DirectionDetailModel(name: "", timer: 0);

  @override
  String toString() => 'DirectionDetailModel(name: $name, timer: $timer)';
}
