// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'direction_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DirectionDetailModel _$DirectionDetailModelFromJson(
        Map<String, dynamic> json) =>
    DirectionDetailModel(
      name: json['name'] as String,
      timer: json['timer'] as int,
    );

Map<String, dynamic> _$DirectionDetailModelToJson(
        DirectionDetailModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'timer': instance.timer,
    };
