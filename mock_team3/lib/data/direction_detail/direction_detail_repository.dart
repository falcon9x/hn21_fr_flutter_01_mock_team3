import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/direction_detail/direction_detail_service.dart';
import 'package:mock_team3/data/direction_detail/model/direction_detail_model.dart';

abstract class DirectionDetailRepository {
  Future<DataResult<DirectionDetailModel>> getDirectionDetail(
      String directionDetailId);
}

class DirectionDetailRepositoryImpl implements DirectionDetailRepository {
  final DirectionDetailService _directionDetailService;

  DirectionDetailRepositoryImpl(this._directionDetailService);

  @override
  Future<DataResult<DirectionDetailModel>> getDirectionDetail(
      String directionDetailId) {
    return _directionDetailService.getDirectionDetail(directionDetailId);
  }
}
