import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/direction_detail/model/direction_detail_model.dart';

import '../base/data_result.dart';
import '../base/service_state.dart';
import '../firebase/firebase_service/firebase_collection_name.dart';

class DirectionDetailService extends BaseService{
  Future<DataResult<DirectionDetailModel>> getDirectionDetail(String directionDetailId) async {
    try {
      DocumentSnapshot snapshot = await super.fireStore.doc(
          "${FirebaseCollectionName.directionDetail}/$directionDetailId").get();
      if(snapshot.exists){
        DirectionDetailModel infoModel = DirectionDetailModel.fromDocumentSnapshot(documentSnapshot: snapshot);
        return Future.value(DataResult.success(infoModel));
      }
      return Future.value(DataResult.failure(APIFailure(invalidResponse, "data null")));
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }
}