// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'direction_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Direction _$DirectionFromJson(Map<String, dynamic> json) => Direction(
      video: json['video'] as String,
      directionDetails: (json['directionDetails'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$DirectionToJson(Direction instance) => <String, dynamic>{
      'video': instance.video,
      'directionDetails': instance.directionDetails,
    };
