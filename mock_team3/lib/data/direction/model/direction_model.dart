import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'direction_model.g.dart';

@JsonSerializable()
class Direction {
  String video;
  List<String> directionDetails;
  Direction({
    required this.video,
    required this.directionDetails,
  });

  factory Direction.fromJson(Map<String, dynamic> json) =>
      _$DirectionFromJson(json);
  Map<String, dynamic> toJson() => _$DirectionToJson(this);
  factory Direction.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return Direction.fromJson(data);
  }
  factory Direction.withInitial() => Direction(video: "", directionDetails: []);
}
