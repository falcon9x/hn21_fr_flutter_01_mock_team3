import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/direction/direction_service.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';

import 'model/direction_model.dart';

abstract class DirectionRepository {
  Future<DataResult<Direction>> getDirection(String directionId);

  Future<DataResult<String>> getVideoFromStorage(String videoUrl);
}

class DirectionRepositoryImpl implements DirectionRepository {
  final DirectionService _directionService;
  final FireBaseService _fireBaseService;

  DirectionRepositoryImpl(this._directionService, this._fireBaseService);

  @override
  Future<DataResult<Direction>> getDirection(String directionId) {
    return _directionService.getDirection(directionId);
  }

  @override
  Future<DataResult<String>> getVideoFromStorage(String videoUrl) {
    return _fireBaseService.getVideoFromStorage(videoUrl);
  }
}
