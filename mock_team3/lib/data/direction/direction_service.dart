import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/direction/model/direction_model.dart';

import '../base/data_result.dart';
import '../base/service_state.dart';
import '../firebase/firebase_service/firebase_collection_name.dart';

class DirectionService extends BaseService{
  Future<DataResult<Direction>> getDirection(String directionId) async {
    try {
      DocumentSnapshot snapshot = await super.fireStore.doc(
          "${FirebaseCollectionName.direction}/$directionId").get();
      if(snapshot.exists){
        Direction infoModel = Direction.fromDocumentSnapshot(documentSnapshot: snapshot);
        return Future.value(DataResult.success(infoModel));
      }
      return Future.value(DataResult.failure(APIFailure(invalidResponse, "data null")));
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }
}