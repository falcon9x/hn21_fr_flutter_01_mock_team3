import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';

import '../base/data_result.dart';
import '../cook_book/model/cook_book_model.dart';
import '../direction/model/direction_model.dart';
import '../direction_detail/model/direction_detail_model.dart';
import '../info/model/info_model.dart';
import '../recipe/model/recipe_model.dart';
import '../tag/model/tag_model.dart';
import '../user/model/user_model.dart';

abstract class FirebaseRepository {
  Future<String> getIdUser();

  Future<DataResult<DocumentSnapshot>> getDocumentSnapshot(String url);

  Future<DataResult<String>> getImageFromStorage(String imageUrl);

  Future<DataResult> uploadImageToStorage(String filePath);

  Future<void> reloadUserCurrent();

  Future<bool> checkEmailVeriry();
  Future<void> sendEmailVerify();
  Future<void> deleteUserAuth();
  Future<void> updateData(String url, Map<String, dynamic> newData);
  Future<void> addData(
      String collectionName, String id, Map<String, dynamic> newData);
  Future<DataResult<QuerySnapshot>> getQuerySnapshot(String nameCollection);
  Future<CookBookModel> getCookBook(String cookBookId);

  Future<RecipeModel> getRecipe(String recipeId);

  Future<Direction> getDirection(String directionId);

  Future<DirectionDetailModel> getDirectionDetail(
      String directionDetailModelId);

  Future<TagModel> getTag(String tagId);

  Future<UserModel> getUser(String userId);

  Future<InfoModel> getInfor(String inforId);
}

class FirebaseRepositoryImpl implements FirebaseRepository {
  final FireBaseService _fireBaseService;
  FirebaseRepositoryImpl(
    this._fireBaseService,
  );
  @override
  Future<bool> checkEmailVeriry() {
    return _fireBaseService.checkEmailVeriry();
  }

  @override
  Future<DataResult<DocumentSnapshot<Object?>>> getDocumentSnapshot(
      String url) {
    return _fireBaseService.getDocumentSnapshot(url);
  }

  @override
  Future<String> getIdUser() {
    return _fireBaseService.getIdUser();
  }

  @override
  Future<DataResult<String>> getImageFromStorage(String imageUrl) {
    return _fireBaseService.getImageFromStorage(imageUrl);
  }

  @override
  Future<void> reloadUserCurrent() {
    return _fireBaseService.reloadUserCurrent();
  }

  @override
  Future<DataResult> uploadImageToStorage(String filePath) {
    return _fireBaseService.uploadImageToStorage(filePath);
  }

  @override
  Future<void> sendEmailVerify() {
    return _fireBaseService.sendEmailVerify();
  }

  @override
  Future<void> deleteUserAuth() {
    return _fireBaseService.deleteUserAuth();
  }

  @override
  Future<void> updateData(String url, Map<String, dynamic> newData) {
    return _fireBaseService.updateData(url, newData);
  }

  @override
  Future<DataResult<QuerySnapshot<Object?>>> getQuerySnapshot(
      String nameCollection) {
    return _fireBaseService.getQuerySnapshot(nameCollection);
  }

  @override
  Future<CookBookModel> getCookBook(String cookBookId) {
    return _fireBaseService.getCookBook(cookBookId);
  }

  @override
  Future<Direction> getDirection(String directionId) {
    return _fireBaseService.getDirection(directionId);
  }

  @override
  Future<DirectionDetailModel> getDirectionDetail(
      String directionDetailModelId) {
    return _fireBaseService.getDirectionDetail(directionDetailModelId);
  }

  @override
  Future<InfoModel> getInfor(String inforId) {
    return _fireBaseService.getInfor(inforId);
  }

  @override
  Future<RecipeModel> getRecipe(String recipeId) {
    return _fireBaseService.getRecipe(recipeId);
  }

  @override
  Future<TagModel> getTag(String tagId) {
    return _fireBaseService.getTag(tagId);
  }

  @override
  Future<UserModel> getUser(String userId) {
    return _fireBaseService.getUser(userId);
  }

  @override
  Future<void> addData(
      String collectionName, String id, Map<String, dynamic> newData) {
    return _fireBaseService.addData(collectionName, id, newData);
  }
}
