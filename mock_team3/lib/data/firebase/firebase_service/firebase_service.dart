import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/direction_detail/model/direction_detail_model.dart';
import 'package:mock_team3/data/info/model/info_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:mock_team3/data/tag/model/tag_model.dart';
import 'package:path/path.dart';
import '../../base/service_state.dart';
import '../../cook_book/model/cook_book_model.dart';
import '../../direction/model/direction_model.dart';
import '../../user/model/user_model.dart';
import 'firebase_collection_name.dart';

class FireBaseService {
  FirebaseFirestore fireStore;
  FirebaseStorage firebaseStorage;
  FirebaseAuth firebaseAuth;

  FireBaseService(
      {required this.fireStore,
      required this.firebaseStorage,
      required this.firebaseAuth});

  Future<String> getIdUser() async {
    String uid = "";
    if (firebaseAuth.currentUser != null) {
      uid = firebaseAuth.currentUser!.uid;
    }
    return uid;
  }

  Future<DataResult<DocumentSnapshot>> getDocumentSnapshot(String url) async {
    try {
      DocumentSnapshot documentSnapshot = await fireStore.doc(url).get();
      if (documentSnapshot.exists) {
        return Future.value(DataResult.success(documentSnapshot));
      } else {
        throw Exception("Fileld is invalid");
      }
    } catch (e) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, e.toString())));
    }
  }

  Future<DataResult<QuerySnapshot>> getQuerySnapshot(
      String nameCollection) async {
    try {
      QuerySnapshot querySnapshot =
          await fireStore.collection(nameCollection).get();
      if (querySnapshot.docs.isNotEmpty) {
        return Future.value(DataResult.success(
            await fireStore.collection(nameCollection).get()));
      } else {
        throw Exception("This Collection is a empty collection");
      }
    } catch (e) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, e.toString())));
    }
  }

  Future<DataResult<String>> getImageFromStorage(String imageUrl) async {
    try {
      Reference ref = firebaseStorage.ref("image/$imageUrl");
      String downloadUrl = await ref.getDownloadURL();
      return DataResult.success(downloadUrl);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }

  Future<DataResult> uploadImageToStorage(String filePath) async {
    try {
      File file = File(filePath);
      String fileName = basename(file.path);
      await firebaseStorage.ref('image/$fileName').putFile(file);
      return DataResult.success(success);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }

  Future<void> reloadUserCurrent() async {
    if (await getIdUser() != "") {
      await firebaseAuth.currentUser!.reload();
    }
  }

  Future<bool> checkEmailVeriry() async {
    if (await getIdUser() != "") {
      return firebaseAuth.currentUser!.emailVerified;
    }
    return false;
  }

  Future<void> sendEmailVerify() async {
    await firebaseAuth.currentUser!.sendEmailVerification();
  }

  Future<void> deleteUserAuth() async {
    if (await getIdUser() != "") {
      firebaseAuth.currentUser!.delete();
    }
  }

  Future<void> deleteData(String url) async {
    fireStore.doc(url).delete();
  }

  Future<void> updateData(String url, Map<String, dynamic> newData) async {
    fireStore.doc(url).update(newData);
  }

  Future<void> addData(
      String collectionName, String id, Map<String, dynamic> newData) async {
    fireStore.collection(collectionName).doc(id).set(newData);
  }

  Future<CookBookModel> getCookBook(String cookBookId) async {
    DataResult dataResult = await getDocumentSnapshot(
        "/${FirebaseCollectionName.cookbook}/$cookBookId");
    if (dataResult.isSuccess && dataResult.data != null) {
      DocumentSnapshot documentSnapshot = dataResult.data;
      if (documentSnapshot.exists) {
        return CookBookModel.fromDocumentSnapshot(
            documentSnapshot: documentSnapshot);
      }
    }
    return CookBookModel.withInitial();
  }

  Future<RecipeModel> getRecipe(String recipeId) async {
    DataResult dataResult = await getDocumentSnapshot(
        "/${FirebaseCollectionName.recipe}/$recipeId");
    if (dataResult.isSuccess && dataResult.data != null) {
      DocumentSnapshot documentSnapshot = dataResult.data;
      if (documentSnapshot.exists) {
        return RecipeModel.fromDocumentSnapshot(
            documentSnapshot: documentSnapshot);
      }
    }
    return RecipeModel.withInitial();
  }

  Future<Direction> getDirection(String directionId) async {
    DataResult dataResult = await getDocumentSnapshot(
        "/${FirebaseCollectionName.direction}/$directionId");
    if (dataResult.isSuccess && dataResult.data != null) {
      DocumentSnapshot documentSnapshot = dataResult.data;
      if (documentSnapshot.exists) {
        return Direction.fromDocumentSnapshot(
            documentSnapshot: documentSnapshot);
      }
    }
    return Direction.withInitial();
  }

  Future<DirectionDetailModel> getDirectionDetail(
      String directionDetailModelId) async {
    DataResult dataResult = await getDocumentSnapshot(
        "/${FirebaseCollectionName.directionDetail}/$directionDetailModelId");
    if (dataResult.isSuccess && dataResult.data != null) {
      DocumentSnapshot documentSnapshot = dataResult.data;
      if (documentSnapshot.exists) {
        return DirectionDetailModel.fromDocumentSnapshot(
            documentSnapshot: documentSnapshot);
      }
    }
    return DirectionDetailModel.withInitial();
  }

  Future<TagModel> getTag(String tagId) async {
    DataResult dataResult =
        await getDocumentSnapshot("/${FirebaseCollectionName.tag}/$tagId");
    if (dataResult.isSuccess && dataResult.data != null) {
      DocumentSnapshot documentSnapshot = dataResult.data;
      if (documentSnapshot.exists) {
        return TagModel.fromDocumentSnapshot(
            documentSnapshot: documentSnapshot);
      }
    }
    return TagModel.withInitial();
  }

  Future<UserModel> getUser(String userId) async {
    DataResult dataResult =
        await getDocumentSnapshot("/${FirebaseCollectionName.user}/$userId");
    if (dataResult.isSuccess && dataResult.data != null) {
      DocumentSnapshot documentSnapshot = dataResult.data;
      if (documentSnapshot.exists) {
        return UserModel.fromDocumentSnapshot(
            documentSnapshot: documentSnapshot);
      }
    }
    return UserModel.withInitial();
  }

  Future<InfoModel> getInfor(String inforId) async {
    DataResult dataResult =
        await getDocumentSnapshot("/${FirebaseCollectionName.info}/$inforId");
    if (dataResult.isSuccess && dataResult.data != null) {
      DocumentSnapshot documentSnapshot = dataResult.data;
      if (documentSnapshot.exists) {
        return InfoModel.fromDocumentSnapshot(
            documentSnapshot: documentSnapshot);
      }
    }
    return InfoModel.withInitial();
  }

  Future<DataResult<String>> getVideoFromStorage(String videoUrl) async {
    try {
      Reference ref = firebaseStorage.ref("video/$videoUrl");
      String downloadUrl = await ref.getDownloadURL();
      return DataResult.success(downloadUrl);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }
}
