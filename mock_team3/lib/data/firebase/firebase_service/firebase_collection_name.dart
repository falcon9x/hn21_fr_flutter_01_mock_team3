class FirebaseCollectionName {
  static String cookbook = "cookbook";
  static String direction = "direction";
  static String directionDetail = "directionDetail";
  static String info = "info";
  static String recipe = "recipe";
  static String tag = "tag";
  static String user = "user";
}
