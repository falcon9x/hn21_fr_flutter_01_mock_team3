class RecipeUIModel {
  String name;
  int time;
  String videoUrl;
  List<String> listDirectionDetail;
  List<int> listTimeDirectionDetail;

  RecipeUIModel({required this.name,
    required this.time,
    required this.videoUrl,
    required this.listDirectionDetail,
    required this.listTimeDirectionDetail});

  factory RecipeUIModel.withInitial(){
    return RecipeUIModel(name: "",
        time: 0,
        videoUrl: "",
        listDirectionDetail: [],
        listTimeDirectionDetail: []);
  }

  @override
  String toString() {
    return 'RecipeUIModel{name: $name, time: $time, videoUrl: $videoUrl, listDirectionDetail: $listDirectionDetail, listTimeDirectionDetail: $listTimeDirectionDetail}';
  }
}