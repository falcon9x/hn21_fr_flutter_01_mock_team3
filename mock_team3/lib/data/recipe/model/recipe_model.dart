import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'recipe_model.g.dart';

@JsonSerializable()
class RecipeModel {
  String name;
  List<String>? galleries;
  List<String>? ingredients;
  String? additionalInfo;
  String? direction;
  int likes;
  int comments;
  int indexCover;
  int? ingredientCount;

  RecipeModel(
      {required this.name,
      this.galleries,
      this.ingredients,
      this.additionalInfo,
      this.direction,
      required this.likes,
      required this.comments,
      required this.indexCover,
      this.ingredientCount});
  factory RecipeModel.fromJson(Map<String, dynamic> json) =>
      _$RecipeModelFromJson(json);
  Map<String, dynamic> toJson() => _$RecipeModelToJson(this);

  factory RecipeModel.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return RecipeModel.fromJson(data);
  }

  factory RecipeModel.withInitial(){
    return RecipeModel(name: "", likes: 0, comments: 0, indexCover: 0);
  }

  @override
  String toString() {
    return 'RecipeModel{name: $name, galleries: $galleries, ingredients: $ingredients, likes: $likes, comments: $comments, indexCover: $indexCover}';
  }
}
