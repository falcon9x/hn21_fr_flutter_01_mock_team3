import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_service.dart';

import '../../base/data_result.dart';

abstract class RecipeRepository {
  Future<DataResult<RecipeModel>> getRecipeById(String recipeId);
}

class RecipeRepositoryImpl implements RecipeRepository {
  final RecipeService _recipeService;
  RecipeRepositoryImpl(this._recipeService);

  @override
  Future<DataResult<RecipeModel>> getRecipeById(String recipeId) {
    return _recipeService.getRecipeById(recipeId);
  }
}
