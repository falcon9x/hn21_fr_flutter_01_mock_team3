// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipe_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecipeModel _$RecipeModelFromJson(Map<String, dynamic> json) => RecipeModel(
      name: json['name'] as String,
      galleries: (json['galleries'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      ingredients: (json['ingredients'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      additionalInfo: json['additionalInfo'] as String?,
      direction: json['direction'] as String?,
      likes: json['likes'] as int,
      comments: json['comments'] as int,
      indexCover: json['indexCover'] as int,
      ingredientCount: json['ingredientCount'] as int?,
    );

Map<String, dynamic> _$RecipeModelToJson(RecipeModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'galleries': instance.galleries,
      'ingredients': instance.ingredients,
      'additionalInfo': instance.additionalInfo,
      'direction': instance.direction,
      'likes': instance.likes,
      'comments': instance.comments,
      'indexCover': instance.indexCover,
      'ingredientCount': instance.ingredientCount,
    };
