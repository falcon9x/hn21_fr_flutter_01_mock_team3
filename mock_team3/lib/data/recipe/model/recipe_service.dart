import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_collection_name.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';

import '../../base/data_result.dart';
import '../../base/service_state.dart';

class RecipeService extends BaseService {
  Future<DataResult<RecipeModel>> getRecipe() async {
    RecipeModel? recipe;

    try {
      QuerySnapshot querySnapshot = await super
          .fireStore
          .collection(
          "/users/najbv1vf1BGNRelQC1Ob/cookbooks/1646706398/recipes/1646706415/recipe_child/")
          .get();

      if (querySnapshot.docs.isNotEmpty) {
        for (var doc in querySnapshot.docs.toList()) {
          log("========${doc.toString()}");

          recipe = RecipeModel.fromDocumentSnapshot(documentSnapshot: doc);

          print(recipe);
          return Future.value(DataResult.success(recipe));
        }
      }
    } catch (ex) {
      log(ex.toString());
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<RecipeModel>> getRecipeById(String recipeId) async {
    try {
      DocumentSnapshot snapshot = await super.fireStore.doc(
          "${FirebaseCollectionName.recipe}/$recipeId").get();
      if(snapshot.exists){
        RecipeModel recipeModel = RecipeModel.fromDocumentSnapshot(documentSnapshot: snapshot);
        return Future.value(DataResult.success(recipeModel));
      }
    } catch (ex) {
      return Future.value(DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }
}
