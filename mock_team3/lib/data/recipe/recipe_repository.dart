import 'package:mock_team3/data/recipe/service/recipe_service.dart';

import '../base/data_result.dart';
import '../cook_book/model/cook_book_model.dart';
import '../user/model/user_model.dart';
import 'model/recipe_model.dart';

abstract class RecipeRepository {
  Future<DataResult<List<RecipeModel>>> getRecipes();

  Future<DataResult<String>> getImageFromStorage(String imageUrl);

  Future<void> addRecipe(RecipeModel recipeChild);

  Future<DataResult<List<UserModel>>> getUser();

  Future<void> addCookBook(CookBookModel cookBookNew);

  Future<DataResult<UserModel>> getUserByID(String userID);

  Future<DataResult<CookBookModel>> getCookBookNameByID(String cookBookID);

  Future<DataResult<RecipeModel>> getRecipeByID(String recipeID);
}

class RecipeRepositoryImpl implements RecipeRepository {
  final RecipeService _recipeService;

  RecipeRepositoryImpl(this._recipeService);

  @override
  Future<DataResult<List<RecipeModel>>> getRecipes() {
    return _recipeService.getRecipes();
  }

  @override
  Future<DataResult<String>> getImageFromStorage(String imageUrl) {
    return _recipeService.getImageFromStorage(imageUrl);
  }

  @override
  Future<void> addRecipe(RecipeModel recipeChild) async {
    await _recipeService.addRecipe(recipeChild);
  }

  @override
  Future<DataResult<List<UserModel>>> getUser() {
    return _recipeService.getUser();
  }

  @override
  Future<void> addCookBook(CookBookModel cookBookNew) {
    return _recipeService.addCookBook(cookBookNew);
  }

  @override
  Future<DataResult<UserModel>> getUserByID(String userID) {
    return _recipeService.getUserByID(userID);
  }

  @override
  Future<DataResult<CookBookModel>> getCookBookNameByID(String cookBookID) {
    return _recipeService.getCookBookNameByID(cookBookID);
  }

  @override
  Future<DataResult<RecipeModel>> getRecipeByID(String recipeID) {
    // TODO: implement getRecipeByID
    return _recipeService.getRecipeByID(recipeID);
  }
}
