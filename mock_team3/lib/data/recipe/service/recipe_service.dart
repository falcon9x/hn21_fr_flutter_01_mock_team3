import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/cook_book/model/cook_book_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';

import '../../base/data_result.dart';
import '../../base/service_state.dart';
import '../../user/model/user_model.dart';

class RecipeService extends BaseService {
  Future<DataResult<List<RecipeModel>>> getRecipes() async {
    try {
      QuerySnapshot querySnapshot = await fireStore.collection("recipe").get();

      if (querySnapshot.docs.isNotEmpty) {
        List<RecipeModel> recipes = [];
        for (var doc in querySnapshot.docs.toList()) {
          print("========${doc.toString()}");

          recipes.add(RecipeModel.fromDocumentSnapshot(documentSnapshot: doc));

          return Future.value(DataResult.success(recipes));
        }
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<String>> getImageFromStorage(String imageUrl) async {
    try {
      Reference ref = firebaseStorage.ref("image/$imageUrl");
      String downloadUrl = await ref.getDownloadURL();
      return DataResult.success(downloadUrl);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }

  Future<void> addRecipe(RecipeModel recipeChild) async {
    try {
      CollectionReference recipe = fireStore.collection("recipe");
      recipe.add(recipeChild.toJson());
    } catch (ex) {}
  }

  Future<void> addCookBook(CookBookModel cookBookNew) async {
    try {
      CollectionReference cookBook = fireStore.collection("cookbook");
      cookBook.add(cookBookNew.toJson());
    } catch (ex) {
      print(ex.toString());
    }
  }

  Future<DataResult<List<UserModel>>> getUser() async {
    List<UserModel> users = [];
    try {
      QuerySnapshot querySnapshot = await fireStore.collection("user").get();
      if (querySnapshot.docs.isNotEmpty) {
        for (var doc in querySnapshot.docs.toList()) {
          UserModel user =
              UserModel.fromDocumentSnapshot(documentSnapshot: doc);
          users.add(user);
        }
      }

      return Future.value(DataResult.success(users));
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }

  Future<DataResult<UserModel>> getUserByID(String userID) async {
    try {
      UserModel? user;
      DocumentSnapshot querySnapshot =
          await fireStore.collection("user").doc(userID).get();
      if (querySnapshot.exists) {
        user = UserModel.fromDocumentSnapshot(documentSnapshot: querySnapshot);
      }
      return Future.value(DataResult.success(user!));
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }

  Future<DataResult<CookBookModel>> getCookBookNameByID(
      String cookBookID) async {
    try {
      CookBookModel? cookBookModel;

      DocumentSnapshot querySnapshot =
          await fireStore.collection("cookbook").doc(cookBookID).get();

      if (querySnapshot.exists) {
        cookBookModel =
            CookBookModel.fromDocumentSnapshot(documentSnapshot: querySnapshot);
      }
      return Future.value(DataResult.success(cookBookModel!));
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }

  Future<DataResult<RecipeModel>> getRecipeByID(String recipeID) async {
    try {
      RecipeModel? recipeModel;

      DocumentSnapshot querySnapshot = await fireStore.collection("recipe").doc(recipeID).get();

      if (querySnapshot.exists) {
        recipeModel =
            RecipeModel.fromDocumentSnapshot(documentSnapshot: querySnapshot);
      }
      return Future.value(DataResult.success(recipeModel!));
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }
}
