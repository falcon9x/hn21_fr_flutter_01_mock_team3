import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/settings/model/setting_model.dart';
import 'package:mock_team3/data/user/model/user_model.dart';
import '../base/service_state.dart';

class UserService extends BaseService {
  Future<DataResult<UserModel>> getUserModel(String idUserCurrent) async {
    UserModel? userModel;
    try {
      DocumentSnapshot documentSnapshot =
          await super.fireStore.doc("user/$idUserCurrent").get();

      if (documentSnapshot.exists) {
        userModel =
            UserModel.fromDocumentSnapshot(documentSnapshot: documentSnapshot);
        log("SUCCESS ${userModel.toString()}");
        return Future.value(DataResult.success(userModel));
      }
    } catch (e) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, e.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<int>> updateUserProfile(
      UserModel user, String idUser) async {
    try {
      log("US:37 ${user.toJson()}");
      await super.fireStore.doc("user/$idUser").update(user.toJson());

      // await uploadImageToStorage(user.avatar ?? "");
      return DataResult.success(success);
    } catch (ex) {
      return DataResult.failure(APIFailure(invalidResponse, ex.toString()));
    }
  }

  Future<DataResult<String>> getImageFromStorage(String imageUrl) async {
    try {
      Reference ref = super.firebaseStorage.ref("image/$imageUrl");
      String downloadUrl = await ref.getDownloadURL();
      return DataResult.success(downloadUrl);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }

  Future<DataResult> uploadImageToStorage(String filePath) async {
    try {
      File file = File(filePath);
      String imagePath = DateTime.now().millisecondsSinceEpoch.toString();
      await super.firebaseStorage.ref('image/$imagePath.jpg').putFile(file);
      return DataResult.success("$imagePath.jpg");
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }

  Future<DataResult> updateUserSettings(
      String idUser, SettingModel settingModel) async {
    try {
      await super
          .fireStore
          .doc("user/$idUser")
          .update({'settings': settingModel.toJson()});
      return DataResult.success(success);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }

  Future<DataResult<List<UserModel>>> getListUserFollow(
      List<String> userIds) async {
    try {
      List<UserModel> list = [];
      for (int indexUserId = 0; indexUserId < userIds.length; indexUserId++) {
        DataResult result = await getUserModel(userIds[indexUserId]);
        UserModel userModel = result.data;
        list.add(userModel);
      }
      return Future.value(DataResult.success(list));
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
  }
}
