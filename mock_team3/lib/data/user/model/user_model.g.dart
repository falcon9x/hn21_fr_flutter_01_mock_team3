// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      account: AccountModel.fromJson(json['account'] as Map<String, dynamic>),
      avatar: json['avatar'] as String?,
      bio: json['bio'] as String?,
      comment: json['comment'] as int?,
      saved:
          (json['saved'] as List<dynamic>?)?.map((e) => e as String).toList(),
      following: (json['following'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      cookbooks: (json['cookbooks'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      likes: json['likes'] as int?,
      phone: json['phone'] as String?,
      settings: SettingModel.fromJson(json['settings'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'account': instance.account.toJson(),
      'avatar': instance.avatar,
      'bio': instance.bio,
      'comment': instance.comment,
      'saved': instance.saved,
      'following': instance.following,
      'cookbooks': instance.cookbooks,
      'likes': instance.likes,
      'phone': instance.phone,
      'settings': instance.settings.toJson(),
    };
