import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

import 'package:mock_team3/data/user/dto/user_ui_model.dart';

import '../../account/model/account_model.dart';
import '../../settings/model/setting_model.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  AccountModel account;
  String? avatar;
  String? bio;
  int? comment;
  List<String>? saved;
  List<String>? following;
  List<String>? cookbooks;
  int? likes;
  String? phone;
  SettingModel settings;

  UserModel({
    required this.account,
    this.avatar,
    this.bio,
    this.comment,
    this.saved,
    this.following,
    this.cookbooks,
    this.likes,
    this.phone,
    required this.settings,
  });

  UserUIModel toUserUIModel() {
    return UserUIModel(
        account: account,
        bio: bio,
        phone: phone,
        avatar: avatar,
        likes: likes,
        recipes: cookbooks?.length,
        listSaved: saved?.length,
        following: following?.length,
        settingModel: settings);
  }

  factory UserModel.withInitial(){
    return UserModel(account: AccountModel.withInitial(), settings: SettingModel.withInitial());
  }

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  factory UserModel.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return UserModel.fromJson(data);
  }
  @override
  String toString() {
    return 'UserModel(account: $account, avatar: $avatar, bio: $bio, comment: $comment, saved: $saved, following: $following, cookbooks: $cookbooks, likes: $likes, phone: $phone, settings: $settings)';
  }
}
