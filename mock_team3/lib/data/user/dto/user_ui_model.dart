import 'package:mock_team3/data/settings/model/setting_model.dart';

import '../../account/model/account_model.dart';

class UserUIModel {
  AccountModel? account;
  String? bio;
  String? phone;
  String? avatar;
  int? likes;

  // int following;
  //TODO
  //change to Recipe class
  int? recipes;
  int? listSaved;
  int? following;
  SettingModel settingModel;

  UserUIModel({required this.account,
    required this.bio,
    required this.phone,
    required this.avatar,
    required this.likes,
    required this.recipes,
    required this.listSaved,
    required this.following,
    required this.settingModel});

  factory UserUIModel.initial(){
    return UserUIModel(account: AccountModel.withInitial(),
        bio: "",
        phone: "",
        avatar: "",
        likes: 0,
        recipes: 0,
        listSaved: 0,
        following: 0,
        settingModel : SettingModel.withInitial());
  }

// UserUIModel.name(this.account, this.bio, this.phone, this.avatar, this.likes,
//     this.recipes, this.listSaved, this.following);

}
