class UserFollowUIModel{
  String uid;
  String name;
  String avatar;

  UserFollowUIModel({required this.uid, required this.name, required this.avatar});
}