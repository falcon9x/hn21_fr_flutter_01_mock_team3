import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/cook_book/model/cook_book_model.dart';
import 'package:mock_team3/data/cook_book/service/cook_book_service.dart';
import 'package:mock_team3/data/recipe/model/recipe_model.dart';
import 'package:mock_team3/data/recipe/model/recipe_service.dart';
import 'package:mock_team3/data/settings/model/setting_model.dart';
import 'package:mock_team3/data/user/model/user_model.dart';
import 'package:mock_team3/data/user/user_service.dart';

abstract class UserRepository {
  Future<DataResult<UserModel>> getUserProfile(String idUserCurrent);

  Future<DataResult<int>>  updateUserProfile(UserModel user,String idUser);

  Future<DataResult<String>> getImageFromStorage(String imageUrl);

  Future<DataResult<List<CookBookModel>>> getListCookbook(List<String> cookbookIds);

  Future<DataResult<RecipeModel>> getRecipeById(String recipeId);

  Future<DataResult> uploadImageToFirebase(String imagePath);

  Future<DataResult> updateUserSettings(String idUser,SettingModel settingModel);

  Future<DataResult<List<UserModel>>> getListUserFollow(List<String> userIds);
}

class UserRepositoryImpl implements UserRepository {
  final UserService _userService;
  final CookBookService _cookBookService;
  final RecipeService _recipeService;

  UserRepositoryImpl(this._userService, this._cookBookService, this._recipeService);

  @override
  Future<DataResult<UserModel>> getUserProfile(String idUserCurrent) {
    return _userService.getUserModel(idUserCurrent);
  }

  @override
  Future<DataResult<int>> updateUserProfile(UserModel user,String idUser) {
    return _userService.updateUserProfile(user,idUser);
  }

  @override
  Future<DataResult<String>> getImageFromStorage(String imageUrl) {
    return _userService.getImageFromStorage(imageUrl);
  }

  @override
  Future<DataResult<List<CookBookModel>>> getListCookbook(List<String> cookbookIds) {
    return _cookBookService.getListCookbook(cookbookIds);
  }

  @override
  Future<DataResult<RecipeModel>> getRecipeById(String recipeId) {
    return _recipeService.getRecipeById(recipeId);
  }

  @override
  Future<DataResult> uploadImageToFirebase(String imagePath) {
    return _userService.uploadImageToStorage(imagePath);
  }

  @override
  Future<DataResult> updateUserSettings(String idUser,SettingModel settingModel) {
    return _userService.updateUserSettings(idUser,settingModel);
  }

  @override
  Future<DataResult<List<UserModel>>> getListUserFollow(List<String> userIds) {
    return _userService.getListUserFollow(userIds);
  }
}
