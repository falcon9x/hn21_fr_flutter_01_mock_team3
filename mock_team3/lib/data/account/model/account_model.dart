import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'account_model.g.dart';

@JsonSerializable()
class AccountModel {
  String email;
  String password;
  String fullname;

  AccountModel({
    required this.email,
    required this.password,
    required this.fullname,
  });

  factory AccountModel.withInitial() {
    return AccountModel(email: "", password: "", fullname: "");
  }

  factory AccountModel.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return AccountModel.fromJson(data);
  }

  factory AccountModel.fromJson(Map<String, dynamic> json) =>
      _$AccountModelFromJson(json);
  Map<String, dynamic> toJson() => _$AccountModelToJson(this);

  @override
  String toString() =>
      'AccountModel(email: $email, password: $password, fullname: $fullname)';
}
