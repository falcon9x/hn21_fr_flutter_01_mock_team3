import 'dart:developer';

enum AuthResultStatus {
  successful,
  emailAlreadyExists,
  wrongPassword,
  invalidEmail,
  userNotFound,
  networkRequestFailed,
  operationNotAllowed,
  tooManyRequests,
  undefined,
}

class AccountResult {
  static handleException(e) {
    log(e.code);
    AuthResultStatus status;
    switch (e.code) {
      case "invalid-email":
        status = AuthResultStatus.invalidEmail;
        break;
      case "wrong-password":
        status = AuthResultStatus.wrongPassword;
        break;
      case "network-request-failed":
        status = AuthResultStatus.networkRequestFailed;
        break;
      case "user-not-found":
        status = AuthResultStatus.userNotFound;
        break;
      case "email-already-in-use":
        status = AuthResultStatus.emailAlreadyExists;
        break;
      default:
        status = AuthResultStatus.undefined;
    }
    return status;
  }

  static String generateExceptionMessage(exceptionCode) {
    String errorMessage;
    switch (exceptionCode) {
      case AuthResultStatus.invalidEmail:
        errorMessage = "Your email address appears to be malformed.";
        break;
      case AuthResultStatus.wrongPassword:
        errorMessage = "Password is wrong";
        break;
      case AuthResultStatus.networkRequestFailed:
        errorMessage = "No Internet";
        break;
      case AuthResultStatus.userNotFound:
        errorMessage = "User isn't exists";
        break;
      case AuthResultStatus.emailAlreadyExists:
        errorMessage = "Email is already exists";
        break;
      default:
        errorMessage = "An undefined Error happened.";
    }

    return errorMessage;
  }
}
