import 'dart:async';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mock_team3/data/account/service/account_result.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/settings/model/setting_model.dart';
import '../../user/model/user_model.dart';
import '../model/account_model.dart';

class AccountService extends BaseService {
  Future<AuthResultStatus>? createUserAuth(
      String email, String password) async {
    try {
      await firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);

      return AuthResultStatus.successful;
    } on FirebaseException catch (e) {
      return AccountResult.handleException(e);
    } catch (e) {
      return AuthResultStatus.undefined;
    }
  }

  Future<AuthResultStatus>? createAccount(
      String fullName, String password, String email) async {
    try {
      UserModel userModel = UserModel(
          account: AccountModel.withInitial(),
          settings: SettingModel.withInitial());
      DataResult dataResult = await getUserModelDefault();
      if (dataResult.isSuccess) {
        userModel = dataResult.data;
      }
      AccountModel accountNew = AccountModel(
        email: email,
        fullname: fullName,
        password: password,
      );
      userModel.account = accountNew;
      log(userModel.toString());
      User? user = FirebaseAuth.instance.currentUser;
      await FirebaseFirestore.instance
          .collection("user")
          .doc(user!.uid)
          .set(userModel.toJson());
      return AuthResultStatus.successful;
    } on FirebaseException catch (e) {
      log(e.toString());
      return AccountResult.handleException(e);
    } catch (e) {
      log(e.toString());
      return AuthResultStatus.undefined;
    }
  }

  Future<AuthResultStatus>? signIn(String email, String password) async {
    try {
      await firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      return AuthResultStatus.successful;
    } on FirebaseException catch (e) {
      return AccountResult.handleException(e);
    } catch (e) {
      return AuthResultStatus.undefined;
    }
  }

  Future<AccountModel> getUser(String email) async {
    try {
      final QuerySnapshot snap = await FirebaseFirestore.instance
          .collection('accounts')
          .where('email', isEqualTo: email)
          .get();

      return AccountModel.fromDocumentSnapshot(documentSnapshot: snap.docs[0]);
    } catch (e) {
      return AccountModel.withInitial();
    }
  }

  Future<void> signOut() async {
    return await firebaseAuth.signOut();
  }
}
