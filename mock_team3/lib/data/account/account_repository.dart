import 'package:mock_team3/data/account/service/account_service.dart';

import 'model/account_model.dart';
import 'service/account_result.dart';

abstract class AccountRepository {
  Future<AuthResultStatus>? createUserAuth(String email, String password);

  Future<AuthResultStatus>? signIn(String email, String password);

  Future<void> signOut();
  Future<AuthResultStatus>? createAccount(
      String fullName, String password, String email);

  Future<AccountModel> getUser(String email);
}

class AccountRepositoryImpl implements AccountRepository {
  final AccountService _accountService;
  AccountRepositoryImpl(
    this._accountService,
  );

  @override
  Future<AuthResultStatus>? createUserAuth(String email, String password) {
    return _accountService.createUserAuth(email, password);
  }

  @override
  Future<AccountModel> getUser(String email) {
    return _accountService.getUser(email);
  }

  @override
  Future<AuthResultStatus>? signIn(String email, String password) {
    return _accountService.signIn(email, password);
  }

  @override
  Future<void> signOut() {
    return _accountService.signOut();
  }

  @override
  Future<AuthResultStatus>? createAccount(
      String fullName, String password, String email) {
    return _accountService.createAccount(fullName, password, email);
  }
}
