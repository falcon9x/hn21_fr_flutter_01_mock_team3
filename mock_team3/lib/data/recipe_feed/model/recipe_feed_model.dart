class RecipeFeedModel {
  String _nameUser;
  String _linkImageUser;
  String _linkImageRecipe;
  String _nameRecipe;
  int _like;
  int _comment;


  RecipeFeedModel(this._nameUser, this._linkImageUser, this._linkImageRecipe,
      this._nameRecipe, this._like, this._comment);



  int get comment => _comment;

  set comment(int value) {
    _comment = value;
  }

  int get like => _like;

  set like(int value) {
    _like = value;
  }

  String get nameRecipe => _nameRecipe;

  set nameRecipe(String value) {
    _nameRecipe = value;
  }

  String get linkImageRecipe => _linkImageRecipe;

  set linkImageRecipe(String value) {
    _linkImageRecipe = value;
  }

  String get linkImageUser => _linkImageUser;

  set linkImageUser(String value) {
    _linkImageUser = value;
  }

  String get nameUser => _nameUser;

  set nameUser(String value) {
    _nameUser = value;
  }

  @override
  String toString() {
    return 'RecipeFeedModel{_nameUser: $_nameUser, _linkImageUser: $_linkImageUser, _linkImageRecipe: $_linkImageRecipe, _nameRecipe: $_nameRecipe, _like: $_like, _comment: $_comment}';
  }
}