class CookBook {
  String _name;
  int _index;

  CookBook(this._name, this._index);

  get index => _index;

  set index(value) {
    _index = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "$_name $_index";
  }
}
