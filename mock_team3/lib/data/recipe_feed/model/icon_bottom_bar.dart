class IconBottomBar {
  int _index;

  String _linkAsset;

  IconBottomBar(this._linkAsset, this._index);

  String get linkAsset => _linkAsset;

  set linkAsset(String value) {
    _linkAsset = value;
  }

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "$index $_linkAsset";
  }
}
