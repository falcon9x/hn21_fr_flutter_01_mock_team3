abstract class ListApi {
  String getUser();

  String getPersonals();

  String getCookbooks();

  String getSaved();

  String getFollowing();

  String getRecipes();

  String getDirection();

  String getInfo();

  String getDirectionDetail();

  String addId(String id);
}

class ListApiImpl implements ListApi {
  @override
  String getCookbooks() => "/cookbook";

  @override
  String getFollowing() => "/following";

  @override
  String getPersonals() => "/personals";

  @override
  String getSaved() => "/saved";

  @override
  String getUser() => "/user";

  @override
  String getDirection() => "/direction";

  @override
  String getDirectionDetail() => "/directionDetail";

  @override
  String getInfo() => "/info";

  @override
  String getRecipes() => "/recipres";

  @override
  String addId(String id) => "/$id";
}
