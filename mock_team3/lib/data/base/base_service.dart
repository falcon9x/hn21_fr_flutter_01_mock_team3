// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

import '../user/model/user_model.dart';
import 'data_result.dart';
import 'list_api.dart';
import 'service_state.dart';

const timeOutMess = "Time out";
const timeOutDuration = Duration(seconds: 3);

abstract class BaseService extends GetConnect {
  final FirebaseFirestore fireStore = FirebaseFirestore.instance;
  final FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  final ListApi? listApi = ListApiImpl();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  // Get User ID From FireStore
  Future<String> getIdUser() async {
    String uid = "";
    if (firebaseAuth.currentUser != null) {
      uid = firebaseAuth.currentUser!.uid;
    }
    return uid;
  }

  Future<Response> httpGet(String url, {Map<String, String>? headers}) {
    print("BaseService $url");
    return get(url, headers: headers);
  }

  Future<DataResult<UserModel>> getUserModelDefault() async {
    UserModel? userModel;
    try {
      DocumentSnapshot documentSnapshot =
          await fireStore.doc("/user/L8Z1HtHTLaAG5KE6Ectw").get();

      if (documentSnapshot.exists) {
        userModel =
            UserModel.fromDocumentSnapshot(documentSnapshot: documentSnapshot);
        return Future.value(DataResult.success(userModel));
      }
    } catch (e) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, e.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  getError(exception) {
    print("BaseService $exception");
    if (exception is HttpException) {
      return DataResult.failure(
          APIFailure(noInternet, 'No Internet Connection'));
    } else if (exception is TimeoutException) {
      return DataResult.failure(APIFailure(timeOut, 'Time out'));
    } else if (exception is SocketException) {
      return DataResult.failure(
          APIFailure(noInternet, 'No Internet Connection'));
    } else if (exception is FormatException) {
      return DataResult.failure(APIFailure(invalidFormat, 'Invalid Format'));
    } else {
      return DataResult.failure(exception is Failure
          ? exception
          : APIFailure(unknownError, "Unknown error"));
    }
  }
}
