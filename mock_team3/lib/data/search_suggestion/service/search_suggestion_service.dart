import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:mock_team3/data/account/model/account_model.dart';
import 'package:mock_team3/data/base/base_service.dart';
import 'package:mock_team3/data/settings/model/setting_model.dart';
import 'package:mock_team3/data/tag/model/tag_model.dart';
import 'package:mock_team3/data/user/model/user_model.dart';

import '../../base/data_result.dart';
import '../../base/service_state.dart';
import '../../recipe/model/recipe_model.dart';

class SearchSuggestionService extends BaseService {
  final int _numberOfLikeTrending = 5000;

  Future<DataResult<List<RecipeModel>>> getRecipesTrending() async {
    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("recipe")
          .where("likes", isGreaterThan: _numberOfLikeTrending)
          .get();
      if (querySnapshot.docs.isNotEmpty) {
        List<RecipeModel> recipes = [];
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          RecipeModel recipe =
              RecipeModel(name: "", likes: 0, comments: 0, indexCover: 0);
          recipe = RecipeModel.fromDocumentSnapshot(
              documentSnapshot: querySnapshot.docs[i]);
          recipes.add(recipe);
        }
        return Future.value(DataResult.success(recipes));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<String>> getImageFromStorage(String imageUrl) async {
    try {
      Reference ref = firebaseStorage.ref("image/$imageUrl");
      String downloadUrl = await ref.getDownloadURL();
      return DataResult.success(downloadUrl);
    } catch (ex) {
      return DataResult.failure(APIFailure(unknownError, ex.toString()));
    }
  }

  Future<DataResult<List<RecipeModel>>> getRecipesByIngredient(
      String ingredientName) async {
    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("recipe")
          .where("ingredients", arrayContainsAny: [ingredientName]).get();
      if (querySnapshot.docs.isNotEmpty) {
        List<RecipeModel> recipes = [];

        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          RecipeModel recipe =
              RecipeModel(name: "", likes: 0, comments: 0, indexCover: 0);
          recipe = RecipeModel.fromDocumentSnapshot(
              documentSnapshot: querySnapshot.docs[i]);
          recipes.add(recipe);
        }

        return Future.value(DataResult.success(recipes));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<List<String>>> getRecipesIDByNumberIngredient(
      int numberIngredient) async {
    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("recipe")
          .where("ingredientCount", isLessThan: numberIngredient)
          .get();
      if (querySnapshot.docs.isNotEmpty) {
        List<String> recipesID = [];
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          String recipeID = querySnapshot.docs[i].id.toString();
          recipesID.add(recipeID);
        }
        return Future.value(DataResult.success(recipesID));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<List<String>>> getListIDInfoByTimeServing(
      int timeStart, int timeEnd) async {
    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("info")
          .orderBy("time")
          .startAt([timeStart]).endAt([timeEnd]).get();
      if (querySnapshot.docs.isNotEmpty) {
        List<String> idInfoRecipes = [];
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          String idInfo = querySnapshot.docs[i].id.toString();

          idInfoRecipes.add(idInfo);
        }

        return Future.value(DataResult.success(idInfoRecipes));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<String>> getIDRecipeByIDInfo(String idInfo) async {
    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("recipe")
          .where("additionalInfo", isEqualTo: idInfo)
          .get();
      if (querySnapshot.docs.isNotEmpty) {
        String idRecipe = "";
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          idRecipe = querySnapshot.docs[i].id.toString();
        }

        return Future.value(DataResult.success(idRecipe));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<RecipeModel>> getRecipeByIDRecipe(String idRecipe) async {
    try {
      var querySnapshot = await fireStore.doc("recipe/$idRecipe").get();

      if (querySnapshot.exists) {
        RecipeModel recipe =
            RecipeModel(name: "", likes: 0, comments: 0, indexCover: 0);
        recipe =
            RecipeModel.fromDocumentSnapshot(documentSnapshot: querySnapshot);
        return Future.value(DataResult.success(recipe));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<List<String>>> getIDCookBookByIDRecipe(String idRecipe) async {

    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("cookbook").where("recipes", arrayContainsAny: [idRecipe])
          .get();
      if (querySnapshot.docs.isNotEmpty) {
        List<String> idCookBooks = [];
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          String idCookBook = querySnapshot.docs[i].id.toString();

          idCookBooks.add(idCookBook);
        }

        return Future.value(DataResult.success(idCookBooks));
      }
      } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<List<String>>> getUserByIDCookBook(String idRecipe) async {

    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("cookbook").where("recipes", arrayContainsAny: [idRecipe])
          .get();
      if (querySnapshot.docs.isNotEmpty) {
        List<String> idCookBooks = [];
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          String idCookBook = querySnapshot.docs[i].id.toString();

          idCookBooks.add(idCookBook);
        }

        return Future.value(DataResult.success(idCookBooks));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<List<UserModel>>> getUser() async {

    try {
      QuerySnapshot querySnapshot = await fireStore.collection("user").get();
      if (querySnapshot.docs.isNotEmpty) {
        List<UserModel> users = [];
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          AccountModel accountModel = AccountModel(email: "", password: "", fullname: "");
          SettingModel settingModel = SettingModel(isNotifyMeForFollowers: false, isWhenSomeOneSendMeAMessage: false, isSomeOneLiveCook: false, isFollowersCanSeeMyRecipes: false, isFollowersCanSeeFollowings: false);
          UserModel user = UserModel(account: accountModel, settings: settingModel);
          user = UserModel.fromDocumentSnapshot(documentSnapshot: querySnapshot.docs[i]);
          users.add(user);
        }
        return Future.value(DataResult.success(users));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }

  Future<DataResult<List<TagModel>>> getTag() async {

    try {
      QuerySnapshot querySnapshot = await fireStore.collection("tag").get();
      if (querySnapshot.docs.isNotEmpty) {
        List<TagModel> tags = [];
        for (int i = 0; i < querySnapshot.docChanges.length; i++) {
          TagModel tagModel = TagModel(name: "");
          tagModel = TagModel.fromDocumentSnapshot(documentSnapshot: querySnapshot.docs[i]);
          tags.add(tagModel);
        }
        return Future.value(DataResult.success(tags));
      }
    } catch (ex) {
      return Future.value(
          DataResult.failure(APIFailure(invalidResponse, ex.toString())));
    }
    return Future.value(
        DataResult.failure(APIFailure(invalidResponse, "data null")));
  }







}
