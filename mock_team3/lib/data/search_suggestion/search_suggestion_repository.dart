import 'package:mock_team3/data/search_suggestion/service/search_suggestion_service.dart';

import '../base/data_result.dart';
import '../recipe/model/recipe_model.dart';
import '../tag/model/tag_model.dart';
import '../user/model/user_model.dart';

abstract class SearchSuggestionRepository {
  Future<DataResult<List<RecipeModel>>> getRecipesTrending();

  Future<DataResult<String>> getImageFromStorage(String imageUrl);

  Future<DataResult<List<RecipeModel>>> getRecipesByIngredient(
      String ingredientName);

  Future<DataResult<List<String>>> getRecipesIDByNumberIngredient(
      int numberIngredient);

  Future<DataResult<List<String>>> getListIDInfoByTimeServing(
      int timeStart, int timeEnd);
  Future<DataResult<String>> getIDRecipeByIDInfo(String idInfo);
  Future<DataResult<RecipeModel>> getRecipeByIDRecipe(String idRecipe);
  Future<DataResult<List<String>>> getIDCookBookByIDRecipe(String idRecipe);
  Future<DataResult<List<UserModel>>> getUser();
  Future<DataResult<List<TagModel>>> getTag();
}

class SearchSuggestionImpl implements SearchSuggestionRepository {
  final SearchSuggestionService _searchSuggestionService;

  SearchSuggestionImpl(this._searchSuggestionService);

  @override
  Future<DataResult<List<RecipeModel>>> getRecipesTrending() {
    return _searchSuggestionService.getRecipesTrending();
  }

  @override
  Future<DataResult<String>> getImageFromStorage(String imageUrl) {
    return _searchSuggestionService.getImageFromStorage(imageUrl);
  }

  @override
  Future<DataResult<List<RecipeModel>>> getRecipesByIngredient(
      String ingredientName) {
    return _searchSuggestionService.getRecipesByIngredient(ingredientName);
  }

  @override
  Future<DataResult<List<String>>> getRecipesIDByNumberIngredient(
      int numberIngredient) {
    return _searchSuggestionService
        .getRecipesIDByNumberIngredient(numberIngredient);
  }

  @override
  Future<DataResult<List<String>>> getListIDInfoByTimeServing(
      int timeStart, int timeEnd) {
    return _searchSuggestionService.getListIDInfoByTimeServing(
        timeStart, timeEnd);
  }

  @override
  Future<DataResult<String>> getIDRecipeByIDInfo(String idInfo) {

    return _searchSuggestionService.getIDRecipeByIDInfo(idInfo);
  }

  @override
  Future<DataResult<RecipeModel>> getRecipeByIDRecipe(String idRecipe) {

    return _searchSuggestionService.getRecipeByIDRecipe(idRecipe);
  }

  @override
  Future<DataResult<List<String>>> getIDCookBookByIDRecipe(String idRecipe) {

   return _searchSuggestionService.getIDCookBookByIDRecipe( idRecipe);
  }

  @override
  Future<DataResult<List<UserModel>>> getUser() {

    return _searchSuggestionService.getUser();
  }

  @override
  Future<DataResult<List<TagModel>>> getTag() {

    return _searchSuggestionService.getTag();
  }


}
