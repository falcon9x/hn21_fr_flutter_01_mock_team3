import 'package:get/get.dart';

class TitleTabBar{
  String _name;
  var _status = false.obs;

  TitleTabBar(this._name);

  get status => _status;

  set status(value) {
    _status = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }
}