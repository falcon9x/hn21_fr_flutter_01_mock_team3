import 'package:get/get.dart';

class ButtonDialog{
  var isSelected = false.obs;
  String _title;

  ButtonDialog(this._title);

  String get title => _title;

  set title(String value) {
    _title = value;
  }
  @override
  String toString() {
    // TODO: implement toString
    return "$title $isSelected";
  }
}