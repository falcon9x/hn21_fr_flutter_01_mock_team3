class SearchSuggestionProfile {
  String _linkImageBackGround;
  String _linkAvatar;
  String _nameUser;
  int _numberRecipe;
  int _numberFollowers;

  SearchSuggestionProfile(this._linkImageBackGround, this._linkAvatar,
      this._nameUser, this._numberRecipe, this._numberFollowers);

  int get numberFollowers => _numberFollowers;

  set numberFollowers(int value) {
    _numberFollowers = value;
  }

  int get numberRecipe => _numberRecipe;

  set numberRecipe(int value) {
    _numberRecipe = value;
  }

  String get nameUser => _nameUser;

  set nameUser(String value) {
    _nameUser = value;
  }

  String get linkAvata => _linkAvatar;

  set linkAvata(String value) {
    _linkAvatar = value;
  }

  String get linkImageBackGround => _linkImageBackGround;

  set linkImageBackGround(String value) {
    _linkImageBackGround = value;
  }

  @override
  String toString() {
    return 'SearchSuggestionProfile{_linkImageBackGround: $_linkImageBackGround, _linkAvata: $_linkAvatar, _nameUser: $_nameUser, _numberRecipe: $_numberRecipe, _numberFollowes: $_numberFollowers}';
  }
}