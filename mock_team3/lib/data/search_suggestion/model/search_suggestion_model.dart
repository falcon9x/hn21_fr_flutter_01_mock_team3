class SearchSuggestionModel{
  String _nameRecipe;
  String _linkImageRecipe;

  SearchSuggestionModel(this._nameRecipe, this._linkImageRecipe);

  String get linkImageRecipe => _linkImageRecipe;

  set linkImageRecipe(String value) {
    _linkImageRecipe = value;
  }

  String get nameRecipe => _nameRecipe;

  set nameRecipe(String value) {
    _nameRecipe = value;
  }

  @override
  String toString() {
    return 'SearchSuggestionModel{_nameRecipe: $_nameRecipe, _linkImageRecipe: $_linkImageRecipe}';
  }
}