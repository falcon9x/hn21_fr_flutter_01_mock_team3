class TitleIngredient{
  int _index;
  String _name;

  TitleIngredient( this._index,  this._name);

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  @override
  String toString() {
    return 'TitleRecipe{_index: $_index, _name: $_name}';
  }
}