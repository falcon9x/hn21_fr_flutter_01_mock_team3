// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tag_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TagModel _$TagModelFromJson(Map<String, dynamic> json) => TagModel(
      follower: json['follower'] as int?,
      name: json['name'] as String,
      recipe: json['recipe'] as int?,
    );

Map<String, dynamic> _$TagModelToJson(TagModel instance) => <String, dynamic>{
      'follower': instance.follower,
      'name': instance.name,
      'recipe': instance.recipe,
    };
