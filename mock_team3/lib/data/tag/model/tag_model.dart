import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'tag_model.g.dart';

@JsonSerializable()
class TagModel {
  int? follower;
  String name;
  int? recipe;
  TagModel({
    this.follower,
    required this.name,
    this.recipe,
  });
  factory TagModel.fromJson(Map<String, dynamic> json) =>
      _$TagModelFromJson(json);
  Map<String, dynamic> toJson() => _$TagModelToJson(this);

  factory TagModel.fromDocumentSnapshot(
      {required DocumentSnapshot documentSnapshot}) {
    final data = documentSnapshot.data() as Map<String, dynamic>;
    return TagModel.fromJson(data);
  }
  factory TagModel.withInitial() => TagModel(name: "");
  @override
  String toString() =>
      'TagModel(follower: $follower, name: $name, recipe: $recipe)';
}
