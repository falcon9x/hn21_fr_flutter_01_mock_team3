
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:firebase_storage_mocks/firebase_storage_mocks.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_sign_in_mocks/google_sign_in_mocks.dart';
import 'package:mock_team3/data/account/model/account_model.dart';
import 'package:mock_team3/data/base/data_result.dart';
import 'package:mock_team3/data/firebase/firebase_service/firebase_service.dart';
import 'package:mock_team3/data/settings/model/setting_model.dart';
import 'package:mock_team3/data/user/model/user_model.dart';

const uid = 'abc';

void main() async {
  group("fetchUser", () {
    /// dump data
    final dumpUser = UserModel(
        account: AccountModel(
            email: "test@gmail.com", password: "123456", fullname: "test"),
        settings: SettingModel.withInitial());

    const dumpImageName = "testImage.jpg";
    /**
     * Fake instance of Firebase Firestore/Auth/Storage/Signin
     */
    final fakeFirebaseFirestore = FakeFirebaseFirestore();
    final mockFirebaseStorage = MockFirebaseStorage();
    final mockFirebaseAuth = MockFirebaseAuth(
        mockUser: MockUser(
      email: "test@gmail.com",
    ));
    final mockGoogleSignIn = MockGoogleSignIn();

    /// init firebase service
    final FireBaseService fireBaseService = FireBaseService(
        fireStore: fakeFirebaseFirestore,
        firebaseAuth: mockFirebaseAuth,
        firebaseStorage: mockFirebaseStorage);

    /// init data for test
    test('init data', () async {
      await fakeFirebaseFirestore.collection('user').add(dumpUser.toJson());
    });
    /**
     * result is true
     * data can get
     */
    test('test query right collection\'s name', () async {
      DataResult result = await fireBaseService.getQuerySnapshot("user");
      expect(result.isSuccess, true);
      final snapshot = result.data;
      expect(snapshot.docs.length, 1);
    });
    /**
     * query wrong collection's name
     */
    test('test query wrong collection\'s name', () async {
      DataResult result = await fireBaseService.getQuerySnapshot("users123");
      expect(result.isFailure, true);
      expect(result.error is APIFailure, true);
    });
    /**
     * query right document
     */
    test(
      'test query right document\'s name',
      () async {
        DataResult result = await fireBaseService.getQuerySnapshot("user");
        QuerySnapshot querySnapshot = result.data;
        result = await fireBaseService
            .getDocumentSnapshot("user/${querySnapshot.docs.first.id}");
        expect(result.isSuccess, true);
        DocumentSnapshot documentSnapshot = result.data;
        UserModel userModel =
            UserModel.fromDocumentSnapshot(documentSnapshot: documentSnapshot);
        expect(userModel.account.email, dumpUser.account.email);
      },
    );
    /**
     * query wrong document
     */
    test(
      'test query wrong document\'s name',
      () async {
        DataResult result = await fireBaseService.getQuerySnapshot("user");
        QuerySnapshot querySnapshot = result.data;
        result = await fireBaseService
            .getDocumentSnapshot("users/${querySnapshot.docs.first.id}");
        expect(result.isFailure, true);
        expect(result.error is APIFailure, true);
      },
    );
    /**
     * test get right image's path
     */
    test(
      'test upload image to firebase storage',
      () async {
        DataResult result =
            await fireBaseService.uploadImageToStorage(dumpImageName);
        expect(result.isSuccess, true);
      },
    );
  });

}
