# mock_team3

# Architecture
MVVM
Package by features

# Lib
- GetX
- Svg Flutter

# Package Structural
- app
    - base
    - bindings
    - core
    - features
    - routes
    - widgets
- data
- generate
main.dart

# Usage!!! ...

- build json serializable
    https://docs.flutter.dev/development/data-and-backend/json